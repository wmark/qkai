--------------------------------------------------------------------------------
-- Script to add the GIS UDF.
-- Replace suffix ".so" with ".dll" under Windows.
--
-- author	W-Mark Kubacki; wmark@hurrikane.de
-- date		2009-05-05

DROP FUNCTION IF EXISTS LatLong2UTM;
CREATE FUNCTION LatLong2UTM RETURNS STRING SONAME 'mysql-gis.so';
DROP FUNCTION IF EXISTS simple_distance;
CREATE FUNCTION simple_distance RETURNS REAL SONAME 'mysql-gis.so';
