/**
 * Adapter to Chuck Gantz's GIS functions for MySQL.
 *
 * @author	W-Mark Kubacki; wmark@hurrikane.de
 * @date	2009-05-05
 */

#ifdef STANDARD
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef __WIN__
typedef unsigned __int64 ulonglong;
typedef __int64 longlong;
#else
typedef unsigned long long ulonglong;
typedef long long longlong;
#endif /*__WIN__*/
#else
#include <my_global.h>
#include <my_sys.h>
#include <m_string.h>
#endif
#include <mysql.h>
#include <ctype.h>

#include "LatLong-UTMconversion.cpp"

#ifdef HAVE_DLOPEN

extern "C" {
	my_bool LatLong2UTM_init(UDF_INIT *initid, UDF_ARGS *args, char *message);
	char *LatLong2UTM(UDF_INIT *initid, UDF_ARGS *args, char *result, unsigned long *length, char *is_null, char *error);

	my_bool simple_distance_init(UDF_INIT *, UDF_ARGS *args, char *message);
	double simple_distance(UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error);
}

/** Does checks whether amount of arguments is right etc. and displays error messages as well as help. */
my_bool LatLong2UTM_init(UDF_INIT *initid, UDF_ARGS *args, char *message) {
	if(args->arg_count != 3) {
		strcpy(message, "Wrong arguments to LatLong2UTM.\n"
				"Usage: LatLong2UTM(ReferenceEllipsoid UNSIGNED INT, Latitutde DOUBLE, Longitude DOUBLE)");
		return 1;
	}
	args->arg_type[0]=STRING_RESULT;
	args->arg_type[1]=STRING_RESULT;
	args->arg_type[2]=STRING_RESULT;
	initid->maybe_null=1;
	initid->decimals=4;
	return 0;
}

/** Wrapper to actual conversion function. */
char *LatLong2UTM(UDF_INIT *initid, UDF_ARGS *args, char *result, unsigned long *length, char *is_null, char *error) {
	int ReferenceEllipsoid = (int) atoi(args->args[0]);
	double Latitude = (double) atof(args->args[1]);
	double Longitude = (double) atof(args->args[2]);

	double northing, easting;
	char zone;

	if(Longitude < -180.0 || Longitude > 179.9) {
		*is_null = 1;
		return 0;
	} else if(Latitude > 84.0 || Latitude < -80.0) {
		*is_null = 1;
		return 0;
	}

	LLtoUTM(ReferenceEllipsoid, Latitude, Longitude,
		northing, easting, &zone);
	sprintf(result, "POINT(%f %f)", easting, northing);
	*length = strlen(result);
	return result;
}

my_bool simple_distance_init(UDF_INIT *initid, UDF_ARGS *args, char *message) {
	if(args->arg_count != 4) {
		strcpy(message,"usage: SIMPLE_DISTANCE(x1, y1, x2, y2)");
		return 1;
	}

	for(uint i=0 ; i < 4; i++) {
		args->arg_type[i]=STRING_RESULT;
	}
	initid->maybe_null=1;
	initid->decimals=4;
	initid->max_length=6;
	return 0;
}

/** Does compute the distance between two coordinates ignoring eccentricity etc. */
double simple_distance(UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error) {
	double x1, x2, y1, y2;
	double result;

	x1 = (double) atof(args->args[0]);
	y1 = (double) atof(args->args[1]);
	x2 = (double) atof(args->args[2]);
	y2 = (double) atof(args->args[3]);

	result = 111.13384*acos(cos(x1*deg2rad)
				*cos(x2*deg2rad)
				*cos((y1-y2)*deg2rad)
				+sin(x1*deg2rad)
				*sin(x2*deg2rad)
				)
		/(deg2rad);

	*is_null = 0;
	*error = 0;

	return result;
}

#endif
