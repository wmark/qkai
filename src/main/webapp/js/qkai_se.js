/**
 * qKAI - Search & Explore
 *
 * @author W-Mark Kubacki; wmark@hurrikane.de
 */

Object.extend(Array.prototype, {
	insert: function(index) {
		var args = Array.prototype.slice.call(arguments, 1);
		this.length = Math.max(this.length, index);
		index = index < 0 ? this.length : index;

		if (args.length > 1) {
			this.splice.apply(this, [index, 0].concat(args));
		} else {
			this.splice(index, 0, args[0]);
		}
		return this;
	}
});

function display_error(msg) {
	if(window['Modalbox']) {
		Modalbox.MessageBox.alert("Error", msg);
	} else {
		alert(msg);
	}
}

function obtJSON(method, url, postVars, onCompleteCallback) {
	new Ajax.Request(url, {
		method: method,
		parameters: postVars,
		requestHeaders: ['Content-Type', 'application/x-www-form-urlencoded'],
		onSuccess: function(r) {
			var json = r.responseText.evalJSON();
			onCompleteCallback(json);
		},
		onFailure: function(r) {
			var json = r.responseText.evalJSON();
			if(json['error']) {
				display_error(json['error']);
			} else {
				display_error("An unexpected error occured during processing the request.");
			}
		}
	});
}
function postJSON(url, postVars, onCompleteCallback) {
	return obtJSON('post', url, postVars, onCompleteCallback);
}
function getJSON(url, postVars, onCompleteCallback) {
	return obtJSON('get', url, postVars, onCompleteCallback);
}
function putJSON(url, postVars, onCompleteCallback) {
	return obtJSON('put', url, postVars, onCompleteCallback);
}

Ajax.Responders.register({
	onCreate: function() {
		$('global_spinner').show();
	},
	onComplete: function() {
		if(Ajax.activeRequestCount <= 0) {
			$('global_spinner').hide();
		}
	}
});

var qkai = {

	/** Context Path */
	cp: "/",
	/**
	 * Counter of set POIs,
	 * so if obtaining a POI takes long we do not run into displaying an old POI over an current, which was returned rapidly.
	 */
	currentPoiNum: 0,

	view: {

		/**
		 * Formats a given list of items for output as ListView.
		 *
		 * @param ids Array of IDs for the particular order to be preserved
		 * @param items JSONArray of items to be formatted and displayed
		 * @return String ready to be inserted into result pane
		 */
		formatForListView: function(ids, items) {
			function format(r) {
				var a = new Array();
				if(r['URI']) a.push("<a class=\"resource_a\" href=\""+r.URI+"\" onmousedown=\"qkai.clk("+r.id+")\">");
				if(r['caption']) a.push("<em class=\"resource_name\">"+r.caption+"</em>");
				a.push("</a>");
				if(r['caption']) {
					if(r['description']) a.push("&nbsp;-&nbsp;<span class=\"resource_description\">"+r.description+"</span>");
				} else {
					a.push(r.URI);
				}
				a.push("<br />");
				
				if(r['comment']) {
					a.push("<p class=\"resource_comment\">");
					if(r.comment.length > 256) {
						a.push(r.comment.substr(0, 250));
						a.push("...");
					} else {
						a.push(r.comment);
					}
					a.push("</p>");
				}
				if(r['caption']) a.push("<span class=\"resource_uri\">"+r.URI+"</span>");
				a.push("&nbsp;-&nbsp;<span class=\"last_update\">updated "+r.lastUpdate+"</span><br />");
				a.push("<div id=\"res_links_"+r.id+"\">&nbsp;</div>");
				return "<li>"+a.join('')+"</li>";
			}
			var resources = new Hash();
			for(var i = 0; items[i]; i++) {
				resource = items[i];
				resources.set(resource.id, resource);
			}
			return "<ol>" + ids.collect(function(n) {
				return format(resources.get(n));
			}).join('') + "</ol>";
		}

	},

	/**
	 * Displays current search result according to current view.
	 *
	 * @param ids Array of IDs of the {@code Resource}s to be displayed
	 * @param forPOI POI's number the results are meant for
	 */
	displayCurrentResult: function(ids, forPOI) {
		function displayListView(result) {
			if (forPOI == qkai.currentPoiNum) {
				console.debug("Texts arrived.");
				$("results").update(qkai.view.formatForListView(ids, result['items']));
				console.debug("Texts have been displayed.");
			} else if (window['console']) {
				console.warn("Text for POI " + forPOI + " arrived too late to be displayed!");
			}
		}
		console.debug("Before retrieving resource texts.");
		getJSON(qkai.cp + "resource/text/" + ids, null, displayListView);
	},

	/**
	 * Sets the given POI, returnin a list of matching resource IDs per callback.
	 *
	 * @param element POI as created by {@link qkai.doSearch}
	 */
	setPOI: function(element) {
		function myCallback(result) {
			if (result['PoiId'] && result['PoiId'] == qkai.currentPoiNum) {
				qkai.displayCurrentResult(result['items'], qkai.currentPoiNum);
			} else if (window['console'] && result['PoiId']) {
				console.warn("POI " + result['PoiId'] + " arrived too late to be displayed!");
			} else if (window['console']) {
				console.error("Malformed response arrived by POI request.");
			}
		}
		qkai.currentPoiNum++;
		postJSON(qkai.cp + "poi/resources", {'poi': element.toJSON(), 'PoiId': qkai.currentPoiNum}, myCallback);
	},

	/**
	 * Sends POI selections to server, obtaining a list of matching resources.
	 */
	doSearch: function() {
		var poi = new Hash();
		// collect fields - search token
		var st = $("search_form_token").value.strip().gsub(",", " | ");
		if (!st.blank()) {
			poi.set('fulltext', st);
		}
		// collect fields - category
		var ct = $("cat_form_token").value.strip().gsub(",", " | ");
		if (!ct.blank()) {
			poi.set('category', ct);
		}
		// perform the search
		if (poi.keys().size() > 0) {
			qkai.setPOI(poi);
		}
	},

	/**
	 * Click handler to register requests for {@code Resource}s.
	 *
	 * @param resId ID of the {@code Resource} clicked on.
	 */
	clk: function(resId) {
		function mycallback(result) {
			alert(result);
		}
		putJSON(qkai.cp + "resource/click/"+resId, null, mycallback);
		return false;
	},

	/**
	 * Initializer of S&E
	 *
	 * Will add autocomplete, selectors and other widgets.
	 */
	init: function() {
		var spinner = $('global_spinner');
		if(spinner) { 
			spinner.hide();
		}
		new Ajax.Autocompleter("search_form_token", "search_form_token_choices", qkai.cp + "autocomplete/searchtoken",
				{paramName: "input", minChars: 3, tokens: [',', '\n', '\t']});
		new Ajax.Autocompleter("cat_form_token", "cat_form_token_choices", qkai.cp + "autocomplete/category",
				{paramName: "input", minChars: 3});
	}

}

document.observe('dom:loaded', function() {
	qkai.init();
});
