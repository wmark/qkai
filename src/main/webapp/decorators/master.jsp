<%@ page contentType="application/xhtml+xml; charset=UTF-8"
%><%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:dc="http://purl.org/dc/elements/1.1/"
      xmlns:foaf="http://xmlns.com/foaf/0.1/"
>
<head>
	<title>qKAI - <decorator:title default="Search and Explore" /></title>
	<link media="all" href="<%=request.getContextPath()%>/3rd/yui/reset-fonts-grids/reset-fonts-grids.css" type="text/css" rel="stylesheet" />
	<link media="all" href="<%=request.getContextPath()%>/css/design_base.css" type="text/css" rel="stylesheet" /><decorator:head/>
</head>
<body<decorator:getProperty property="body.onload" writeEntireProperty="true" /><decorator:getProperty property="body.onresize" writeEntireProperty="true" />>
<div id="<decorator:getProperty property="body.super:id" default="doc2" />" class="yui-t1">
<div id="hd" class="yui-gd">
	<div class="yui-u first">
		&nbsp;
		<a href="<%=request.getContextPath()%>/" id="qkai-logo">
			<img src="<%=request.getContextPath()%>/img/qkai_search_and_explore.png" alt="qKAI" />
		</a>
	</div>
	<div class="yui-u" about="#this">
		<h1 property="dc:title">qKAI</h1>
		<h2 property="dc:subject"><decorator:title default="Search and Explore" /></h2>
		<ul>
			<li class="first"><a href="<%=request.getContextPath()%>/" title="search page">Search</a></li>
			<li><a href="<%=request.getContextPath()%>/user/login" title="login">Login</a></li>
		</ul>
	</div>
</div>
<div<decorator:getProperty property="body.id" default="bd" writeEntireProperty="true" />><decorator:body /></div>
<div id="ft">
	<hr />
	<div class="center">
	<img class="first" src="<%=request.getContextPath()%>/img/logo_grey.png" alt="logo qKAI" />
	<div id="impressum" about="#this">
	<ul>
		<li class="first"><a href="<%=request.getContextPath()%>/privacy_policy.jsp">Privacy Policy</a></li>
		<li><a href="<%=request.getContextPath()%>/contact.jsp">Contact</a></li>
	</ul>
	<p class="copyright" property="dc:publisher" typeof="foaf:Organization">
		© University of Hanover, <a about="#sra" property="foaf:name" rel="foaf:homepage" href="http://www.sra.uni-hannover.de/" title="SRA">Institute of Systems Engineering</a>
	</p>
	<p class="copyright" property="dc:creator" typeof="foaf:Person">
		created by <a about="#kubacki" property="foaf:name" rel="foaf:weblog" href="http://mark.ossdl.de/">W-Mark Kubacki</a>
	</p>
	</div>
	<a href="http://www.sra.uni-hannover.de/"><img src="<%=request.getContextPath()%>/img/logo_sra.png" alt="logo SRA" /></a>
	<a href="http://www.uni-hannover.de/"><img src="<%=request.getContextPath()%>/img/logo_uni-hannover.png" alt="logo University of Hanover" /></a>
	</div>
</div>
</div>
</body>
</html>