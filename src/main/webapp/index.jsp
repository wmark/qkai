<%@ page
	contentType="text/html; charset=utf-8"
	import="qkai.model.User"
%><%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"
%><html>
<head>
	<title>Search and Explore</title>
	<link media="all" href="/3rd/scriptaculous/css/thirdparty/modalbox.css" type="text/css" rel="stylesheet" />
	<script src="/3rd/scriptaculous/javascript/prototype.js" type="text/javascript"></script>
	<script src="/3rd/scriptaculous/javascript/effects.js" type="text/javascript"></script>
	<script src="/3rd/scriptaculous/javascript/controls.js" type="text/javascript"></script>
	<script src="/3rd/scriptaculous/javascript/thirdparty/modalbox.js" type="text/javascript"></script>
	<script src="/js/qkai_se.js" type="text/javascript"></script>
</head>
<body id="sp_bd" super:id="doc3">
<div id="sp" class="yui-g">
	<div id="search_pane" class="form yui-u first">
		<label class="fieldlabel" for="search_form_token">search term</label><br />
		<input type="text" value="" id="search_form_token" class="autocomplete" onkeypress="if((event.which||event.keyCode)==Event.KEY_RETURN){qkai.doSearch()}" />
		<div id="search_form_token_choices" class="autocomplete"></div>
		<a href="javascript:qkai.doSearch()" class="quasi_btn">search</a>
		<br />
		<img src="/img/ajax-loader.gif" id="global_spinner" />
	</div>
	<div id="category_pane" class="yui-u">
		<label class="fieldlabel" for="cat_form_token">category</label><br />
		<input type="text" value="" id="cat_form_token" class="autocomplete" onkeypress="if((event.which||event.keyCode)==Event.KEY_RETURN){qkai.doSearch()}" />
		<div id="cat_form_token_choices" class="autocomplete"></div>
	</div>
</div>
<div id="bd">
	<div id="results"></div>
</div>
</body>
</html>