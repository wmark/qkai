<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>qKAI Wikipedia Extraction</title>
</head>
<body>
	<h1>qKAI Wikipedia Extraction</h1>
	<form action="/WikipediaExtractor" method="post">
			<p>
				<label>Wikipedia PageTitle:</label>
				<input type="text" name="pageTitle" value="<% if(request.getParameter("pageTitle") != null) out.println(request.getParameter("pageTitle")); %>" />
			</p>
			<p>
				<label>English</label>
				<input type="radio" value="en" checked="checked" name="language" />
				<label>German</label>
				<input type="radio" value="de"  name="language" />
			</p>
			<input type="submit" value="Send" />
	</form>
</body>
</html>