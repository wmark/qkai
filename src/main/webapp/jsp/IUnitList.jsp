<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ page import="org.openrdf.model.Statement" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>IUnit Overview</title>
</head>
<body>
	<h1>IUnit Overview</h1>
	<%
	Collection collection = (Collection)request.getAttribute("iunits");
	out.println("<p>");
	out.println(collection.size());
	out.println("</p>");
	
	out.println("<ul>");
	for (Iterator iter = collection.iterator(); iter.hasNext();) {
		Statement element = (Statement) iter.next();
		out.println("<li>");
		String resource = element.getSubject().toString();
		if(resource.length() > 23)
		{
			out.println("<a href=\"" + request.getContextPath() + "/resource/" + resource.substring(25) + "\">");
			out.println(element.getSubject().toString());
			out.println("</a>");
		}
		
		out.println("</li>");
	}
	out.println("</ul>");
	%>
</body>
</html>