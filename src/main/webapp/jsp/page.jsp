<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><% out.println(request.getAttribute("resourceLocation"));%></title>
</head>
<body>
	<p>Momentan gibt es keine HTML-Repr&auml;sentation der Ressource <% out.println(request.getAttribute("resourceLocation"));%></p>
	<p>Unter <a href="<%=request.getContextPath()%>/<% out.println(request.getAttribute("dataLocation"));%>"><%=request.getContextPath()%>/<% out.println(request.getAttribute("dataLocation"));%></a> gibt es die RDF-Repr&auml;sentation.</p>
</body>
</html>