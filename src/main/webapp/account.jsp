<%@ page
	contentType="text/html; charset=utf-8"
	import="qkai.model.User"
%><%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"
%><%
	User user = User.getCurrent(request);
%><html>
<head>
	<title><% if(user.isAnonymous()) { %>Login<% } else { %>Account<% } %></title>
</head>
<body>
<% if(user.isAnonymous()) { %>
	<h3 class="openid">Login with <a href="http://openid.net/" title="OpenID Homepage" class="inh">OpenID</a></h3>
	<div class="openid_error">${openid_servlet_filter_msg}</div>
	<form action="/user/login/openid" method="post">
		<input id="openid_identifier" name="openid_identifier" type="text" value="${openid_user.claimedId}" />
		<input class="btn" type="submit" value="send" />
	</form>
<% } else { %>
	<div>You are logged in as <cite><%= user.getName() %></cite>.</div>
	<h3 class="openid">Logout with <a href="http://openid.net/" title="OpenID Homepage" class="inh">OpenID</a></h3>
	<p>
		<a href="/user/logout"><strong>Click here</strong> to log out.</a>
		You will be redirected to login page to be able to change identity.
	</p>
<% } %>
</body>
</html>