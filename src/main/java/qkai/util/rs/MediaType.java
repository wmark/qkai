package qkai.util.rs;

/**
 * MediaType proxy with extensions for usage in webservice annotations.
 *
 * Some additional <a href="http://www.w3.org/2008/01/rdf-media-types">media types for RDF</a> are used from the
 * <a href="http://www4.wiwiss.fu-berlin.de/bizer/rdfapi/tutorial/netapi.html">TU Berlin's NetAPI</a>.
 *
 * {@stickyNote The difference between {@code application/*} and {@code text/*} is in general
 * that the first is expected to return values encoded in ASCII and for the latter automatic
 * charset detection by the browser (or any other client) is allow to be applied.}
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public class MediaType extends javax.ws.rs.core.MediaType {

	/** RDF N-Triples, {@code application/n-triples} as used by NetAPI. */
	public static final String APPLICATION_NTRIPLES = "application/n-triples";

	/** RDF N3, {@code application/n3} as used by NetAPI. */
	public static final String APPLICATION_RDF_N3 = "application/n3";

	/** {@code application/rdf+xml}. */
	public static final String APPLICATION_RDF_XML = "application/rdf+xml";

	/** {@code application/x-turtle}. */
	public static final String APPLICATION_XTURTLE = "application/x-turtle";

	/**
	 * {@code text/javascript}, generally not expected to be evaluated.
	 * @warning Marked as "obsolete" as of <a href="http://www.ietf.org/rfc/rfc4329.txt">RFC 4329</a>.
	 */
	public static final String TEXT_JAVASCRIPT = "text/javascript";

	/** RDF N-Triples, {@code text/plain} as recommended by W3C. */
	public static final String TEXT_NTRIPLES = "text/plain";

	/** {@code text/rdf+n3}. */
	public static final String TEXT_RDF_N3 = "text/rdf+n3";
}
