package qkai.util;

import java.io.IOException;

import java.util.Properties;

/**
 * Proxy to configuration files
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public final class ConfigObj {

	private static final String DEFAULT_RESOURCE_PATH = "/qkai.properties";
	private static final Properties properties;

	static {
		synchronized (ConfigObj.class) {
			try {
				properties = new Properties();
				properties.load(ConfigObj.class.getResourceAsStream(DEFAULT_RESOURCE_PATH));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/** Constructor hidden as this is a Signleton. */
	private ConfigObj() {
		// intentionally left empty
	}

	/**
	 * Gets this application's {@code Properties}.
	 *
	 * @info Call this to get any property of interest from config files.
	 * @return the properties
	 */
	public static Properties getProperties() {
		return properties;
	}

}
