package qkai.model.openid;

import javax.servlet.http.HttpServletRequest;

//~--- non-JDK imports --------------------------------------------------------

import com.dyuproject.openid.OpenIdUser;
import com.dyuproject.openid.RelyingParty;
import com.dyuproject.openid.UrlEncodedParameterMap;

/**
 * Adaption to the original <a href="http://code.google.com/p/dyuproject/source/browse/trunk/modules/openid/src/main/java/com/dyuproject/openid/ext/SRegConfigListener.java">{@code com.dyuproject.openid.ext.SRegConfigListener}</a>
 * to minify the request for user data.
 *
 * @info qKAI does not need all the information provided by the full {@code SReg}.
 *
 * @info To use this listener you will have to add it inside of <cite>resources/openid.properties#openid.relyingparty.listeners</cite>.
 * @warning It will replace {@code SRegConfigListener}, therefore do not use both of them.}
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
public final class MinSRegConfigListener implements RelyingParty.Listener {

	@Override
	public void onDiscovery(final OpenIdUser user, final HttpServletRequest request) {
		// intentionally left empty
	}

	@Override
	public void onPreAuthenticate(final OpenIdUser user, final HttpServletRequest request, final UrlEncodedParameterMap params) {
		MinSReg.put(params);
	}

	@Override
	public void onAuthenticate(final OpenIdUser user, final HttpServletRequest request) {
		MinSReg.set(user, MinSReg.parse(request));
	}

	@Override
	public void onAccess(final OpenIdUser user, final HttpServletRequest request) {
		// intentionally left empty
	}

}
