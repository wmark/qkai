package qkai.model.openid;

import java.io.Serializable;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//~--- non-JDK imports --------------------------------------------------------

import com.dyuproject.openid.Constants;
import com.dyuproject.openid.OpenIdUser;
import com.dyuproject.openid.UrlEncodedParameterMap;

import org.mortbay.util.ajax.JSON;
import org.mortbay.util.ajax.JSON.Output;

/**
 * Stripped <a hreaf="http://code.google.com/p/dyuproject/source/browse/trunk/modules/openid/src/main/java/com/dyuproject/openid/ext/SReg.java">{@code com.dyuproject.openid.ext.SReg}</a>
 * to minify the data a given user is shown to must provide.
 *
 * <p>qKAI does not need all the information provided by the full {@code SReg}.</p>
 *
 * {@stickyNote "For more on this please see:"
 * <ul>
 *      <li>
 *              <a href="http://openid.net/specs/openid-simple-registration-extension-1_0.html">OpenID Simple Registration Extension 1.0</a>
 *              at <a href="http://openid.net/specs/">OpenID Specifications</a>.
 *      </li>
 *      <li><a href="http://openid.net/specs/openid-attribute-exchange-1_0.html">OpenID Attribute Exchange 1.0 - Final</a></li>
 *      <li><a href=""http://www.axschema.org/>Schema for OpenID Attribute Exchange</a> (AxSchema)</li>
 * </ul>}
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
@SuppressWarnings("serial")
public final class MinSReg implements Serializable, JSON.Convertible {

	public static final String ATTR_NAME = "sreg";
	public static final String OPENID_SREG_OPTIONAL = "openid.sreg.optional";
	public static final String OPENID_SREG_REQUIRED = "openid.sreg.required";
	private int _propertyCount = 0;
	private String _country;
	private String _nickname;
	private String _postcode;

	static void put(final UrlEncodedParameterMap params) {
		params.put(Constants.OPENID_NS_SREG, Constants.Sreg.VERSION);
		params.put(OPENID_SREG_REQUIRED, "nickname,country");
		params.put(OPENID_SREG_OPTIONAL, "postcode");
	}

	static void set(final OpenIdUser user, final MinSReg sreg) {
		user.setAttribute(ATTR_NAME, sreg);
	}

	public static MinSReg get(final OpenIdUser user) {
		return (MinSReg) user.getAttribute(ATTR_NAME);
	}

	static MinSReg parse(final HttpServletRequest request) {
		final MinSReg sreg = new MinSReg();

		sreg.setNickname(request.getParameter(Constants.SREG_NICKNAME));
		sreg.setPostcode(request.getParameter(Constants.SREG_POSTCODE));
		sreg.setCountry(request.getParameter(Constants.SREG_COUNTRY));
		if (sreg._propertyCount == 0) {
			return null;
		}
		request.setAttribute(ATTR_NAME, sreg);
		return sreg;
	}

	public int getPropertyCount() {
		return _propertyCount;
	}

	/**
	 * AxSchema compatibility
	 *
	 * @return username
	 */
	public String getUsername() {
		return getNickname();
	}

	/**
	 * AxSchema compatibility
	 *
	 * @return alias
	 */
	public String getAlias() {
		return getNickname();
	}

	public String getNickname() {
		return _nickname;
	}

	void setNickname(final String nickname) {
		if (nickname != null) {
			if (_nickname == null) {
				_propertyCount++;
			}
			_nickname = nickname;
		}
	}

	public String getPostcode() {
		return _postcode;
	}

	void setPostcode(final String postcode) {
		if (postcode != null) {
			if (_postcode == null) {
				_propertyCount++;
			}
			_postcode = postcode;
		}
	}

	public String getCountry() {
		return _country;
	}

	void setCountry(final String country) {
		if (country != null) {
			if (_country == null) {
				_propertyCount++;
			}
			_country = country;
		}
	}

	@Override
	public void fromJSON(final Map map) {
		_propertyCount = ((Number) map.get("pc")).intValue();
		_nickname = (String) map.get("n");
		_postcode = (String) map.get("p");
		_country = (String) map.get("c");
	}

	@Override
	public void toJSON(final Output out) {
		out.addClass(getClass());
		out.add("pc", _propertyCount);
		out.add("n", _nickname);
		out.add("p", _postcode);
		out.add("c", _country);
	}

}
