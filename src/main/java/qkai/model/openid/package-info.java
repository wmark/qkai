/**
 * OpenID utilizes data listener and other strategies. They are here.
 *
 * {@stickyNote These are not used by qKAI directly but configured to be used by third party code.}
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
package qkai.model.openid;
