package qkai.model;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.util.HashMap;
import java.util.Map;

//~--- non-JDK imports --------------------------------------------------------

import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.rio.RdfDocumentWriter;
import org.openrdf.rio.n3.N3Writer;
import org.openrdf.rio.ntriples.NTriplesWriter;
import org.openrdf.rio.rdfxml.RdfXmlWriter;
import org.openrdf.rio.turtle.TurtleWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.model.db.Property;
import qkai.model.db.Relation;
import qkai.model.db.Resource;

import qkai.util.rs.MediaType;

/**
 * Externalizes qKAI's internal RDF model to a common RDF externalization form.
 *
 * Such forms are used for exchanging or displaying RDF data.
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
public final class RdfExternalizer {

	private static final Logger log = LoggerFactory.getLogger(RdfExternalizer.class);
	private static final Map<String, Class<? extends RdfDocumentWriter>> documentWriter = new HashMap<String,
																							  Class<? extends RdfDocumentWriter>>(4);
	private static final ValueFactory valueFactory = new ValueFactoryImpl();
	private static final org.openrdf.model.URI sameAs = valueFactory.createURI("http://www.w3.org/2002/07/owl#sameAs");

	static {
		documentWriter.put(MediaType.TEXT_RDF_N3, N3Writer.class);
		documentWriter.put(MediaType.APPLICATION_RDF_N3, N3Writer.class);
		documentWriter.put(MediaType.TEXT_NTRIPLES, NTriplesWriter.class);
		documentWriter.put(MediaType.APPLICATION_NTRIPLES, NTriplesWriter.class);
		documentWriter.put(MediaType.APPLICATION_RDF_XML, RdfXmlWriter.class);
		documentWriter.put(MediaType.APPLICATION_XTURTLE, TurtleWriter.class);
	}

	/**
	 * Gets an instantiated {@code RdfDocumentWriter} for a given mime type.
	 *
	 * @param mimeType expected to be one of the RDF mime types defined in {@link qkai.util.rs.MediaType}
	 * @param out used for initialization of the writer to be returned
	 * @return instance of {@code RdfDocumentWriter}, not initialized
	 * @throws ClassNotFoundException if either the class implementing {@code RdfDocumentWriter} has been removed after start,
	 *                                or if its constructor does not accept a sole {@code OutputStream} (or needs more paramters)
	 */
	public static RdfDocumentWriter getWriterFor(final String mimeType, final OutputStream out) throws ClassNotFoundException {
		if (documentWriter.containsKey(mimeType)) {
			try {
				RdfDocumentWriter w = documentWriter.get(mimeType).getDeclaredConstructor(OutputStream.class).newInstance(out);

				return w;
			} catch (Exception e) {
				log.error("OpenRDF API has changed or is used incorrectly", e);
				throw(RuntimeException) new RuntimeException().initCause(e);
			}
		} else {
			log.error("OpenRDF API has changed or is used incorrectly", mimeType);
			throw new ClassNotFoundException();
		}
	}

	/**
	 * Initializes the given {@code RdfDocumentWriter}. E.g. by setting namespaces.
	 *
	 * @param rdfWriter the writer to be initialized
	 * @throws IOException if passed by {@code RdfDocumentWriter}
	 */
	private static void initializeWriter(final RdfDocumentWriter rdfWriter) throws IOException {
		rdfWriter.setNamespace("dbpedia-owl", "http://dbpedia.org/ontology/");
		rdfWriter.setNamespace("dbpprop", "http://dbpedia.org/property/");
		rdfWriter.setNamespace("georss", "http://www.georss.org/georss/");
		rdfWriter.setNamespace("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		rdfWriter.setNamespace("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		rdfWriter.setNamespace("owl", "http://www.w3.org/2002/07/owl#");
		rdfWriter.setNamespace("geo", "http://www.w3.org/2003/01/geo/wgs84_pos#");
		rdfWriter.setNamespace("skos", "http://www.w3.org/2004/02/skos/core#");
		rdfWriter.setNamespace("foaf", "http://xmlns.com/foaf/0.1/");
		rdfWriter.setNamespace("dbpedia", "http://dbpedia.org/resource/");
		rdfWriter.setNamespace("", "http://qkai.org/resource/");
	}

	/**
	 * Creates a {@link org.openrdf.model.Value} a qKAI {@code Property}.
	 *
	 * @impl Sets language if necessary.
	 * @param qkaiProperty the {@code Value} is to be created for
	 * @return representation of a qKAI {@code Property}
	 */
	private static org.openrdf.model.Value getValueOf(Property qkaiProperty) {
		if (qkaiProperty.getLang() != null) {
			return valueFactory.createLiteral(qkaiProperty.getPvalue(), qkaiProperty.getLang());
		} else {
			return valueFactory.createLiteral(qkaiProperty.getPvalue());
		}
	}

	/**
	 * Externalizes the given {@code Resource} to the given {@code RdfDocumentWriter}.
	 *
	 * Use this method if you either don't want to handle mime types and want to use directly the correct writer,
	 * or if you have written your own writer to be used here.
	 *
	 * @param qkaiResource the item to be externalized
	 * @param rdfWriter writer to be used for externalization;
	 *                  will be initialized, started and finally its {@literal endDocument} method called
	 * @throws IOException if passed by {@code RdfDocumentWriter}
	 */
	public static void externalize(final Resource qkaiResource, final RdfDocumentWriter rdfWriter) throws IOException {
		final org.openrdf.model.URI subject = valueFactory.createURI("http://qkai.org/resource/" + qkaiResource.getID());
		org.openrdf.model.URI predicate;
		org.openrdf.model.Value object;

		initializeWriter(rdfWriter);
		rdfWriter.startDocument();
		rdfWriter.writeStatement(subject, sameAs, valueFactory.createURI(qkaiResource.getResourceURI()));
		for (Property p : qkaiResource.getPropertyCollection()) {
			predicate = valueFactory.createURI(p.getPredicate().getResourceURI());
			object = getValueOf(p);
			rdfWriter.writeStatement(subject, predicate, object);
		}
		for (Relation r : qkaiResource.getRelationCollection()) {
			predicate = valueFactory.createURI(r.getPredicate().getResourceURI());
			object = valueFactory.createURI(r.getTarget().getResourceURI());
			rdfWriter.writeStatement(subject, predicate, object);
		}
		rdfWriter.endDocument();
	}

	/**
	 * Wrapper to {@link #externalize(qkai.model.db.Resource, org.openrdf.rio.RdfDocumentWriter)} for a given mime type,
	 * externalizing to String.
	 *
	 * This one is expected to be used for common RDF formats.
	 * Therefore, in doubt, in your controllers call this method.
	 *
	 * @param qkaiResource the item to be externalized
	 * @param mimeType expected to be one of the RDF mime types defined in {@link qkai.util.rs.MediaType}
	 * @return externalized {@code Resource}
	 * @throws ClassNotFoundException if a OpenRDF writer class has been not found during runtime, e.g. removed after the start
	 */
	public static String externalize(final Resource qkaiResource, final String mimeType) throws ClassNotFoundException {
		try {
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			RdfDocumentWriter rdfWriter = getWriterFor(mimeType, bout);

			externalize(qkaiResource, rdfWriter);
			return bout.toString();
		} catch (IOException e) {
			log.error("Caught unexpected exception which ocurence was assumed to be impossible.", e);
			return "";
		}
	}

}
