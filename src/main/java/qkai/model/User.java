package qkai.model;

import /* const */ javax.servlet.http.HttpServletRequest;
import /* const */ javax.servlet.http.HttpServletResponse;

//~--- non-JDK imports --------------------------------------------------------

import com.dyuproject.openid.OpenIdUser;
import com.dyuproject.openid.RelyingParty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.model.db.Resource;
import qkai.model.openid.MinSReg;

/**
 * Single access point for obtaining users and identities.
 *
 * <p>As we rely on OpenID, there is currently no explicit user creation in qKAI.
 * For more informations please see
 * <a href="http://code.google.com/p/dyuproject/wiki/openid">OpenID with dyuproject</a>
 * or <a href="http://openid.net/what/">what OpenID is</a>.</p>
 *
 * {@example "getting current user in JSP"
 * // get the current user
 * User user = User.getCurrent(request);
 * // logout
 * user.logout(request, response);
 * response.sendRedirect(request.getContextPath() + "/");
 * }
 * {@example "getting current user from within [a JAX-RS] servlet"
 * User user = User.getCurrent(@Context HttpServletRequest request);
 * // operations same as above
 * }
 * {@stickyNote If you need to get a User outside the scopes mentioned above,
 * you will have to fetch its {@link Resource} by the value of his OpenID URL.
 * See {@link qkai.model.db.Resource.ContentHint#USER} to aid you with that.}
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
public final class User {

	private static final String ANONYMOUS_URI = "http://qkai.org/qkai/rest/user/anonymous";
	private static final Logger log = LoggerFactory.getLogger(User.class);

	/** for mapping to the user if identified by OpenID */
	private OpenIdUser openid_user = null;

	/**
	 * Creating a new user is meant to be done with any identifying data a priori to avoid duplicates.
	 * E.g., use {@link User#getCurrent} instead.
	 */
	private User() {
		super();
	}

	/**
	 * Does always return a user be able to work with.
	 * So check prior to using this if he is anonymous!
	 *
	 * @return     a User. Whether a new one or not is left intentionally unsaid.
	 * @throws IllegalArgumentException if request is null
	 */
	public static User getCurrent(final HttpServletRequest request) {
		if (request == null) {
			throw new IllegalArgumentException("request cannot be null.");
		}

		final User u = new User();

		try {
			u.openid_user = RelyingParty.getInstance().discover(request);
		} catch (Exception e) {
			log.debug("Uncaught exception", e);
		}
		return u;
	}

	/**
	 * In order to store data for anonymous there is a NULL user.
	 * This way you can find out whether you fetched him to avoid such things as unnecessary logouts.
	 *
	 * @return     true if he is 'the masses', i.e. no individual
	 */
	public boolean isAnonymous() {
		return !((openid_user != null) && openid_user.isAuthenticated());
	}

	/**
	 * Gets an URI identifying the current user.
	 *
	 * <p>By using OpenID we assume every user has an unique URI.
	 * Therefore a given user can be seen as RDF node, which fits into qKAI's data model</p>
	 *
	 * @return unique URI identifying the current user; in case of anonymous the URI is shared by users
	 */
	public String getIdentifier() {
		if (isAnonymous()) {
			return ANONYMOUS_URI;
		} else {
			return openid_user.getIdentifier();
		}
	}

	/**
	 * Gets the {@code Resource} associated with the current user.
	 *
	 * @info You can use this {@code Resource} to store user data, such as preferences.
	 * @info Check whether the current user is Anonymous first before storing data not meant for the masses.
	 * @return {@code Resource} associated with the current user ready to add {@code Properties} and such
	 */
	public Resource getUserAsResource() {
		Resource r = Resource.valueOf(this.getIdentifier(), Resource.ContentHint.USER);

		if (r.getHint() != Resource.ContentHint.USER) {
			log.warn("A Resource whose hint = {} is atypical has been associated with user: {}", r.getHint().toString(),
					 r.getResourceURI());
		}
		return r;
	}

	/**
	 * Use this in case you need to address him/her/it.
	 */
	public String getName() {
		if (isAnonymous()) {
			return "Anonymous";
		} else {
			final MinSReg sreg = MinSReg.get(openid_user);

			if (sreg == null) {
				return openid_user.getIdentifier();
			} else {
				return sreg.getNickname();
			}
		}
	}

	/**
	 * @param       request         Won't be modified.
	 * @param       response        Is being altered for invalidating the users' login.
	 * @throws IllegalArgumentException if request or response is null
	 */
	public void logout(final HttpServletRequest request, final HttpServletResponse response) {
		if ((request == null) || (response == null)) {
			throw new IllegalArgumentException("Neither request nor response cannot be null.");
		}
		if (!isAnonymous()) {
			try {
				RelyingParty.getInstance().invalidate(request, response);
			} catch (java.io.IOException e) {
				log.debug("Uncaught exception", e);
			}
		}
	}

}
