package qkai.model.db;

import java.io.Serializable;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.persistence.*;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONException;
import org.json.JSONObject;

import qkai.logic.service.PersistenceService;

/**
 * Representation of a RDF resource.
 *
 * <p>Also referred to as <em>node</em> or in special cases <em>subject</em>.</p>
 *
 * <p>It is instance-controlled, immutable except for the property {@link #fullyLoaded} and thread-safe, with the exceptions being the
 * collections which are meant to be managed by persistency or converters only.</p>
 *
 * {@stickyInfo Obtain instances by the static factory method {@link #valueOf}.}
 *
 * {@stickyInfo <p>An {@code Resource} can represent a node, property (being a {@link Property} or {@link Relation})
 * or (in qKAI) an {@link qkai.model.User}. Despite this it also is still a <em>node</em>.</p>
 * <p>Mind the duality. It basically means, everything can have its own properties.</p>}
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
@Entity
@Table(name = "resources")
@NamedQueries( {
	@NamedQuery(name = "Resource.findAll", query = "SELECT r FROM Resource r") ,
	@NamedQuery(name = "Resource.findByResource", query = "SELECT r FROM Resource r WHERE r.resource = :resource") ,
	@NamedQuery(name = "Resource.findByResourceURI", query = "SELECT r FROM Resource r WHERE r.resourceURI = :resourceURI") ,
	@NamedQuery(name = "Resource.setFullyLoaded", query = "UPDATE Resource r SET r.fullyLoaded = :fullyLoaded WHERE r.resource = :resource")
})
public final class Resource implements Serializable {

	private static final Lock lock = new ReentrantLock();
	private static final long serialVersionUID = 1L;
	@Basic(optional = false)
	@Column(name = "fully_loaded", nullable = false)
	private boolean fullyLoaded;
	@Basic(optional = false)
	@Column(name = "hint")
	@Enumerated(EnumType.STRING)
	private ContentHint hint;

	/** This is only utilized at database for pruning and updates. Might be null, which means it has been just created. */
	@Basic(optional = false, fetch = FetchType.LAZY)
	@Column(name = "last_update", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdate;
	@OneToMany(
		cascade = CascadeType.ALL,
		mappedBy = "predicate",
		fetch = FetchType.LAZY
	)
	Collection<Property> propertyAsPredicateCollection;
	@OneToMany(
		cascade = CascadeType.ALL,
		mappedBy = "resource",
		fetch = FetchType.LAZY
	)
	Collection<Property> propertyCollection;

	/** As predicate in an inter-resource relation. */
	@OneToMany(
		cascade = CascadeType.ALL,
		mappedBy = "predicate",
		fetch = FetchType.LAZY
	)
	Collection<Relation> relationAsPredicateCollection;

	/** As resource in that relation. */
	@OneToMany(
		cascade = CascadeType.ALL,
		mappedBy = "resource",
		fetch = FetchType.LAZY
	)
	Collection<Relation> relationAsSubjectCollection;

	/** As target/object in an inter-resource relation. */
	@OneToMany(
		cascade = CascadeType.ALL,
		mappedBy = "target",
		fetch = FetchType.LAZY
	)
	Collection<Relation> relationAsTargetCollection;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "resource")
	private Long resource;
	@Basic(optional = false)
	@Column(
		name = "resourceURI",
		nullable = false,
		length = 255,
		unique = true
	)
	private String resourceURI;
	@JoinTable(
		name = "findings",
		joinColumns = {@JoinColumn(name = "resource", referencedColumnName = "resource") },
		inverseJoinColumns = {@JoinColumn(name = "tid", referencedColumnName = "tid") }
	)
	@ManyToMany(fetch = FetchType.LAZY)
	private Collection<SearchToken> searchTokenCollection;

	/**
	 * RDF extrinsic hint to indicate a node of being a certain type.
	 *
	 * @info No matter what an indication says, mind the duality of a {@link Resource} always being a {@code NODE}/"RDF subject", too.
	 * @author W-Mark Kubacki
	 */
	public static enum ContentHint {

		/** Also referred to as "subject". Should be the standard. */
		NODE,

		/** Indicates this {@code Resource} is being used as RDF property. */
		PROPERTY,

		/** Mark as this is being used for storage of user-data. */
		USER
	}

	/** Private constructor for persistence only. */
	private Resource() {}

	private Resource(final String resourceURI, final ContentHint hint) {
		this.resourceURI = resourceURI;
		this.fullyLoaded = false;
		this.hint = hint;
	}

	/**
	 * Gets an existing instance.
	 *
	 * <p>If this fails with an exception it will be safe to create a new instance without further checking whether it already exists.</p>
	 *
	 * @param uri URI as representation of the Resource to be found
	 * @return Resource, already persistent
	 * @throws javax.persistence.NoResultException Marks the case where it is safe to create a new Resource for given values.
	 */
	public static Resource valueOfIfExists(final String uri) throws javax.persistence.NoResultException {
		final EntityManager em = PersistenceService.getInstance().getEntityManager();
		final Query q = em.createNamedQuery("Resource.findByResourceURI");

		q.setParameter("resourceURI", uri);
		q.setMaxResults(1);
		return (Resource) q.getSingleResult();
	}

	/**
	 * Gets an Resource, creates one should it not exist.
	 *
	 * <p>Business logic needs to retrieve Resources from database without bothering whether it existed or not.
	 * This static factory method realizes instance-controll and guarantees instances are being immutable.
	 * Plus it replaces a builder for making sure this Bean is always in consistent state.</p>
	 *
	 * @param uri URI the resource will represent
	 * @param hint ContentHint will be used to indicate the RDF extrinsic type of the Resource, if a new one will be created
	 * @return existing and persisting instance of this model
	 * @throws IllegalArgumentException if uri or hint is null
	 */
	public static Resource valueOf(final String uri, final ContentHint hint) {
		if ((uri == null) || (hint == null)) {
			throw new IllegalArgumentException("Neither uri nor hint can be null.");
		}
		lock.lock();
		try {
			return Resource.valueOfIfExists(uri);
		} catch (javax.persistence.NoResultException e) {
			return PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Resource>() {
				@Override
				public Resource run() {
					final Resource w = new Resource(uri, hint);

					em.persist(w);
					return w;
				}
			});
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Gets the <em>primary key</em> of this {@code Entity}.
	 *
	 * @return primary key of this {@code Entity}
	 * @throws NullPointerException if a primary key has not been assigned, yet
	 */
	public long getID() {
		return resource;
	}

	/**
	 * Gets this {@code Resource}'s URI.
	 *
	 * <p>That is the place where the RDF document with contents (such as properties) was found.</p>
	 *
	 * @return URI as String
	 */
	public String getResourceURI() {
		return resourceURI;
	}

	/**
	 * Indicates whether this {@code Resource}s properties have been already fetched.
	 *
	 * @return true, if this {@code Resource}s properties have been already fetched
	 */
	public boolean isFullyLoaded() {
		return fullyLoaded;
	}

	/**
	 * Sets the 'fully loaded' flag of this {@code Resource}.
	 *
	 * @param fullyLoaded true, if this {@code Resource}s properties have been already fetched
	 */
	public void setFullyLoaded(final boolean fullyLoaded) {
		this.fullyLoaded = fullyLoaded;
	}

	/**
	 * Returns the RDF extrinsic hint about this instance's content.
	 *
	 * @return a hint about the contents behing this {@code Resource}
	 */
	@Enumerated(EnumType.STRING)
	public ContentHint getHint() {
		return this.hint;
	}

	/**
	 * Gets the date/time stamp indicating when this instance has lately experienced an update.
	 *
	 * <p>Creating a {@code Resource} sets 'last update'.</p>
	 *
	 * @warning invocation of this method is expensive, as the corresponding fields are lazyly loaded
	 * @return date and time of the last update
	 * @throws NullPointerException if this {@code Resource} has not been serialized and/or not updated, yet
	 */
	public Date getLastUpdate() {
		return new Date(lastUpdate.getTime());
	}

	/**
	 * Establishes mutual relationship between Resource and SearchToken.
	 *
	 * <p>Represents a 'finding' of that token in the Resource or its Properties.</p>
	 *
	 * @see SearchToken#addResource
	 * @param token Will be added to this Resource.
	 */
	void addToSearchToken(final SearchToken token) {
		assert token != null;
		this.searchTokenCollection.add(token);
	}

	void addRelationAsPredicate(final Relation rr) {
		this.relationAsPredicateCollection.add(rr);
		if (rr.getPredicate() == null) {
			rr.setPredicate(this);
		}
	}

	void addPropertyAsPredicate(final Property property) {
		this.propertyCollection.add(property);
		if (property.getResource() == null) {
			property.setResource(this);
		}
	}

	void addProperty(final Property property) {
		this.propertyCollection.add(property);
		if (property.getResource() == null) {
			property.setResource(this);
		}
	}

	/**
	 * Gets {@code Property}es of this {@code Resource}.
	 *
	 * @warning The collection might become obsolete during runtime, if this {@code Resource} is concurrently being fetched.
	 * @return Properties in unmodifiable Collection.
	 */
	public Collection<Property> getPropertyCollection() {
		return Collections.unmodifiableCollection(propertyCollection);
	}

	/**
	 * Gets {@code Relation}s of this {@code Resource}.
	 *
	 * @warning The collection might become obsolete during runtime, if this {@code Resource} is concurrently being fetched.
	 * @return Relations in unmodifiable Collection.
	 */
	public Collection<Relation> getRelationCollection() {
		return Collections.unmodifiableCollection(relationAsSubjectCollection);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return (resource == null) ? 0 : resource.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Resource)) {
			return false;
		}

		final Resource other = (Resource) o;

		return (resourceURI == null) ? other.resourceURI == null : resourceURI.equals(other.resourceURI);
	}

	/**
	 * This is subject to change and not meant for public use as creation is expensive.
	 * Please use JAXB converters for obtaining a complete JSON representation.
	 *
	 * @return a incomplete JSON representation of this resource
	 * @throws JSONException
	 */
	private JSONObject toJSON() throws JSONException {
		final JSONObject jo = new JSONObject();

		jo.put("resource", (this.resource == null) ? JSONObject.NULL : this.resource);
		jo.put("resourceURI", this.resourceURI);
		jo.put("fully_loaded", this.fullyLoaded);
		jo.put("hint", this.hint);
		if (this.lastUpdate != null) {
			jo.put("last_update", this.lastUpdate);
		}
		return jo;
	}

	/**
	 * Returns a String representation.
	 *
	 * {@stickyInfo <p>The output format is subject to change, but the following may be regarded as typical:</p>
	 * {@source qkai.model.db.Resource({"last_update":"Thu Apr 09 21:54:26 CEST 2009","resourceURI":"http://.../resource/Hanover","resource":4,"fully_loaded":true})}
	 * }
	 *
	 * @return String representation of this instance.
	 */
	@Override
	public String toString() {
		try {
			return "qkai.model.db.Resource(" + this.toJSON().toString() + ")";
		} catch (Exception e) {
			return "qkai.model.db.Resource({\"resource\": " + ((this.resource == null) ? "null" : this.resource) + "})";
		}
	}

}
