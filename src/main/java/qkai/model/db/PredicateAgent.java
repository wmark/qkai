package qkai.model.db;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

//~--- non-JDK imports --------------------------------------------------------

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Agent for predicates ({@code Resources} with special meaning).
 *
 * <p>Used to hold a list of predicate IDs in memory to ease construction of queries.</p>
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public final class PredicateAgent {

	private static final Logger log = LoggerFactory.getLogger(PredicateAgent.class);
	private static final Map<String, Long> memory = new HashMap<String, Long>();

	/** Constructor hidden. */
	private PredicateAgent() {
		// intentionally left blank;
	}

	/**
	 * Gets the ID of a predicate identified by URI.
	 *
	 * Will create the predicate as {@code Resource} if it does not exist.
	 *
	 * <p>Prefer calling this to doing expensive queries like {@literal predicate = ANY (SELECT ...)}.</p>
	 *
	 * @param uri URI as identifier of the predicate
	 * @return its ID
	 */
	public static long getPredicateValueOf(final String uri) {
		if (memory.containsKey(uri)) {
			log.debug("Served from memory: {}", uri);
			return memory.get(uri);
		} else {
			log.debug("Not in memory: {}", uri);

			final Resource r = Resource.valueOf(uri, Resource.ContentHint.PROPERTY);

			memory.put(uri, r.getID());
			log.debug("Served after creating: {}", uri);
			return r.getID();
		}
	}

	/**
	 * Gets the IDs of predicates identified by URIs.
	 *
	 * @param uris collection of URIs
	 * @return list of IDs
	 */
	public static List<Long> getPredicateValueOf(final List<String> uris) {
		final List<Long> r = new ArrayList<Long>(uris.size());
		for (String uri : uris) {
			r.add(getPredicateValueOf(uri));
		}
		return r;
	}

}
