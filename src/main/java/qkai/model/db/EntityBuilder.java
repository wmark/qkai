package qkai.model.db;

/**
 * Supertype for model builders.
 *
 * {@stickyInfo This is needed in {@link qkai.model.kb.ResponseInCommonFormat} for collections
 * of the builder implementing this interface. Otherwise it would be needed to create data classes,
 * which wouldn't be as reusable as this.}
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public abstract class EntityBuilder<T> {

	/** 
	 * Must be overriden by a more specific constructor.
	 */
	EntityBuilder() {
		// intentionally empty
	}

	/**
	 * Builds an actual instance of the {@code Entity} {@code T}.
	 *
	 * @warning Run from a {@code synchronized} context if you expect to create properties for a {@code Resource} concurrently.
	 * @return an existing and persisted {@code Entity}
	 */
	public abstract T build();
}
