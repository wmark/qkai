package qkai.model.db;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import javax.persistence.Table;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONException;
import org.json.JSONObject;

import qkai.logic.service.PersistenceService;

/**
 * Representation of a RDF resource's leaf.
 *
 * <p>Also referred to as <em>predicate</em> or in special cases <em>verb</em>.
 * See {@link Relation} for another representation form.</p>
 *
 * <p>It is immutable, instance-controlled and thread-safe, with the exceptions being the collections which are meant to be managed
 * by persistency or converters only.</p>
 *
 * {@stickyInfo Obtain instances by the static factory method {@link #valueOf}.}
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
@Entity
@Table(name = "properties")
@NamedQueries( {
	@NamedQuery(name = "Property.findAll", query = "SELECT p FROM Property p") ,
	@NamedQuery(name = "Property.findByProperty", query = "SELECT p FROM Property p WHERE p.property = :property") ,
	@NamedQuery(name = "Property.valueOf",
				query = "SELECT p FROM Property p WHERE p.resource = :resource AND p.predicate = :predicate AND p.ptype = :ptype AND p.pvalue = :pvalue AND p.lang IS NULL") ,
	@NamedQuery(name = "Property.valueOfWithLang",
				query = "SELECT p FROM Property p WHERE p.resource = :resource AND p.predicate = :predicate AND p.ptype = :ptype AND p.pvalue = :pvalue AND p.lang = :lang") ,
	@NamedQuery(name = "Property.findByPtype", query = "SELECT p FROM Property p WHERE p.ptype = :ptype")
})
public final class Property implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(name = "lang", length = 2)
	private String lang;
	@JoinColumn(name = "predicate", referencedColumnName = "resource")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Resource predicate;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "property")
	private Long property;
	@Basic(optional = false)
	@Column(
		name = "ptype",
		nullable = false,
		length = 16
	)
	private String ptype;
	@Basic(optional = false)
	@Lob
	@Column(name = "pvalue", nullable = false)
	private String pvalue;
	@JoinColumn(name = "resource", referencedColumnName = "resource")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Resource resource;

	/** Private constructor for persistence only. */
	private Property() {}

	/** Package-friendly constructor solely meant for tests. */
	Property(final Resource resource, final Resource predicate, final String ptype, final String pvalue, final String lang) {
		this.setResource(resource);
		this.setPredicate(predicate);
		this.ptype = ptype;
		this.pvalue = pvalue;
		this.lang = lang;
	}

	/**
	 * Gets an existing instance.
	 *
	 * <p>If this fails with an exception it will be safe to create a new instance without further checking whether it already exists.</p>
	 *
	 * @param resource already persistent instance of Resource
	 * @param predicate already persistent instance of Resource, with {@code ContentHint = PROPERTY}
	 * @param ptype property's type (literal, uri...)
	 * @param pvalue actual value of this property
	 * @param lang language of value, may be null
	 * @return already persisting instance
	 * @throws javax.persistence.NoResultException Marks the case where it is safe to create a new instance.
	 */
	private static Property getByQuery(final Resource resource, final Resource predicate, final String ptype, final String pvalue,
									   final String lang)
			throws javax.persistence.NoResultException {
		final EntityManager em = PersistenceService.getInstance().getEntityManager();
		Query q;

		if (lang == null) {
			q = em.createNamedQuery("Property.valueOf");
		} else {
			q = em.createNamedQuery("Property.valueOfWithLang");
			q.setParameter("lang", lang);
		}
		q.setParameter("resource", resource);
		q.setParameter("predicate", predicate);
		q.setParameter("ptype", ptype);
		q.setParameter("pvalue", pvalue);
		q.setMaxResults(1);
		return (Property) q.getSingleResult();
	}

	/**
	 * Gets an instance, creates one should it not exist.
	 *
	 * <p>Business logic needs to retrieve instances of this from database without bothering whether it existed or not.
	 * This static factory method realizes instance-controll and guarantees instances are being immutable.
	 * Plus it replaces a builder for making sure this Bean is always in consistent state.</p>
	 *
	 * @warning Run from a {@code synchronized} context if you expect to create properties for a {@code Resource} concurrently.
	 * @param resource already persistent instance of Resource
	 * @param predicate already persistent instance of Resource, with {@code ContentHint = PROPERTY}
	 * @param ptype property's type (literal, uri...)
	 * @param pvalue actual value of this property
	 * @param lang language of value, may be null
	 * @return existing and persisting instance of this model
	 * @throws IllegalArgumentException if one of the arguments (except lang) is null
	 */
	public static Property valueOf(final Resource resource, final Resource predicate, final String ptype, final String pvalue,
								   final String lang) {
		if ((resource == null) || (predicate == null) || (ptype == null) || (pvalue == null)) {
			throw new IllegalArgumentException("None of the arguments (except lang) can be null.");
		}
		assert predicate.getHint() == Resource.ContentHint.PROPERTY;
		try {
			return Property.getByQuery(resource, predicate, ptype, pvalue, lang);
		} catch (javax.persistence.NoResultException e) {
			return PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Property>() {
				@Override
				public Property run() {
					final Property w = new Property(resource, predicate, ptype, pvalue, lang);

					em.persist(w);
					return w;
				}
			});
		}
	}

	/**
	 * Gets an instance based on the builder's internal representation.
	 *
	 * @param builder from which data shall be taken from
	 * @return existing and persisting instance of this model
	 */
	private static Property valueOf(final PropertyBuilder builder) {
		Resource r;

		if (builder.resource == null) {
			r = Resource.valueOf(builder.resourceURI, Resource.ContentHint.NODE);
		} else {
			r = builder.resource;
		}
		return Property.valueOf(r, Resource.valueOf(builder.predicateURI, Resource.ContentHint.PROPERTY), builder.ptype, builder.pvalue,
								builder.lang);
	}

	/**
	 * Gets the <em>primary key</em> of this {@code Entity}.
	 *
	 * @return primary key of this {@code Entity}
	 * @throws NullPointerException if a primary key has not been assigned, yet
	 */
	public long getID() {
		return property;
	}

	/**
	 * Gets the type of this property's value.
	 *
	 * <p>For example, {@code uri}, {@code literal} or {@code typed-literal} are such.</p>
	 *
	 * @return value's type
	 */
	public String getPtype() {
		return ptype;
	}

	/**
	 * Gets this property's value.
	 *
	 * @return value of this property
	 */
	public String getPvalue() {
		return pvalue;
	}

	/**
	 * Gets the language of this property's value.
	 *
	 * @warning Do not rely on caseing: it depends on the knowledge base this property has been obtained from.
	 * @return language as country abbreviation, such as <em>en</em> or <em>de</em>
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * Gets the {@code Resource} this property is for.
	 *
	 * @return {@code Resource} which holds this property
	 */
	public Resource getResource() {
		return resource;
	}

	void setResource(final Resource resource) {
		if (this.resource != null) {
			this.resource.propertyCollection.remove(this);
		}
		this.resource = resource;
		resource.addProperty(this);
	}

	/**
	 * Gets the predicate of this property as {@code Resource}.
	 *
	 * @return {@code Resource} as predicate
	 */
	public Resource getPredicate() {
		return predicate;
	}

	void setPredicate(final Resource predicate) {
		assert predicate.getHint() == Resource.ContentHint.PROPERTY;
		if (this.predicate != null) {
			this.predicate.propertyAsPredicateCollection.remove(this);
		}
		this.predicate = predicate;
		predicate.addPropertyAsPredicate(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int hash = 7;

		hash = 31 * hash + resource.hashCode();
		hash = 31 * hash + predicate.hashCode();
		hash = 31 * hash + ptype.hashCode();
		hash = 31 * hash + pvalue.hashCode();
		hash = (lang == null) ? hash : 31 * hash + lang.hashCode();
		return hash;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Property)) {
			return false;
		}

		final Property other = (Property) o;

		return ((property != null) && property.equals(other.property))
			   || ((pvalue != null) && pvalue.equals(other.pvalue) && (resource != null) && resource.equals(other.resource)
				   && predicate.equals(other.predicate)
				   && (((lang == null) && (other.lang == null)) || ((lang != null) && lang.equals(other.lang))));
	}

	/**
	 * This is subject to change and not meant for public use as creation is expensive.
	 * Please use JAXB converters for obtaining a complete JSON representation.
	 *
	 * @return a incomplete JSON representation of this resource
	 * @throws JSONException
	 */
	private JSONObject toJSON() throws JSONException {
		final JSONObject jo = new JSONObject();

		jo.put("resource", (this.resource == null) ? JSONObject.NULL : this.resource.getID());
		jo.put("predicate", (this.predicate == null) ? JSONObject.NULL : this.predicate.getID());
		jo.put("ptype", this.ptype);
		jo.put("pvalue", this.pvalue);
		if (this.lang != null) {
			jo.put("lang", this.lang.toUpperCase());
		}
		return jo;
	}

	/**
	 * Returns a String representation.
	 *
	 * {@stickyInfo <p>The output format is subject to change, but the following may be regarded as typical:</p>
	 * {@source qkai.model.db.Property({"pURI":"http://.../...","resource":4,"pvalue":"XYZ","lang":"EN","ptype":"literal"})}
	 * }
	 *
	 * @return String representation of this instance.
	 */
	@Override
	public String toString() {
		try {
			return "qkai.model.db.Property(" + this.toJSON().toString() + ")";
		} catch (Exception e) {
			return "qkai.model.db.Property({\"property\": " + ((this.property == null) ? "null" : this.property) + "})";
		}
	}

	/**
	 * Builder to {@code Property}s.
	 *
	 * @info This was introduced to make data classes dispensable, hence the public fields.
	 * @author              W-Mark Kubacki
	 * @version             $Revision$
	 */
	public static class PropertyBuilder extends EntityBuilder<Property> {

		public final String lang;
		public final String predicateURI;
		public final String ptype;
		public final String pvalue;
		private Resource resource;
		public final String resourceURI;

		/**
		 * Constructs an instance of {@code PropertyBuilder}.
		 *
		 * @info This constructor does not build the actual {@code Entity}, therefore this instance can still be used otherwise.
		 * @see Resource#valueOf
		 * @see Property#valueOf
		 */
		public PropertyBuilder(final String resourceURI, final String predicateURI, final String ptype, final String pvalue,
							   final String lang) {
			this.resourceURI = resourceURI;
			this.predicateURI = predicateURI;
			this.ptype = ptype;
			this.pvalue = pvalue;
			this.lang = lang;
		}

		/**
		 * Sets a pre-instantiated {@code Resource} for performance increase.
		 *
		 * @param resource instance of {@code Resource}
		 * @return this updated builder
		 */
		public PropertyBuilder resource(final Resource resource) {
			this.resource = resource;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Property build() {
			return Property.valueOf(this);
		}

	}

}
