package qkai.model.db;

import java.io.Serializable;

import java.net.URI;

import java.util.Collection;
import java.util.Collections;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Representation of a search token.
 *
 * <p>It is immutable and thread-safe, with the exception being the collection which is meant to be managed by
 * persistency or converters only.</p>
 *
 * @impl For persisting links to resources as well as sieveing out already known token in {@link qkai.logic.fetch.FoundTokenSieve}.
 * @warning Do not merge instances of this yourself to persistence context: That will be done in by searching and fetching services for you.
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
@Entity
@Table(name = "search_token", uniqueConstraints = {@UniqueConstraint(columnNames = {"token", "kb"}) })
@NamedQueries( {
	@NamedQuery(name = "SearchToken.findAll", query = "SELECT s FROM SearchToken s") ,
	@NamedQuery(name = "SearchToken.findByTid", query = "SELECT s FROM SearchToken s WHERE s.tid = :tid") ,
	@NamedQuery(name = "SearchToken.countByToken",
				query = "SELECT COUNT(s) FROM SearchToken s WHERE s.token = :token AND s.kb = :kb_name") ,
	@NamedQuery(name = "SearchToken.findByToken", query = "SELECT s FROM SearchToken s WHERE s.token = :token")
})
public final class SearchToken implements Serializable, Comparable<SearchToken> {

	private static final long serialVersionUID = 1L;
	@Column(
		name = "kb",
		nullable = false,
		length = 16
	)
	private String kb;
	@ManyToMany(mappedBy = "searchTokenCollection", fetch = FetchType.LAZY)
	Collection<Resource> resourceCollection;

	/**
	 * This is needed in {@link qkai.logic.fetch.FoundTokenSieve} to store related URIs
	 * before going to {@link qkai.logic.fetch.RelationWriterSieve} for final persisting.
	 */
	@Transient
	public Collection<URI> resourcesToBeConnectedWith;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "tid")
	private Integer tid;
	@Basic(optional = false)
	@Column(
		name = "token",
		nullable = false,
		length = 64
	)
	private String token;

	/** Private constructor for persistence only. */
	private SearchToken() {}

	/**
	 * Constructs a unserialized {@code SearchToken} with 'term' indication.
	 *
	 * @param knowledgeBaseName exact name of the knowledge base this token is meant for
	 * @param token String with the actual token
	 * @throws IllegalArgumentException if knowledgeBaseName or token is null
	 */
	public SearchToken(final String knowledgeBaseName, final String token) {
		if (knowledgeBaseName == null) {
			throw new IllegalArgumentException("knowledgeBaseName cannot be null.");
		}
		if (token == null) {
			throw new IllegalArgumentException("token cannot be null.");
		}
		this.kb = knowledgeBaseName;
		this.token = token;
	}

	/**
	 * Signalizes whether the {@code SearchToken} is due to special processing as it is not a single word.
	 *
	 * @return true, if the {@code SearchToken} is not a single word
	 */
	public boolean isTerm() {
		return token.contains(" ");
	}

	/**
	 * Gets the <em>primary key</em> of this {@code Entity}.
	 *
	 * @return primary key of this {@code Entity}
	 * @throws NullPointerException if a primary key has not been assigned, yet
	 */
	public int getID() {
		return tid.intValue();
	}

	/**
	 * Gets the search token. A word or short term.
	 *
	 * @info Might be a lone word or a short term, such as {@code University of Hanover}.
	 * @return token as String
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Gets the name of the knowledge base at which this {@code SearchToken} was used.
	 *
	 * @info The name is in short form. E.g., {@code DBPedia} and not {@code qkai.model.kb.DBPedia}.
	 * @return String name of a knowledge base
	 */
	public String getKb() {
		return kb;
	}

	/**
	 * If a SearchToken yielded a Resource, use this to express their relation as 'finding'.
	 *
	 * @param toBeAdded {@code Resource} to be added
	 */
	public void addResource(final Resource toBeAdded) {
		this.resourceCollection.add(toBeAdded);
		toBeAdded.addToSearchToken(this);
	}

	/**
	 * Gets {@code Resource}s found to be containing this {@code SearchToken}.
	 *
	 * @return {@code Resource}s in unmodifiable Collection.
	 */
	public Collection<Resource> getResourceCollection() {
		return Collections.unmodifiableCollection(resourceCollection);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int hash = 17;

		hash += (this.kb == null) ? 0 : 3 * this.kb.hashCode();
		hash += (this.token == null) ? 0 : 31 * this.token.hashCode();
		return hash;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof SearchToken)) {
			return false;
		}

		final SearchToken other = (SearchToken) o;

		return ((kb != null) && (token != null) && kb.equals(other.kb) && token.equals(other.token));
	}

	/**
	 * This is subject to change and not meant for public use as creation is expensive.
	 * Please use JAXB converters for obtaining a complete JSON representation.
	 *
	 * @return a incomplete JSON representation of this resource
	 * @throws JSONException
	 */
	private JSONObject toJSON() throws JSONException {
		final JSONObject jo = new JSONObject();

		jo.put("tid", (tid == null) ? JSONObject.NULL : tid);
		jo.put("kb", (kb == null) ? JSONObject.NULL : kb);
		jo.put("token", (token == null) ? JSONObject.NULL : token);
		return jo;
	}

	/**
	 * Returns a String representation.
	 *
	 * {@stickyInfo <p>The output format is subject to change, but the following may be regarded as typical:</p>
	 * {@source qkai.model.db.SearchToken({"token":"Hanover","kb":"dbpedia","tid":1})}
	 * }
	 *
	 * @return String representation of this instance.
	 */
	@Override
	public String toString() {
		try {
			return "qkai.model.db.SearchToken(" + this.toJSON().toString() + ")";
		} catch (Exception e) {
			return "qkai.model.db.SearchToken({\"tid\": " + ((this.tid == null) ? "null" : this.tid) + "})";
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo(final SearchToken o) {
		final int sgn = token.compareToIgnoreCase(o.token);

		if (sgn == 0) {
			return kb.compareTo(o.kb);
		} else {
			return sgn;
		}
	}

}
