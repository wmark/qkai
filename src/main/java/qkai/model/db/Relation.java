package qkai.model.db;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONException;
import org.json.JSONObject;

import qkai.logic.service.PersistenceService;

/**
 * Representation of a RDF resource's link (a special RDF property).
 *
 * <p>Also referred to as <em>predicate</em> or in special cases <em>verb</em>.
 * See {@link Property} for a non-linking representation form.</p>
 *
 * <p>It is immutable, instance-controlled and thread-safe.</p>
 *
 * {@stickyInfo Obtain instances by the static factory method {@link #valueOf}.}
 *
 * {@stickyInfo Modelling RDF the field {@code n} is not needed,
 * but in this context used for storing application values which - if they were strings - otherwise could not be used in
 * SQL aggregate functions. A use case is storing ratings.}
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
@Entity
@Table(name = "relations", uniqueConstraints = {@UniqueConstraint(columnNames = {"resource", "predicate", "target"}) })
@NamedQueries( {
	@NamedQuery(name = "Relation.find", query = "SELECT r FROM Relation r WHERE r.resource = :resource AND r.predicate = :predicate AND r.target = :target") ,
	@NamedQuery(name = "Relation.valueOf",
				query = "SELECT r FROM Relation r WHERE r.resource = :resource AND r.predicate = :predicate AND r.target = :target") ,
	@NamedQuery(name = "Relation.findByRelation", query = "SELECT r FROM Relation r WHERE r.relation = :relation") ,
	@NamedQuery(name = "Relation.findByPredicate", query = "SELECT r FROM Relation r WHERE r.predicate = :predicate")
})
public final class Relation implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic(optional = true)
	@Column(name = "n")
	private Integer n;
	@JoinColumn(name = "predicate", referencedColumnName = "resource")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Resource predicate;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "relation")
	private Long relation;
	@JoinColumn(name = "resource", referencedColumnName = "resource")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Resource resource;
	@JoinColumn(name = "target", referencedColumnName = "resource")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Resource target;

	/** Private constructor for persistence only. */
	private Relation() {}

	/** Package-friendly constructor solely meant for tests. */
	Relation(final Resource subjectR, final Resource predicate, final Resource objectR) {
		this.setResource(subjectR);
		this.setPredicate(predicate);
		this.setTarget(objectR);
	}

	/**
	 * Gets an existing instance.
	 *
	 * <p>If this fails with an exception it will be safe to create a new instance without further checking whether it already exists.</p>
	 *
	 * @param subjectR persistent instance
	 * @param predicate value of the relation
	 * @param objectR persistent instance
	 * @return Relation, already persistent.
	 * @throws javax.persistence.NoResultException Marks the case where it is safe to create a new Resource for that URI.
	 */
	private static Relation getByQuery(final Resource subjectR, final Resource predicate, final Resource objectR)
			throws javax.persistence.NoResultException {
		final EntityManager em = PersistenceService.getInstance().getEntityManager();
		final Query q = em.createNamedQuery("Relation.valueOf");

		q.setParameter("resource", subjectR);
		q.setParameter("predicate", predicate);
		q.setParameter("target", objectR);
		q.setMaxResults(1);
		return (Relation) q.getSingleResult();
	}

	/**
	 * Gets an instance, creates one should it not exist.
	 *
	 * <p>Business logic needs to retrieve instances of this from database without bothering whether it existed or not.
	 * This static factory method realizes instance-controll and guarantees instances are being immutable.
	 * Plus it replaces a builder for making sure this Bean is always in consistent state.</p>
	 *
	 * @warning Run from a {@code synchronized} context if you expect to create properties for a {@code Resource} concurrently.
	 * @param subjectR persistent instance
	 * @param predicate value of the relation
	 * @param objectR persistent instance
	 * @return existing and persisting instance of this model
	 * @throws IllegalArgumentException if one of the arguments is null
	 */
	public static Relation valueOf(final Resource subjectR, final Resource predicate, final Resource objectR) {
		if ((subjectR == null) || (predicate == null) || (objectR == null)) {
			throw new IllegalArgumentException("None of the arguments can be null.");
		}
		assert predicate.getHint() == Resource.ContentHint.PROPERTY;
		try {
			return Relation.getByQuery(subjectR, predicate, objectR);
		} catch (javax.persistence.NoResultException e) {
			return PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Relation>() {
				@Override
				public Relation run() {
					final Relation w = new Relation(subjectR, predicate, objectR);

					em.persist(w);
					return w;
				}
			});
		}
	}

	/**
	 * Gets an instance based on the builder's internal representation.
	 *
	 * @param builder from which data shall be taken from
	 * @return existing and persisting instance of this model
	 */
	private static Relation valueOf(final RelationBuilder builder) {
		return Relation.valueOf(Resource.valueOf(builder.resourceURI, Resource.ContentHint.NODE),
								Resource.valueOf(builder.predicateURI, Resource.ContentHint.PROPERTY),
								Resource.valueOf(builder.targetURI, Resource.ContentHint.NODE));
	}

	/**
	 * Gets the numerical value of this relation.
	 *
	 * <p><em>N</em> is an RDF extrinsic value.</p>
	 *
	 * @return Integer or null
	 * @throws IllegalStateException if N has not been assigned, yet
	 */
	public int getN() {
		if (n == null) {
			throw new IllegalStateException("N has not been assigned yet.");
		}
		return n;
	}

	/**
	 * Sets the RDF extrinsic value of this relation.
	 *
	 * <p>This could be a rating.</p>
	 *
	 * @param n Integer or null
	 */
	public void setN(final int n) {
		this.n = n;
	}

	/**
	 * Sets the RDF extinsic value N to null, thus clearing it.
	 */
	public void clearN() {
		this.n = null;
	}

	/**
	 * Gets the <em>primary key</em> of this {@code Entity}.
	 *
	 * @return primary key of this {@code Entity}
	 * @throws NullPointerException if a primary key has not been assigned, yet
	 */
	public long getID() {
		return relation;
	}

	/**
	 * Gets the predicate of this property as {@code Resource}.
	 *
	 * @return {@code Resource} as predicate
	 */
	public Resource getPredicate() {
		return predicate;
	}

	void setPredicate(final Resource predicate) {
		assert predicate.getHint() == Resource.ContentHint.PROPERTY;
		if (this.predicate != null) {
			this.predicate.relationAsPredicateCollection.remove(this);
		}
		this.predicate = predicate;
		predicate.addRelationAsPredicate(this);
	}

	/**
	 * Gets the target of this property as {@code Resource}.
	 *
	 * @info Such <em>targets</em> are the difference between {@code Relation}s and {@link Property}es.
	 * @return {@code Resource} as target of this {@code Relation}
	 */
	public Resource getTarget() {
		return target;
	}

	private void setTarget(final Resource target) {
		if (this.target != null) {
			this.target.relationAsTargetCollection.remove(this);
		}
		this.target = target;
		target.relationAsTargetCollection.add(this);
	}

	/**
	 * Gets the {@code Resource} this property is for.
	 *
	 * @return {@code Resource} which holds this property
	 */
	public Resource getResource() {
		return resource;
	}

	private void setResource(final Resource resource) {
		if (this.resource != null) {
			this.resource.relationAsSubjectCollection.remove(this);
		}
		this.resource = resource;
		resource.relationAsSubjectCollection.add(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int hash = 17;

		hash += (this.resource == null) ? 0 : this.resource.getID();
		hash += (this.predicate == null) ? 0 : 7 * this.predicate.getID() - 1;
		hash += (this.target == null) ? 0 : 31 * this.target.getID();
		return hash;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Relation)) {
			return false;
		}

		final Relation other = (Relation) o;

		return ((relation != null) && relation.equals(other.relation))
			   || (((resource == null) ? other.resource == null : resource.getID() == other.resource.getID())
				   && ((predicate == null) ? other.predicate == null : predicate.getID() == other.predicate.getID())
				   && ((target == null) ? other.target == null : target.getID() == other.target.getID()));
	}

	/**
	 * This is subject to change and not meant for public use as creation is expensive.
	 * Please use JAXB converters for obtaining a complete JSON representation.
	 *
	 * @return a incomplete JSON representation of this resource
	 * @throws JSONException
	 */
	private JSONObject toJSON() throws JSONException {
		final JSONObject jo = new JSONObject();

		jo.put("relation", (this.relation == null) ? JSONObject.NULL : this.relation);
		jo.put("resource", (this.resource == null) ? JSONObject.NULL : this.resource.getID());
		jo.put("predicate", (this.predicate == null) ? JSONObject.NULL : this.predicate.getID());
		jo.put("target", (this.target == null) ? JSONObject.NULL : this.target.getID());
		jo.put("n", (this.n == null) ? JSONObject.NULL : this.n);
		return jo;
	}

	/**
	 * Returns a String representation.
	 *
	 * {@stickyInfo <p>The output format is subject to change, but the following may be regarded as typical:</p>
	 * {@source qkai.model.db.Relation({"predicate":30,"target":12,"resource":4,"relation":331,"n":null})}
	 * }
	 *
	 * @return String representation of this instance.
	 */
	@Override
	public String toString() {
		try {
			return "qkai.model.db.Relation(" + this.toJSON().toString() + ")";
		} catch (Exception e) {
			return "qkai.model.db.Relation({\"relation\": " + ((this.relation == null) ? "null" : this.relation) + "})";
		}
	}

	/**
	 * Builder to {@code Relation}s.
	 *
	 * @info This was introduced to make data classes dispensable, hence the public fields.
	 * @author              W-Mark Kubacki
	 * @version             $Revision$
	 */
	public static class RelationBuilder extends EntityBuilder<Relation> {

		public final String predicateURI;
		public final String resourceURI;
		public final String targetURI;

		/**
		 * Constructs an instance of {@code RelationBuilder}.
		 *
		 * @info This constructor does not build the actual {@code Entity}, therefore this instance can still be used otherwise.
		 * @see Resource#valueOf
		 * @see Relation#valueOf
		 */
		public RelationBuilder(final String resourceURI, final String predicateURI, final String targetURI) {
			this.resourceURI = resourceURI;
			this.predicateURI = predicateURI;
			this.targetURI = targetURI;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Relation build() {
			return Relation.valueOf(this);
		}

	}

}
