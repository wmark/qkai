package qkai.model.kb;

import java.io.IOException;

import java.util.Hashtable;

//~--- non-JDK imports --------------------------------------------------------

import qkai.model.db.Resource;
import qkai.model.db.SearchToken;

/**
 * {@code QueryStrategy} for utilizing the various SparQL endpoints.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public abstract class AbstractQueryStrategySparql implements QueryStrategy {

	/** Needed by {@link #runQuery} to address an endpoint. */
	protected String sparqlEndpoint;

	/**
	 * Constructs this query strategy.
	 *
	 * @param endpoint HTTP endpoint to direct queries to
	 */
	protected AbstractQueryStrategySparql(final String endpoint) {
		this.sparqlEndpoint = endpoint;
	}

	/**
	 * Executes the actual query by a HTTP POST.
	 *
	 * @impl Inovked at the end of {@link #runQueryForRCT} and {@link #runQueryForGRA}.
	 * @param query The actual query to be send to the endpoint
	 * @param responseFormat Response format to be set as HTTP header
	 * @return String with response from the queried host
	 * @throws java.io.IOException if something goes wrong during fetching at transport layer
	 */
	protected String runQuery(final String query, final String responseFormat) throws IOException {
		final Hashtable<String, String> params = new Hashtable<String, String>(3);

		params.put("default-graph-uri", "http://dbpedia.org");
		params.put("format", responseFormat);
		params.put("query", query);
		try {
			return HTTPAccess.POST(sparqlEndpoint, params);
		} catch (javax.xml.ws.http.HTTPException e) {
			throw(IOException) new IOException().initCause(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String runQueryForRCT(final SearchToken needle, final String responseFormat) throws IOException {
		final String baseQuery = "select distinct ?node { ?node ?p ?c . FILTER {0}}";		// {0} being the filter expression
		final String res = baseQuery.replace("{0}", "(regex(?c, \"" + needle.getToken() + "\"))");

		return runQuery(res, responseFormat);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String runQueryForGRA(final Resource resource, final String responseFormat) throws IOException {
		final String baseQuery = "SELECT ?property ?hasValue ?isValueOf WHERE {{<{0}> ?property ?hasValue} UNION {?isValueOf ?property <{0}>}}";
		final String res = baseQuery.replace("{0}", resource.getResourceURI());

		return runQuery(res, responseFormat);
	}
}
