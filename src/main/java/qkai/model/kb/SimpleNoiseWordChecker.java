package qkai.model.kb;

/**
 * Simple concrete implementation of {@code NoiseWordChecker}.
 *
 * @warning This checker is not suitable for iconographic alphabets, such as the Chinese or Japanese.
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public final class SimpleNoiseWordChecker implements NoiseWordChecker {

	private final int minWordLength;
	private final NoiseWordContainer noiseWords;

	/**
	 * Constructs an instance expecting a word be of minimal length to be a valid word.
	 *
	 * @param minWordLength min word length for a word to not be a noise word
	 */
	public SimpleNoiseWordChecker(final int minWordLength) {
		this.minWordLength = minWordLength;
		this.noiseWords = NoiseWordContainer.INSTANCE;
	}

	/**
	 * @equivalence return word.length() <= minWordLength || noiseWords.contains(word);
	 */
	@Override
	public boolean isNoiseWord(final String word) {
		return word.length() <= minWordLength || noiseWords.contains(word);
	}

}
