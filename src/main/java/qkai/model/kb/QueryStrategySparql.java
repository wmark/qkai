package qkai.model.kb;

/**
 * {@code QueryStrategy} for utilizing the various SparQL endpoints.
 *
 * {@stickyInfo If the SparQL implementation is Virtuoso consider using {@link QueryStrategyVirtuoso} due to
 * its superior performance with no functionality loss.}
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public class QueryStrategySparql extends AbstractQueryStrategySparql {

	/**
	 * Constructs this query strategy.
	 *
	 * @param endpoint HTTP endpoint to direct queries to
	 */
	public QueryStrategySparql(final String endpoint) {
		super(endpoint);
	}
}
