package qkai.model.kb;

import java.io.IOException;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.commons.lang.StringUtils;

import qkai.model.db.SearchToken;

/**
 * {@code QueryStrategy} for utilizing the Virtuoso SparQL implementation.
 *
 * @see <a href="http://virtuoso.openlinksw.com/">Virtuoso Homepage</a>
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public class QueryStrategyVirtuoso extends AbstractQueryStrategySparql {

	/** Used to sieve out noise words in sub-terms at assemblying a query. */
	protected final NoiseWordChecker noiseWordChecker;

	/**
	 * Constructs this query strategy.
	 *
	 * @param endpoint HTTP endpoint to direct queries to
	 * @param noiseWordChecker will be used to discard noise words from query
	 */
	public QueryStrategyVirtuoso(final String endpoint, final NoiseWordChecker noiseWordChecker) {
		super(endpoint);
		this.noiseWordChecker = noiseWordChecker;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String runQueryForRCT(final SearchToken needle, final String responseFormat) throws IOException {
		final String baseQuery = "select distinct ?node { ?node ?p ?c . FILTER {0}}";		// {0} being the filter expression
		String res;

		if (needle.isTerm()) {
			final String ersatzTerm = StringUtils.join(Tokenizer.parseText(noiseWordChecker, needle.getToken()).getAcceptedToken(),
										  " and ");

			res = baseQuery.replace("{0}", "(bif:contains(?c, \"" + ersatzTerm + "\") && regex(?c, \"" + needle.getToken() + "\"))");
		} else {
			res = baseQuery.replace("{0}", "(bif:contains(?c, \"" + needle.getToken() + "\"))");
		}
		return runQuery(res, responseFormat);
	}
}
