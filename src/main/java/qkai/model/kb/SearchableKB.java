package qkai.model.kb;

import java.io.IOException;

import java.net.URI;

import java.util.Collection;

//~--- non-JDK imports --------------------------------------------------------

import qkai.model.db.Resource;
import qkai.model.db.SearchToken;

/**
 * Interface to be implemented by every knowledge base (<em>KB</em>).
 *
 * {@stickyInfo Knowledge bases are instanciated by a call to {@code newInstance()} in {@link KBFactory},
 * which is by default a member of your KB's class.}
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public interface SearchableKB {

	/**
	 * Gets the name of the KB.
	 *
	 * @info The name is in short form. E.g., {@code DBPedia} and not {@code qkai.model.kb.DBPedia}.
	 * @impl Meant for storing {@link qkai.model.db.SearchToken} along with this.
	 * @return name of the kb
	 */
	public String getName();

	/**
	 * Will yield a list of RDF resources' URIs containing a given token.
	 *
	 * @info URIs but not {@link qkai.model.db.Resource}s are returned for the caller might implement batch queries.</p>
	 * @param needle This {@code SearchToken}'s token will be queried for.
	 * @return Collection of URIs with the resources containing the token. May be empty.
	 * @throws java.io.IOException if something goes wrong during fetching at transport layer
	 */
	public Collection<URI> resourcesContainingToken(SearchToken needle) throws IOException;

	/**
	 * Gets and already creates properties of the given {@code Resource} as well as relations pointing to it.
	 *
	 * @info For a implementation example please see {@link ResponseInCommonFormat}.
	 * @param resource {@code Resource} for which adjacencies will be queries for
	 * @return Collection of {@code Resource}s representing the adjacency.
	 * @throws java.io.IOException if something goes wrong during fetching at transport layer
	 */
	public ResponseInCommonFormat getResourcesAdjacencies(Resource resource) throws IOException;

	/**
	 * Gets a {@code NoiseWordChecker} fit for this {@code SearchableKB}.
	 *
	 * @info Is at least used by {@link Tokenizer}.
	 * @return instance of {@code NoiseWordChecker}
	 */
	public NoiseWordChecker getNoiseWordChecker();
}
