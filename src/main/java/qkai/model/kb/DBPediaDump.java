package qkai.model.kb;

import java.io.IOException;
import java.io.InputStream;

import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.Query;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.tools.bzip2.CBZip2InputStream;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;
import org.openrdf.rio.Parser;
import org.openrdf.rio.StatementHandler;
import org.openrdf.rio.ntriples.NTriplesParser;
import org.openrdf.rio.rdfxml.RdfXmlParser;
import org.openrdf.rio.turtle.TurtleParser;
import org.openrdf.sesame.constants.RDFFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.logic.service.PersistenceService;

import qkai.model.db.Property.PropertyBuilder;
import qkai.model.db.Relation.RelationBuilder;
import qkai.model.db.SearchToken;

import qkai.util.ConfigObj;

/**
 * Mock KB for reading DBPedia dumps.
 *
 * <p>Currently does only accept dumps in N3 format, which is currently the preferred format.</p>
 *
 * {@stickyInfo Configure the dumps as comma-separated list and place them in {@code resources/dumps}.
 * As the files will read as stream to save memory you can safely use DBPedia's vanilla dumps.
 * Bzip2 compression is supported, but only enabled if the filename contains {@literal ".bz2"}.
 * }
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
final class DBPediaDump extends AbstractSearchableKB {

	/** Property containing a comma-separated list of the dump files. */
	public static final String DUMPS_PROPERTY = "dbpedia.dump_files";
	private static final Logger log = LoggerFactory.getLogger(DBPediaDump.class);
	private static RDFFormat format = RDFFormat.NTRIPLES;
	private static boolean dumps_loaded = false;

	/**
	 * @equivalent super(new SparqlJsonStrategy("http://dbpedia.org/sparql", new SimpleNoiseWordChecker(2), true));
	 */
	DBPediaDump() {
		super(new ResponseStrategyJson(new QueryStrategyVirtuoso("http://dbpedia.org/sparql", new SimpleNoiseWordChecker(2))));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<java.net.URI> resourcesContainingToken(final SearchToken needle) throws IOException {
		final Collection<java.net.URI> c = new LinkedList<java.net.URI>();

		if (!dumps_loaded) {
			dumps_loaded = true;

			final String[] dumps = ConfigObj.getProperties().getProperty(DUMPS_PROPERTY).split("\\,");

			try {
				for (String df : dumps) {
					String lang;

					if (df.contains("_")) {
						final int li = df.lastIndexOf("_") + 1;

						lang = df.substring(li, li + 2);
					} else {
						lang = "types";
					}

					final java.net.URI u = new java.net.URI("http://downloads.dbpedia.org/3.2/" + lang + "/" + df);

					c.add(u);
				}
			} catch (java.net.URISyntaxException e) {
				log.warn("Uncaught exception", e);
			}
		}
		return c;
	}

	/**
	 * Parses an opened dump file, adds its data to the {@code ResponseInCommonFormat}.
	 *
	 * @param is dump file as opened {@code InputStream} (won't be closed here)
	 * @param format format of the dump's contents
	 * @param resp reponse to which builders will be added
	 * @throws java.io.IOException if file cannot be read for parsing
	 */
	private void parseDump(final InputStream is, final RDFFormat format, final ResponseInCommonFormat resp) throws java.io.IOException {
		Parser parser = null;

		if (format.equals(RDFFormat.TURTLE)) {
			parser = (Parser) new TurtleParser();
		} else if (format.equals(RDFFormat.RDFXML)) {
			parser = (Parser) new RdfXmlParser();
		} else if (format.equals(RDFFormat.NTRIPLES)) {
			parser = (Parser) new NTriplesParser();
		} else {
			return;
		}
		parser.setDatatypeHandling(Parser.DT_IGNORE);
		if (PersistenceService.isConnectedToMysql()) {
			parser.setStatementHandler(new DumpHandlerMysql());
		} else {
			parser.setStatementHandler(new DumpHandler(resp));
		}
		try {
			parser.parse(is, "");
		} catch (Exception e) {
			log.error("Uncaught exception during parsing", e);
		}
	}

	/**
	 * Does the actual loading of the dumps.
	 *
	 * <p>Returns the reading only once.</p>
	 */
	@Override
	public ResponseInCommonFormat getResourcesAdjacencies(final qkai.model.db.Resource resource) throws IOException {
		final ResponseInCommonFormat resp = new ResponseInCommonFormat(16);
		String df = resource.getResourceURI();

		df = df.substring(df.lastIndexOf('/') + 1);
		log.info("Dump opened: /dumps/{}", df);

		InputStream is = DBPediaDump.class.getResourceAsStream("/dumps/" + df);

		if (df.contains(".bz2")) {
			try {
				is.read();
				is.read();
				is = new CBZip2InputStream(is);
			} catch (IOException e) {
				log.error("Dump is no valid bzip2: /dumps/" + df, e);
				throw e;
			}
		}
		try {
			parseDump(is, format, resp);
			log.info("Ending dump read of: /dumps/{}", df);
		} catch (Exception e) {
			log.error("Uncaught exception", e);
		} finally {
			log.info("Closing dump: /dumps/{}", df);
			is.close();
		}
		return resp;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return DBPediaDump.class.getSimpleName();
	}

	/**
	 * Constructor of {@link qkai.model.EntityBuilder} from triples stored in a dump.
	 *
	 * @see <a href="http://www.openrdf.org/issues/browse/SES-454">OpenRDF SES-454 describing why interface Value should not be used</a>
	 * @author      W-Mark Kubacki
	 * @version     $Revision$
	 */
	private static class DumpHandler implements StatementHandler {

		private final ResponseInCommonFormat response;

		/**
		 * Creates an instance meant for storing its results in {@code resp}.
		 *
		 * @param resp storage for the {@code EntityBuilder}.
		 */
		public DumpHandler(final ResponseInCommonFormat response) {
			super();
			this.response = response;
		}

		/**
		 * Run by the RDF parser every time a triple has been read.
		 *
		 * @warning This implementation does not support {@code BNode}s, but trivial triples.
		 * @param subj in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#NODE}
		 * @param pred in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#PROPERTY}
		 * @param obj an (yet) unknown value
		 */
		@Override
		public void handleStatement(final org.openrdf.model.Resource subj, final org.openrdf.model.URI pred, final Value obj) {
			if (obj instanceof org.openrdf.model.URI) {
				addRelation(subj.toString(), pred.toString(), ((org.openrdf.model.URI) obj).toString());
			} else {
				addProperty(subj.toString(), pred.toString(), (Literal) obj);
			}
		}

		/**
		 * Adds an identified {@link qkai.model.db.Relation}.
		 *
		 * @param subj in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#NODE}
		 * @param pred in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#PROPERTY}
		 * @param obj in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#NODE}
		 */
		private void addRelation(final String subj, final String pred, final String obj) {
			response.addBuilder(subj, new RelationBuilder(subj, pred, obj));
		}

		/**
		 * Adds an identified {@link qkai.model.db.Property}.
		 *
		 * @param subj in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#NODE}
		 * @param pred in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#PROPERTY}
		 * @param obj {@code literal} or {@code typed-literal} to be stored
		 */
		private void addProperty(final String subj, final String pred, final Literal obj) {
			if (obj.getLanguage() == null) {
				response.addBuilder(subj, new PropertyBuilder(subj, pred, "typed-literal", obj.toString(), null));
			} else {
				response.addBuilder(subj, new PropertyBuilder(subj, pred, "literal", obj.getLabel(), obj.getLanguage()));
			}
		}

	}

	/**
	 * Constructor of {@link qkai.model.EntityBuilder} from triples stored in a dump.
	 *
	 * <p>This version has MySQL optimizations and writes directly to MySQL.</p>
	 *
	 * @see <a href="http://www.openrdf.org/issues/browse/SES-454">OpenRDF SES-454 describing why interface Value should not be used</a>
	 * @author      W-Mark Kubacki
	 * @version     $Revision$
	 */
	private static class DumpHandlerMysql implements StatementHandler {

		private static final String PROPERTY_CREATE_SQL = "INSERT IGNORE INTO properties (resource,predicate,ptype,pvalue,lang) "
														  + "SELECT a.resource, b.resource predicate, ?2 ptype, ?3 pvalue, ?4 lang "
														  + "FROM resources a, resources b WHERE a.resourceURI=?1 AND b.resourceURI=?5";
		private static final String RELATION_CREATE_SQL = "INSERT IGNORE INTO relations (resource,predicate,target) "
														  + "SELECT a.resource resource, b.resource predicate, c.resource target "
														  + "FROM resources a, resources b, resources c "
														  + "WHERE a.resourceURI=?1 AND b.resourceURI=?2 AND c.resourceURI=?3";
		private static final String RESOURCE_CREATE_SQL = "INSERT IGNORE INTO resources (resourceURI,hint,fully_loaded) VALUES (?1,?2,1)";
		private String lastCreatedProperty = "";

		/**
		 * Creates an instance meant for storing its results directly in MySQL.
		 */
		public DumpHandlerMysql() {
			super();
		}

		/**
		 * Run by the RDF parser every time a triple has been read.
		 *
		 * @warning This implementation does not support {@code BNode}s, but trivial triples.
		 * @param subj in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#NODE}
		 * @param pred in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#PROPERTY}
		 * @param obj an (yet) unknown value
		 */
		@Override
		public void handleStatement(final org.openrdf.model.Resource subj, final org.openrdf.model.URI pred, final Value obj) {
			try {
				createResource(subj.toString(), qkai.model.db.Resource.ContentHint.NODE);
				createResource(pred.toString(), qkai.model.db.Resource.ContentHint.PROPERTY);
				if (obj instanceof org.openrdf.model.URI) {
					createRelation(subj.toString(), pred.toString(), ((org.openrdf.model.URI) obj).toString());
				} else {
					createProperty(subj.toString(), pred.toString(), (Literal) obj);
				}
			} catch (Exception e) {
				log.warn("Skipping line due to: ", e);
			}
		}

		/**
		 * Creates a {@link qkai.model.db.Resource} if it does not exist.
		 *
		 * Creation is done directly through MySQL native function, bypassing {@code Resource.valueOf}.
		 *
		 * @param resourceURI URI of the {@code Resource} to be created.
		 * @param hint RDF extrinsic hint about this {@code Resource}'s content
		 */
		private void createResource(final String resourceURI, final qkai.model.db.Resource.ContentHint hint) {
			assert resourceURI != null;
			assert hint != null;
			if ((hint == qkai.model.db.Resource.ContentHint.PROPERTY) && lastCreatedProperty.equals(resourceURI)) {
				return;
			} else {
				lastCreatedProperty = resourceURI;
			}

			final Query resourceCreateQuery = PersistenceService.getInstance().getEntityManager().createNativeQuery(RESOURCE_CREATE_SQL);

			resourceCreateQuery.setParameter("1", resourceURI);
			resourceCreateQuery.setParameter("2", hint.toString());
			PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {
				@Override
				public Boolean run() {
					resourceCreateQuery.executeUpdate();
					return true;
				}
			});
		}

		/**
		 * Creates an identified {@link qkai.model.db.Relation}.
		 *
		 * @param subj in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#NODE}
		 * @param pred in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#PROPERTY}
		 * @param obj in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#NODE}
		 */
		private void createRelation(final String subj, final String pred, final String obj) {
			createResource(obj, qkai.model.db.Resource.ContentHint.NODE);

			final Query relationCreateQuery = PersistenceService.getInstance().getEntityManager().createNativeQuery(RELATION_CREATE_SQL);

			relationCreateQuery.setParameter("1", subj);
			relationCreateQuery.setParameter("2", pred);
			relationCreateQuery.setParameter("3", obj);
			PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {
				@Override
				public Boolean run() {
					relationCreateQuery.executeUpdate();
					return true;
				}
			});
		}

		/**
		 * Creates an identified {@link qkai.model.db.Property}.
		 *
		 * @param subj in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#NODE}
		 * @param pred in qKAI, a {@link qkai.model.db.Resource} with {@link qkai.model.db.Resource.ContentHint#PROPERTY}
		 * @param obj {@code literal} or {@code typed-literal} to be stored
		 */
		private void createProperty(final String subj, final String pred, final Literal obj) {
			final Query propertyCreateQuery = PersistenceService.getInstance().getEntityManager().createNativeQuery(PROPERTY_CREATE_SQL);

			propertyCreateQuery.setParameter("1", subj);
			propertyCreateQuery.setParameter("5", pred);
			if (obj.getLanguage() == null) {
				propertyCreateQuery.setParameter("2", "typed-literal");
				propertyCreateQuery.setParameter("3", obj.toString());
				propertyCreateQuery.setParameter("4", null);
			} else {
				propertyCreateQuery.setParameter("2", "literal");
				propertyCreateQuery.setParameter("3", obj.getLabel());
				propertyCreateQuery.setParameter("4", obj.getLanguage());
			}
			PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {
				@Override
				public Boolean run() {
					propertyCreateQuery.executeUpdate();
					return Boolean.TRUE;
				}
			});
		}

	}
}
