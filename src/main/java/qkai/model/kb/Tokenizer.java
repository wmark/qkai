package qkai.model.kb;

import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

//~--- non-JDK imports --------------------------------------------------------

import qkai.model.db.SearchToken;

/**
 * Converter of {@code String}s into valid tokens for search.
 *
 * {@example "Example usage"
 * NoiseWordChecker nwc = new SimpleNoiseWordChecker(2);
 * TokenizerResult result = Tokenizer.parseText(nwc, "mars venus \"milky way\" sun");
 * Set<String> tokens = result.getAcceptedToken();
 * // the tokens are: [mars, sun, milky way, venus]
 * }
 *
 * {@stickyInfo Here "token" is defined as either a lone word surrounded by whitespace, or as a series
 * of words surrounded by double quotes, "like this"; also, very common
 * words (and, the, etc.) do not qualify as possible search targets.}
 *
 * {@stickyNote The <i>ersatz</i> regular expressions - without quotation support - are:
 * <dl>
 *   <dt>token</dt><dd>{@code (?:(?<!\-)\-?[\w\d]+)+ }</dd>
 *   <dt>term</dt><dd>{@code ((?:\s*(?:(?<!\-)\-?[\w\d]+)+)+) }</dd>
 * </dl>
 * }
 *
 * @see         <a href="http://www.javapractices.com/topic/TopicAction.do?Id=87">Java Practices: Parse text</a>
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public final class Tokenizer {

	private static final String fDOUBLE_QUOTE = "\"";
	private static final String fQUOTES_ONLY = "\"";
	private static final String fWHITESPACE_AND_QUOTES = " \t\r\n\"";

	private Tokenizer() {}

	/**
	 * Parses the text into tokens, sieveing out noise words.
	 *
	 * @warning Does not preserve any word order, except inside quotated sub-terms.
	 * @param kbName is meant to be non-null for creating concrete {@link SearchToken}
	 * @param nwc used to determine common words, may be null
	 * @param text is non-null, but may have no content, and represents what the user has input in a search box
	 * @return tokens from parsing, segregated by category
	 */
	public static TokenizerResult parseText(final String kbName, final NoiseWordChecker nwc, final String text) {
		if (text == null) {
			throw new IllegalArgumentException("Text cannot be null.");
		}

		final boolean returnTokens = true;
		String currentDelims = fWHITESPACE_AND_QUOTES;
		final StringTokenizer parser = new StringTokenizer(text, currentDelims, returnTokens);
		String token = null;
		final Set<SearchToken> acceptedToken = new TreeSet<SearchToken>();
		final Set<String> acceptedTokenAsString = new TreeSet<String>();
		final Set<String> noiseWords = new TreeSet<String>();
		final Set<String> rejectedToken = new TreeSet<String>();

		while (parser.hasMoreTokens()) {
			token = parser.nextToken(currentDelims);
			if (isDoubleQuote(token)) {
				currentDelims = flipDelimiters(currentDelims);
			} else {
				token = token.trim();
				if (textHasContent(token)) {
					if (((nwc != null) && nwc.isNoiseWord(token))
							|| (!currentDelims.equals(fWHITESPACE_AND_QUOTES)
								&& (Tokenizer.parseText(nwc, token).getAcceptedToken().size() <= 0))) {
						noiseWords.add(token);
					} else {
						if (kbName == null) {
							acceptedTokenAsString.add(token);
						} else {
							acceptedToken.add(new SearchToken(kbName, token));
						}
					}
				}
			}
		}
		if (kbName == null) {
			return new TokenizerResult(acceptedToken, noiseWords, rejectedToken, acceptedTokenAsString);
		} else {
			return new TokenizerResult(acceptedToken, noiseWords, rejectedToken);
		}
	}

	/**
	 * Parses the text into tokens, sieveing out noise words.
	 *
	 * @warning Does not preserve any word order, except inside quotated sub-terms.
	 * @param nwc is non-null, and used to determine common words
	 * @param text is non-null, but may have no content, and represents what the user has input in a search box
	 * @return tokens from parsing, segregated by category
	 */
	public static TokenizerResult parseText(final NoiseWordChecker nwc, final String text) {
		return parseText(null, nwc, text);
	}

	private static boolean textHasContent(final String aText) {
		return (aText != null) && (!aText.equals(""));
	}

	private static boolean isDoubleQuote(final String aToken) {
		return aToken.equals(fDOUBLE_QUOTE);
	}

	private static String flipDelimiters(final String aCurrentDelims) {
		String result = null;

		if (aCurrentDelims.equals(fWHITESPACE_AND_QUOTES)) {
			result = fQUOTES_ONLY;
		} else {
			result = fWHITESPACE_AND_QUOTES;
		}
		return result;
	}

}
