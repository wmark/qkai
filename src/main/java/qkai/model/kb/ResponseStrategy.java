package qkai.model.kb;

import java.io.IOException;

import java.net.URI;

import java.util.Collection;

//~--- non-JDK imports --------------------------------------------------------

import qkai.model.db.Resource;
import qkai.model.db.SearchToken;

/**
 * Strategy to convert response formats of KB queries in the common format.
 *
 * <p>Meant to be used in conjunction with {@link QueryStrategy}.</p>
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public interface ResponseStrategy {

	/**
	 * Will yield a list of RDF resources' URIs containing a given token.
	 *
	 * @info URIs but not {@link qkai.model.db.Resource}s are returned for the caller might implement batch queries.</p>
	 * @param needle This {@code SearchToken}'s token will be queried for.
	 * @return Collection of URIs with the resources containing the token. May be empty.
	 * @throws java.io.IOException if something goes wrong during fetching at transport layer
	 */
	public Collection<URI> resourcesContainingToken(SearchToken needle) throws IOException;

	/**
	 * Gets properties of the given {@code Resource} as well as relations pointing to it and does create the necessary models.
	 *
	 * @param resource {@code Resource} for which adjacencies will be queries for
	 * @return instance of {@code ResponseInCommonFormat} containing the fetched adjacencies
	 * @throws java.io.IOException if something goes wrong during fetching at transport layer
	 */
	public ResponseInCommonFormat getResourcesAdjacencies(Resource resource) throws IOException;
}
