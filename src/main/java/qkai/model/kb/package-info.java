/**
 * Classes concerning knowledge bases (<em>kb</em>) are here.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
package qkai.model.kb;
