package qkai.model.kb;

import java.io.IOException;

//~--- non-JDK imports --------------------------------------------------------

import qkai.model.db.Resource;
import qkai.model.db.SearchToken;

/**
 * Implementations of this represent various ways of querying remote KB.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public interface QueryStrategy {

	/**
	 * Helper function to {@link SearchableKB#getResourcesAdjacencies}.
	 *
	 * @param resource {@code Resource} for which adjacencies will be queries for
	 * @param responseFormat Response format to be set as HTTP header
	 * @return String with response from the queried host
	 * @throws java.io.IOException if something goes wrong during fetching at transport layer
	 */
	String runQueryForGRA(Resource resource, String responseFormat) throws IOException;

	/**
	 * Helper function to {@link SearchableKB#resourcesContainingToken}.
	 *
	 * <p>The query's result format must be equivalent to that of "{@code select distinct ?node}" (in SparQL).</p>
	 * <p>This will utilize the endpoint's SparQL extensions.</p>
	 *
	 * @param needle This SearchToken's token will be queried for
	 * @param responseFormat Response format to be set as HTTP header
	 * @return String with response from the queried host
	 * @throws java.io.IOException if something goes wrong during fetching at transport layer
	 */
	String runQueryForRCT(SearchToken needle, String responseFormat) throws IOException;
}
