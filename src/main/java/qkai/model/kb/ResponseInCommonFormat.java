package qkai.model.kb;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

//~--- non-JDK imports --------------------------------------------------------

import qkai.model.db.EntityBuilder;

/**
 * Representation of the common data format returned by {@link ResponseStrategy}s.
 *
 * <p>Such is needed as conversion result from JSON, XML, N3 and friends.
 * Else it would impose additional creating of processing strategies.</p>
 *
 * {@example "Sample Usage 1 - in ResponseStrategy"
 * // make an educated guess about the amount of resources
 * int n = queryResult.getProperties().size() / 2 + 1;
 * ResponseInCommonFormat myResponse = new ResponseInCommonFormat(n);
 * for (myProperty p : queryResult.getProperties()) {
 *  if (p.isTransitive()) {
 *      response.addBuilder(resourceURI, new RelationBuilder(...));
 *  } else {
 *      response.addBuilder(resourceURI, new PropertyBuilder(...));
 *  }
 * }
 * // do something with myResponse; e.g., pass it to a SearchableKB
 * }
 * {@example "Sample Usage 2 - using in consumers"
 * ResponseInCommonFormat response = responseStrategy.getResourcesAdjacencies(resource);
 * Collection<Resource> result = new LinkedList<Resource>();
 *
 * for (String resourceURI : response.getDescribedResources()) {
 *  Resource r = Resource.valueOf(resourceURI, Resource.ContentHint.NODE);
 *  for (EntityBuilder e : response.getBuilderFor(resourceURI)) {
 *      e.build();
 *  }
 *  result.add(r);
 * }
 * return result;
 * }
 *
 * @see ResponseStrategy#getResourcesAdjacencies
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public final class ResponseInCommonFormat {

	private final Map<String, Collection<EntityBuilder>> builderMap;

	/**
	 * Constructs an instance of {@code ResponseInCommonFormat}.
	 *
	 * @param initialSize should correspond to the estimated number of {@code Resource}s in the response
	 */
	public ResponseInCommonFormat(final int initialSize) {
		this.builderMap = new HashMap<String, Collection<EntityBuilder>>(initialSize);
	}

	/**
	 * Adds an {@code EntityBuilder} from the response for later processing at {@code SearchableKB}s.
	 *
	 * @param <T> generic to convey an {@code EntityBuilder} from
	 * @param resourceURI URI which points to the {@code Resource} having the specified property
	 * @param builder {@code EntityBuilder} of a property representation
	 */
	public <T extends EntityBuilder> void addBuilder(final String resourceURI, final T builder) {
		if (!builderMap.containsKey(resourceURI)) {
			builderMap.put(resourceURI, new LinkedList<EntityBuilder>());
		}
		builderMap.get(resourceURI).add(builder);
	}

	/**
	 * Gets the set of {@code Resource}s described by this response.
	 *
	 * @return unmodifiable {@code Set} with the URIs of {@code Resource}s.
	 */
	public Set<String> getDescribedResources() {
		return Collections.unmodifiableSet(builderMap.keySet());
	}

	/**
	 * Gets the builder associated with a {@code Resource}'s URI.
	 *
	 * @param resourceURI URI which points to the {@code Resource} having the specified property
	 * @return unmodifiable {@code Set} with the builder
	 */
	public Collection<EntityBuilder> getBuilderFor(final String resourceURI) {
		return Collections.unmodifiableCollection(builderMap.get(resourceURI));
	}

}
