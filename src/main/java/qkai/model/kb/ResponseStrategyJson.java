package qkai.model.kb;

import java.io.IOException;

import java.net.URI;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.model.db.Property.PropertyBuilder;
import qkai.model.db.Relation.RelationBuilder;
import qkai.model.db.Resource;
import qkai.model.db.SearchToken;

/**
 * Concrete implementation of {@code ResponseStrategy} for transforming JSON responses.
 *
 * <p>Does utilize a {@link QueryStrategy}.</p>
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public class ResponseStrategyJson implements ResponseStrategy {

	private static final String HAS_VALUE = "hasValue";
	private static final String IS_VALUE_OF = "isValueOf";
	private static final String NODE = "node";
	private static final String PROPERTY = "property";
	private static final String TYPE = "type";
	private static final String TYPE_URI = "uri";
	private static final String VALUE = "value";
	private static final Logger log = LoggerFactory.getLogger(ResponseStrategyJson.class);
	protected final QueryStrategy queryStrategy;
	private final String responseFormat;

	/**
	 * Constructs this {@code ProcessingStrategy}.
	 *
	 * @param queryStrategy used to launch queries which responses this strategy will process
	 */
	public ResponseStrategyJson(final QueryStrategy queryStrategy) {
		this.queryStrategy = queryStrategy;
		this.responseFormat = "application/sparql-results+json";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<URI> resourcesContainingToken(final SearchToken needle) throws IOException {
		JSONObject response;

		try {
			response = new JSONObject(queryStrategy.runQueryForRCT(needle, responseFormat));
		} catch (JSONException e) {
			throw new IOException(e);
		}

		final Collection<URI> result = new LinkedList<URI>();

		try {
			final JSONArray rows = response.getJSONObject("results").getJSONArray("bindings");
			JSONObject node;

			for (int i = 0; i < rows.length(); i++) {
				node = rows.getJSONObject(i).getJSONObject(NODE);
				if (node.getString(TYPE).equals(TYPE_URI)) {
					try {
						result.add(new URI(node.getString(VALUE)));
					} catch (java.net.URISyntaxException e) {
						log.debug("Uncaught exception; response contains malformed URI?", e);
					}
				}
			}
		} catch (JSONException e) {
			log.debug("Uncaught exception; malformed response?", e);
		}
		return Collections.unmodifiableCollection(result);
	}

	/**
	 * Scans a value for unwanted content in order to discard it, in case it is.
	 *
	 * @info In DBPedia this are values containing CSS style informations of the examined info-boxes.
	 * @param value value of RDF {@code Property}
	 * @return true, if the value contains content not considered to be informative to a human
	 */
	protected static boolean hasUnwantedContent(final String value) {
		return (value == null) || value.equals(".") || value.contains("style=");
	}

	/**
	 * Transforms JSON response into the common format.
	 *
	 * @impl This is used by {@link #getResourcesAdjacencies} after invoking {@link QueryStrategy#runQueryForGRA}.
	 * @param resource Reference {@code Resource} as the JSON respons omits it, for creating instances of builders
	 * @param rows the part of the JSON response which contains the actual data
	 * @return response in the common format
	 */
	protected ResponseInCommonFormat resourcesFromJSON(final Resource resource, final JSONArray rows) {
		final ResponseInCommonFormat response = new ResponseInCommonFormat(rows.length() / 2 + 1);

		for (int i = 0; i < rows.length(); i++) {
			try {
				final JSONObject node = rows.getJSONObject(i);
				final String predicateURI = node.getJSONObject(PROPERTY).getString(VALUE);

				if (node.has(HAS_VALUE)) {
					final JSONObject leaf = node.getJSONObject(HAS_VALUE);

					if (leaf.getString(TYPE).equals(TYPE_URI)) {
						response.addBuilder(resource.getResourceURI(),
											new RelationBuilder(resource.getResourceURI(), predicateURI, leaf.getString(VALUE)));
					} else if (!hasUnwantedContent(leaf.getString(VALUE))) {
						response.addBuilder(resource.getResourceURI(),
											new PropertyBuilder(resource.getResourceURI(), predicateURI, leaf.getString(TYPE),
												leaf.getString(VALUE), leaf.has("xml:lang") ? leaf.getString("xml:lang") : null));
					}
				} else if (node.has(IS_VALUE_OF)) {
					final String r = node.getJSONObject(IS_VALUE_OF).getString(VALUE);

					response.addBuilder(r, new RelationBuilder(r, predicateURI, resource.getResourceURI()));
				}
			} catch (JSONException e) {
				log.debug("Uncaught exception", e);
			}
		}
		return response;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResponseInCommonFormat getResourcesAdjacencies(final Resource resource) throws IOException {
		JSONObject response;

		try {
			response = new JSONObject(queryStrategy.runQueryForGRA(resource, responseFormat));
		} catch (JSONException e) {
			throw new IOException(e);
		}

		JSONArray rows;

		try {
			rows = response.getJSONObject("results").getJSONArray("bindings");
		} catch (JSONException e) {
			log.debug("Uncaught exception, returning empty response", e);
			return new ResponseInCommonFormat(0);
		}
		return resourcesFromJSON(resource, rows);
	}

}
