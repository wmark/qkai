package qkai.model.kb;

/**
 * Concrete representation of DBPedia as KB.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
final class DBPedia extends AbstractSearchableKB {

	/**
	 * @equivalent super(new SparqlJsonStrategy("http://dbpedia.org/sparql", new SimpleNoiseWordChecker(2), true));
	 */
	DBPedia() {
		super(new ResponseStrategyJson(new QueryStrategyVirtuoso("http://dbpedia.org/sparql", new SimpleNoiseWordChecker(2))));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return DBPedia.class.getSimpleName();
	}
}
