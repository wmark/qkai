package qkai.model.kb;

import java.util.HashSet;
import java.util.Set;

/**
 * Container for noise words.
 *
 * <p>These are words disregarded for searches. Some of them might even break search query.</p>
 *
 * {@stickyNote Meant to be used by every knowldege base implementing {@link qkai.model.kb.SearchableKB}.}
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public enum NoiseWordContainer {

	/** The only instance of this Singleton. */
	INSTANCE;

	private static final Set<String> NOISE_WORDS = new HashSet<String>();

	/**
	 * Very common words against which searches will not be performed.
	 */
	static {
		NOISE_WORDS.add("a");
		NOISE_WORDS.add("at");
		NOISE_WORDS.add("and");
		NOISE_WORDS.add("be");
		NOISE_WORDS.add("for");
		NOISE_WORDS.add("from");
		NOISE_WORDS.add("has");
		NOISE_WORDS.add("i");
		NOISE_WORDS.add("in");
		NOISE_WORDS.add("is");
		NOISE_WORDS.add("it");
		NOISE_WORDS.add("of");
		NOISE_WORDS.add("on");
		NOISE_WORDS.add("to");
		NOISE_WORDS.add("the");
	}

	/**
	 * Adds the specified word to the set if it is not already present.
	 *
	 * @param word word to be added to this set
	 * @return true if this set did not already contain the specified word
	 */
	public boolean add(final String word) {
		return NOISE_WORDS.add(word);
	}

	/**
	 * Returns true if the word is a noise word.
	 *
	 * @param word word to be checked for containment in this set
	 * @return true if word is a noise word
	 */
	public boolean contains(final String word) {
		return NOISE_WORDS.contains(word);
	}
}
