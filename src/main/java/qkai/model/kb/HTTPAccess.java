package qkai.model.kb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.Hashtable;
import java.util.zip.DeflaterInputStream;
import java.util.zip.GZIPInputStream;

import javax.xml.ws.http.HTTPException;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.tools.bzip2.CBZip2InputStream;

/**
 * Provides a convenient way for easy access to remote resources by HTTP methods.
 *
 * <p>If you receive errors and exceptions with SLF4J, read
 * <a href="http://mark.ossdl.de/2009/03/fixing-exceptions-of-slf4j-and-httpunit/">fixing exception of SLF4J</a>.</p>
 *
 * @see <a href="http://hc.apache.org/httpclient-3.x/apidocs/">JavaDocs for Apache Commons' HTTPClient</a>
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
public final class HTTPAccess {

	private static final int AVERAGE_BODY_SIZE = 2048;
	private static final MultiThreadedHttpConnectionManager connectionManager;

	static {
		connectionManager = new MultiThreadedHttpConnectionManager();
		connectionManager.getParams().setDefaultMaxConnectionsPerHost(12);
	}

	private HTTPAccess() {}

	/**
	 * Does a POST to a URL with empty body.
	 *
	 * <p>Does utilize content compression, if the host supports it.</p>
	 *
	 * @param URL e.g., <a href="http://www.uni-hannover.de/de/">http://www.uni-hannover.de/de/</a>
	 * @param params the POST's POST parameters
	 * @return response body as plaintext, decompressed if necessary
	 * @throws IOException if e.g. there was no internet connection or the host was unreachable.
	 * @throws HTTPException if the host does not serve a regular response but raises an unexpected HTTP code
	 */
	public static String POST(final String URL, final Hashtable<String, String> params) throws IOException, HTTPException {
		final HttpClient client = new HttpClient(connectionManager);
		final PostMethod method = new PostMethod(URL);

		method.getParams().setContentCharset("utf-8");
		method.addRequestHeader("accept-encoding", "gzip,deflate,bzip,bzip2");
		if (params != null) {
			for (String k : params.keySet()) {
				method.addParameter(k, params.get(k));
			}
		}
		try {
			final int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				throw new HTTPException(statusCode);
			}
			return getResponseBody(method);
		} finally {
			method.releaseConnection();
		}
	}

	/**
	 * Decorates the given response so the content is decompressed by GZIP, BZip2 or Deflate, if necessary.
	 *
	 * @see <a href="http://www.ietf.org/rfc/rfc1951.txt">RFC 1951 - DEFLATE Compressed Data Format</a>
	 * @see <a href="http://www.gzip.org/zlib/rfc-gzip.html">RFC 1952 - GZIP Compressed Data Format</a>
	 * @param method Method which response is to be examined.
	 * @return The response body as decorated stream, ready to be used as if there were no compression
	 * @throws java.io.IOException if the response body cannot be obtained or is too short
	 */
	private static InputStream decorateResponseStream(final HttpMethod method) throws IOException {
		final Header ceh = method.getResponseHeader("content-encoding");
		final InputStream is = method.getResponseBodyAsStream();

		if (ceh != null) {
			if (ceh.getValue().equalsIgnoreCase("gzip")) {
				return new GZIPInputStream(is);
			} else if (ceh.getValue().equalsIgnoreCase("deflate")) {
				return new DeflaterInputStream(is);
			} else if (ceh.getValue().equalsIgnoreCase("bzip")) {
				is.read();
				is.read();
				return new CBZip2InputStream(is);
			}
		}
		if (is.markSupported()) {
			is.mark(3);

			final int id1 = is.read();
			final int id2 = is.read();
			final int id3 = is.read();

			is.reset();
			if ((id1 == 0x1f) && (id2 == 0x8b)) {
				return new GZIPInputStream(is);
			} else if ((id1 == 0x42) && (id2 == 0x5a) && (id3 == 0x68)) {
				is.read();
				is.read();
				return new CBZip2InputStream(is);
			}
		}
		return is;
	}

	/**
	 * Gets a HTTP method's response body.
	 *
	 * @param method Method which response is to be obtained.
	 * @return String with response as plaintext, decompressed if necessary
	 * @throws java.io.IOException if the response body cannot be obtained
	 */
	private static String getResponseBody(final HttpMethod method) throws IOException {
		final InputStream rstream = decorateResponseStream(method);
		final BufferedReader br = new BufferedReader(new InputStreamReader(rstream));
		final StringBuilder sb = new StringBuilder(AVERAGE_BODY_SIZE);
		String line;

		while ((line = br.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		}
		br.close();
		rstream.close();
		return sb.toString();
	}

}
