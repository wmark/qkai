package qkai.model.kb;

/**
 * Implemented by Strategies determining noise words.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public interface NoiseWordChecker {

	/**
	 * Determines whether a given word should be disregarded for searching, fetching or quering.
	 *
	 * <p>E.g., "and", "of" or "the".<p>
	 *
	 * @param word to be checked
	 * @return true if word is a noise word
	 */
	public boolean isNoiseWord(String word);
}
