package qkai.model.kb.mediawiki;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.net.URL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------

import opennlp.tools.sentdetect.SentenceDetectorME;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.Text;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extracts information units from MediaWiki's XHTML output.
 *
 * @author Jan Hein
 * @version     $Revision$
 */
public class MediawikiExtractor {

	private static final String jdomElem = "org.jdom.Element";
	private static final Logger log = LoggerFactory.getLogger(MediawikiExtractor.class);
	private static final Namespace ns = Namespace.getNamespace("http://www.w3.org/1999/xhtml");
	private final SentenceDetectorME sentenceDetector;

	/**
	 * Constructs a new instance of this around the given {@code SentenceDetectorME}.
	 *
	 * @param sentenceDetector used to extract information unitss based by NLP
	 */
	public MediawikiExtractor(SentenceDetectorME sentenceDetector) {
		assert sentenceDetector != null;
		this.sentenceDetector = sentenceDetector;
	}

	/**
	 * Gets the parsed XML tree of the given URL of a Mediawiki page.
	 *
	 * @param pageURL URL of page to be extracted
	 * @return JDOM Document
	 */
	private static Document obtainXhtml(String pageURL) {
		Document result = new Document();

		try {
			URL wikiExportUrl = new URL(pageURL);
			SAXBuilder builder = new SAXBuilder();

			builder.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			result = builder.build(wikiExportUrl);
		} catch (Exception e) {
			log.warn("Uncaught exception", e);
		}
		return result;
	}

	/**
	 * Extracts information units.
	 *
	 * @info Has no side-effects and can be reused.
	 * @param pageURL URL of page to be extracted
	 * @return information units in one XML tree
	 * @throws IOException
	 */
	public Document createXML(String pageURL) throws IOException {
		Document doc = obtainXhtml(pageURL);

		doc.setDocType(null);

		Element root = doc.getRootElement();

		root.removeChild("head", ns);
		this.removeImages(root);
		this.removeLinks(root);
		return this.buildXML(root);
	}

	/**
	 * Extracts the information units.
	 *
	 * @info Has no side-effects and can be reused.
	 * @param pageURL URL of page to be extracted
	 * @return information units in one XML tree
	 * @throws IOException
	 */
	public String createString(String pageURL) throws IOException {
		return saveXmlToString(this.createXML(pageURL));
	}

	/**
	 * Externalizes XML to String for output to client browser.
	 *
	 * @param doc the document to be externalized
	 * @return its String representation
	 * @throws java.io.IOException should never occur
	 */
	private static String saveXmlToString(Document xml) throws IOException {
		XMLOutputter xmlOutputter = new XMLOutputter();
		ByteArrayOutputStream bais = new ByteArrayOutputStream();

		try {
			xmlOutputter.output(xml, bais);
		} finally {
			bais.close();
		}
		return bais.toString();
	}

	/**
	 * Durchläuft das XHTML und transformiert die Strukturelemente
	 * zu DocBook XML
	 *
	 * @param root
	 * @return
	 * @throws IOException
	 */
	private Document buildXML(Element root) throws IOException {
		Element article = new Element("article");
		Element sect1 = new Element("sect1");
		Element sect2 = new Element("sect2");
		Element sect3 = new Element("sect3");
		Element parentElement = article;
		Document qkaiXML = new Document(article);
		Iterator descandants = root.getDescendants();

		while (descandants.hasNext()) {
			Object node = descandants.next();

			if (jdomElem.equals(node.getClass().getName())) {
				Element currentElement = (Element) node;

				if (currentElement.getName().equalsIgnoreCase("p")) {
					this.extractParagraph(parentElement, currentElement);
				}
				if (currentElement.getName().equalsIgnoreCase("table")) {
					this.extractTable(parentElement, currentElement);
				}
				if (currentElement.getName().equalsIgnoreCase("h1")) {
					sect1 = this.extractSect(article, currentElement, "sect1");
					parentElement = sect1;
				}
				if (currentElement.getName().equalsIgnoreCase("h2")) {
					sect2 = this.extractSect(sect1, currentElement, "sect2");
					parentElement = sect2;
				}
				// don't process {@literal <h3 id="siteSub">aus Wikipedia, der freien Enzyklopädie</h3>}
				if ((currentElement.getName().equalsIgnoreCase("h3")) && (currentElement.getAttributeValue("id") == null)) {
					sect3 = this.extractSect(sect2, currentElement, "sect3");
					parentElement = sect3;
				}
			}
		}
		this.addId(qkaiXML);
		return qkaiXML;
	}

	/**
	 * Transformiert Tabellen
	 *
	 * @param parentElement
	 * @param table
	 * @throws IOException
	 */
	private void extractTable(Element parentElement, Element table) throws IOException {
		Element informaltable = new Element("informaltable");
		Element parentRow = new Element("error");
		Iterator descandants = table.getDescendants();

		while (descandants.hasNext()) {
			Object node = descandants.next();

			if (jdomElem.equals(node.getClass().getName())) {
				Element currentElement = (Element) node;

				if (currentElement.getName().equalsIgnoreCase("tr")) {
					Element row = new Element("row");

					informaltable.addContent(row);
					parentRow = row;
				}
				if ((currentElement.getName().equalsIgnoreCase("td")) || (currentElement.getName().equalsIgnoreCase("th"))) {
					Element entry = new Element("entry");

					entry.setText(currentElement.getText());
					parentRow.addContent(entry);
				}
			}
		}
		parentElement.addContent(informaltable);
	}

	/**
	 * Transformiert Absätze
	 *
	 * @param parentElement
	 * @param currentElement
	 * @throws IOException
	 */
	private void extractParagraph(Element parentElement, Element currentElement) throws IOException {
		String text = "";
		Iterator descendants = currentElement.getDescendants();

		while (descendants.hasNext()) {
			Object node = descendants.next();

			if ("org.jdom.Text".equals(node.getClass().getName())) {
				Text currentNode = (Text) node;

				text = text + currentNode.getText();
			}
		}

		Element para = new Element("para");

		this.extractSentence(para, text);
		parentElement.addContent(para);
	}

	/**
	 * Transformiert Sätze
	 *
	 * @param parentElement
	 * @param text
	 * @throws IOException
	 */
	private void extractSentence(Element parentElement, String text) throws IOException {
		String[] sents = this.sentenceDetector.sentDetect(text);

		for (int i = 0; i < sents.length; i++) {
			Element phrase = new Element("phrase");

			phrase.setText(sents[i]);
			parentElement.addContent(phrase);
		}
	}

	/**
	 * Transformiert ein Kapitel und fügt es dem parentElement zu
	 *
	 * @param parentElement
	 * @param currentElement
	 * @param sectName
	 * @return
	 */
	private Element extractSect(Element parentElement, Element currentElement, String sectName) {
		String text = "";
		Iterator descendants = currentElement.getDescendants();

		while (descendants.hasNext()) {
			Object node = descendants.next();

			if ("org.jdom.Text".equals(node.getClass().getName())) {
				Text currentNode = (Text) node;

				text = text + currentNode.getText();
			}
		}
		text = text.replace("[Bearbeiten] ", "");

		Element title = new Element("title");

		title.setText(text);

		Element sect = new Element(sectName);

		sect.addContent(title);
		parentElement.addContent(sect);
		return sect;
	}

	/**
	 * Löscht Links aus einem Teilbaum und behält den Text bei
	 *
	 * @author Jan Hein
	 * @param element Element im XML aus dessen Nachfahren Links gelöscht werden sollen
	 */
	private void removeLinks(Element element) {
		Iterator descandants = element.getDescendants();
		// ///////////////
		//
		// Hier werden die Nachfahren in eine HashMap gespeichert um anschließen über sie zu iterieren.
		// Dies ist notwendig, da die Nachfahren während der Iteration verändert werden sollen.
		//
		// //////////////
		HashMap map = new HashMap();
		int mapIndex = 0;

		while (descandants.hasNext()) {
			Object obj = descandants.next();

			map.put(mapIndex, obj);
			mapIndex++;
		}

		Collection keys = new ArrayList(map.keySet());

		for (Iterator iterator = keys.iterator(); iterator.hasNext(); ) {
			Object key = iterator.next();
			Object node = map.get(key);

			if (jdomElem.equals(node.getClass().getName())) {
				Element currentElement = (Element) node;
				List<Element> linkList = currentElement.getChildren("a", ns);

				for (int i = 0; i < linkList.size(); i++) {
					Element link = linkList.get(i);
					int index = currentElement.indexOf(link);
					Text textNode = new Text(link.getText());

					// ///////////////
					// Hier sollte nicht nur der Text des a Tags eingefügt werden sondern alle child-Nodes um Verlust zu vermeiden
					// Es gibt z.B. den Fall das <span>-Tags im a Tag sind. In dem Fall wird kein Text extrahiert.
					// //////////////
					currentElement.setContent(index, textNode);
				}
			}
		}
	}

	/**
	 * Löscht Images aus dem Teilbaum
	 *
	 * @param element Element im XML aus dessen Nachfahren Images gelöscht werden sollen
	 */
	private void removeImages(Element element) {
		Iterator descandants = element.getDescendants();

		while (descandants.hasNext()) {
			Object node = descandants.next();

			if (jdomElem.equals(node.getClass().getName())) {
				Element currentElement = (Element) node;

				currentElement.removeChildren("img", ns);
			}
		}
	}

	/**
	 * Fügt jedem Element ein ID Attribut hinzu
	 *
	 * @param qkaiXML
	 */
	private void addId(Document qkaiXML) {
		Element element = qkaiXML.getRootElement();
		Iterator listMainElements = element.getDescendants();		// <Elem>
		int i = 1;

		while (listMainElements.hasNext()) {
			Object el = listMainElements.next();

			if (jdomElem.equals(el.getClass().getName())) {
				Element elMain = (Element) (el);
				List attributeList = elMain.getAttributes();

				for (int j = 0; j < attributeList.size(); j++) {
					Attribute attribute = (Attribute) (attributeList.get(j));

					elMain.removeAttribute(attribute);
				}

				String id = "info" + i;

				elMain.setAttribute("id", id);
				i++;
			}
		}
	}

}
