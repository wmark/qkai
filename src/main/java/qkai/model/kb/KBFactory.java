package qkai.model.kb;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

//~--- non-JDK imports --------------------------------------------------------

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.util.ConfigObj;

/**
 * Factory to get KBs by name.
 *
 * {@stickyNote KBs have to be provided in a property file {@code qkai.properties}
 * by the property {@code qkai.knowledge_bases} as comma or semicolon delimited list.}
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public final class KBFactory {

	private static final Logger log = LoggerFactory.getLogger(KBFactory.class);
	private static final Map<String, Class<SearchableKB>> kbs;

	static {
		synchronized (KBFactory.class) {
			try {
				final String kbsList = ConfigObj.getProperties().getProperty("qkai.knowledge_bases", "qkai.model.kb.DBPedia");

				kbs = attachKBs(kbsList);
			} catch (ClassNotFoundException e) {
				throw(RuntimeException) new RuntimeException().initCause(e);
			}
		}
	}

	private KBFactory() {}

	/**
	 * Attaches KBs to this Observer derivate.
	 *
	 * @param kbsList list of full knowledge-base names as string (such as {code qkai.model.kb.DBPedia})
	 * @return map with the classes accessible by their reported names
	 * @throws java.lang.ClassNotFoundException if a named class could not be found
	 */
	private static Map<String, Class<SearchableKB>> attachKBs(final String kbsList) throws ClassNotFoundException {
		final String[] kbListed = kbsList.split("\\,");
		final HashMap<String, Class<SearchableKB>> h = new HashMap<String, Class<SearchableKB>>(kbListed.length);

		for (String kb : kbListed) {
			try {
				final Class<SearchableKB> c = getByFullName(kb);
				final SearchableKB k = c.newInstance();

				h.put(k.getName(), getByFullName(kb));
			} catch (InstantiationException e) {
				log.error("Uncaught exception", e);
			} catch (IllegalAccessException e) {
				log.error("Uncaught exception", e);
			}
		}
		return h;
	}

	/**
	 * Gets a knowledge-base {@code Class} by its full name.
	 *
	 * @param kbFullName e.g., {@code qkai.model.kb.DBPedia}
	 * @return found class
	 * @throws java.lang.ClassNotFoundException if class was not found
	 */
	private static Class<SearchableKB> getByFullName(final String kbFullName) throws ClassNotFoundException {
		try {
			return (Class<SearchableKB>) Class.forName(kbFullName);
		} catch (Exception e) {
			throw(ClassNotFoundException) new ClassNotFoundException().initCause(e);
		}
	}

	/**
	 * Gets the short names of all attached KBs.
	 *
	 * @return unmodifiable {@code Set} of KBs' short names, such as {@code DBPedia}
	 */
	public static Set<String> getKBsNames() {
		return Collections.unmodifiableSet(kbs.keySet());
	}

	/**
	 * Gets an instance of a KB by its registered name.
	 *
	 * @param   kbName name of KB to return, case matters; e.g. {@code DBPedia}
	 * @throws  ClassNotFoundException  in case KB is not found
	 * @return  Instantiated KB ready to use.
	 */
	public static SearchableKB getByName(final String kbName) throws ClassNotFoundException {
		if (kbs.containsKey(kbName)) {
			try {
				return kbs.get(kbName).newInstance();
			} catch (Exception e) {
				kbs.remove(kbName);
				throw(ClassNotFoundException) new ClassNotFoundException().initCause(e);
			}
		}
		throw new ClassNotFoundException();
	}

}
