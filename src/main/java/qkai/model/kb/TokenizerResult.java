package qkai.model.kb;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import qkai.model.db.SearchToken;

/**
 * Data container for the {@link Tokenizer}'s results.
 *
 * {@stickyInfo This is meant to be used in output to the user for him to know
 * what of his inputs had lead to the actual search.}
 *
 * @impl This is used internally for search (hence the package friendly fields).
 * @warning Keep in mind that any particular word order is not being preserved.
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public final class TokenizerResult {

	/** List of token which are used to formulate a/the search query. */
	private final Set<SearchToken> acceptedToken;
	private final Set<String> acceptedTokenAsString;

	/** Noise words disregarded for searching purposes. */
	private final Set<String> noiseWords;

	/** Strings, which have been found inadequate for searching. */
	private final Set<String> rejectedToken;

	TokenizerResult(final Set<SearchToken> acceptedToken, final Set<String> noiseWords, final Set<String> rejectedToken) {
		this(acceptedToken, noiseWords, rejectedToken, TokenizerResult.stringify(acceptedToken));
	}

	TokenizerResult(final Set<SearchToken> acceptedToken, final Set<String> noiseWords, final Set<String> rejectedToken,
					final Set<String> acceptedTokenAsString) {
		this.acceptedToken = Collections.unmodifiableSet(acceptedToken);
		this.noiseWords = Collections.unmodifiableSet(noiseWords);
		this.rejectedToken = Collections.unmodifiableSet(rejectedToken);
		this.acceptedTokenAsString = Collections.unmodifiableSet(acceptedTokenAsString);
	}

	/**
	 * Helper function to convert a Set of {@code SearchToken} into a Set of Strings.
	 *
	 * @param tokenSet Set to be converted
	 * @return Set with tokens as Strings
	 */
	private static Set<String> stringify(final Set<SearchToken> tokenSet) {
		final Set<String> ret = new TreeSet<String>();

		for (SearchToken t : tokenSet) {
			ret.add(t.getToken());
		}
		return ret;
	}

	/**
	 * Gets the list of token as {@code SearchToken}.
	 *
	 * @return unmodifiable Set
	 */
	public Set<SearchToken> getAcceptedSearchToken() {
		return acceptedToken;
	}

	/**
	 * Gets the list of token as Strings.
	 *
	 * @return unmodifiable Set
	 */
	public Set<String> getAcceptedToken() {
		return acceptedTokenAsString;
	}

	/**
	 * Gets the list containing noise words disregarded for searching purposes.
	 *
	 * @return unmodifiable Set
	 */
	public Set<String> getNoiseWords() {
		return noiseWords;
	}

	/**
	 * Gets the list of Strings, which have been found inadequate for searching.
	 *
	 * @return unmodifiable Set
	 */
	public Set<String> getRejectedToken() {
		return rejectedToken;
	}

	/**
	 * Gets this {@code TokenizerResult} as JSON ready for passing-through.
	 *
	 * {@example "Example Output"
	 * {
	 *  "rejectedToken":[],
	 *  "noiseWords":[],
	 *  "acceptedToken":["Hanover","Leibniz","University of Hanover"]
	 * }}
	 *
	 * @return JSON representation of this instance
	 * @throws org.json.JSONException if any token is not displayable in JSON format
	 */
	public JSONObject toJSON() throws JSONException {
		final JSONObject j = new JSONObject();

		j.put("acceptedToken", new JSONArray(getAcceptedToken()));
		j.put("noiseWords", (noiseWords == null) ? JSONObject.NULL : new JSONArray(noiseWords));
		j.put("rejectedToken", (rejectedToken == null) ? JSONObject.NULL : new JSONArray(rejectedToken));
		return j;
	}
}
