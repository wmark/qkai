package qkai.model.kb;

import java.io.IOException;

import java.net.URI;

import java.util.Collection;

//~--- non-JDK imports --------------------------------------------------------

import qkai.model.db.Resource;
import qkai.model.db.SearchToken;

/**
 * Skeletal implementation of {@link SearchableKB} for fetching data from querieable sources.
 *
 * <p>This implementation is made for inheritance and to promote creation of KBs.
 * It is basically a wrapper to the strategies imposed by its constructor.</p>
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public abstract class AbstractSearchableKB implements SearchableKB {

	/**
	 * A {@code NoiseWordChecker} for sieveing out unwanted token.
	 * @impl returned by {@link #getNoiseWordChecker}
	 * @impl in {@code AbstractSearchableKB} this is set to {@code SimpleNoiseWordChecker(2)}
	 */
	protected NoiseWordChecker noiseWordChecker;
	/**
	 * {@code ResponseStrategy} for obtaining results which have been converted into a common format used by this.
	 */
	protected final ResponseStrategy responseStrategy;

	AbstractSearchableKB(final ResponseStrategy concreteResponseStrategy) {
		if (concreteResponseStrategy == null) {
			throw new IllegalArgumentException("Processing Strategy cannot be null.");
		}
		this.responseStrategy = concreteResponseStrategy;
		this.noiseWordChecker = new SimpleNoiseWordChecker(2);
	}

	/**
	 * @equivalence return <ThisClass>.class.getSimpleName();
	 */
	@Override
	public abstract String getName();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<URI> resourcesContainingToken(final SearchToken needle) throws IOException {
		return responseStrategy.resourcesContainingToken(needle);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResponseInCommonFormat getResourcesAdjacencies(final Resource resource) throws IOException {
		return responseStrategy.getResourcesAdjacencies(resource);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NoiseWordChecker getNoiseWordChecker() {
		return noiseWordChecker;
	}

}
