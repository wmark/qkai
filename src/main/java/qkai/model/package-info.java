/**
 * All commonly used models reside here.
 *
 * {@stickyDone Only those models belong inside the main package, which are used by all aspects of qKAI.<br />
 * If your models are only for a certain purpose, place them in their own sub-package.}
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
package qkai.model;
