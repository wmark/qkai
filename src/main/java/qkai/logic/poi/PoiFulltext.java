package qkai.logic.poi;

import javax.persistence.Query;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONException;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * POI by fulltext setter.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public class PoiFulltext implements Poi {

	/** For limiting search space by fulltext query. */
	private static final String Q_SEARCH = "SELECT DISTINCT resource FROM properties_fulltext WHERE q=?fulltext";
	private static final Logger log = LoggerFactory.getLogger(PoiFulltext.class);

	@Override
	public boolean doesUnderstand(JSONObject poiBuilder) {
		return poiBuilder.has("fulltext");
	}

	@Override
	public String getPartialQuery(JSONObject poiBuilder) {
		return Q_SEARCH;
	}

	@Override
	public void setParameter(Query poiLimitedQuery, JSONObject poiBuilder) {
		String sphinxContext;
		String term;

		try {
			term = poiBuilder.getString("fulltext");
		} catch (JSONException e) {
			log.warn("Uncaught exception", e);
			return;
		}
		if (term.contains("*") || term.contains("\"") || term.contains("^") || term.contains("<") || term.contains("=")
				|| term.contains("/")) {
			sphinxContext = ";mode=extended2;limit=3000";
		} else if (term.contains("|") || term.contains("&") || term.contains("!") || term.contains("(")) {
			sphinxContext = ";mode=boolean;limit=3000";
		} else {
			sphinxContext = ";limit=3000";
		}
		poiLimitedQuery.setParameter("fulltext", term + sphinxContext);
	}
}
