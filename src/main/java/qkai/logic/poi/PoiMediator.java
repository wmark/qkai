package qkai.logic.poi;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Query;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.logic.service.PersistenceService;

import qkai.util.ConfigObj;

/**
 * Constructor of search space limiting queries out of a given {@code poiBuilder}.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public final class PoiMediator {

	private static final Logger log = LoggerFactory.getLogger(PoiMediator.class);
	private static final List<Poi> availablePois;

	static {
		synchronized (PoiMediator.class) {
			final String poiList = ConfigObj.getProperties().getProperty("qkai.poi_setter", "qkai.logic.poi.PoiFulltext");

			availablePois = registerPois(poiList);
		}
	}

	/** Constructor hidden for Singleton. */
	private PoiMediator() {
		// intentionally left blank
	}

	/**
	 * Helper function for initialization to register all {@code Poi}s provided by configuration file.
	 *
	 * @param poiList configuration file's entry
	 * @return list of available {@code Poi}s
	 */
	private static List<Poi> registerPois(final String poiList) {
		List<Poi> pois = new LinkedList<Poi>();

		for (String p : poiList.split("\\,")) {
			try {
				final Class<Poi> c = (Class<Poi>) Class.forName(p);
				final Poi e = c.newInstance();

				pois.add(e);
			} catch (ClassNotFoundException e) {
				log.error("Uncaught exception", e);
			} catch (InstantiationException e) {
				log.error("Uncaught exception", e);
			} catch (IllegalAccessException e) {
				log.error("Uncaught exception", e);
			}
		}
		return pois;
	}

	/**
	 * Gets all applicable {@code Poi}s as determined by the {@code poiBuilder}.
	 *
	 * @param poiBuilder from request to the {@link qkai.controller.web.PoiController}
	 * @return list of applicable {@code Poi}s
	 */
	private static List<Poi> getApplicablePois(JSONObject poiBuilder) {
		List<Poi> pois = new LinkedList<Poi>();

		for (Poi p : availablePois) {
			if (p.doesUnderstand(poiBuilder)) {
				pois.add(p);
			}
		}
		return pois;
	}

	/**
	 * Constructs a query unitying all {@code Poi} aspects.
	 *
	 * @param currentPois applicable {@code Poi}s.
	 * @param poiBuilder from request to the {@link qkai.controller.web.PoiController}
	 * @return prepared statement with all aspects integrated
	 */
	private static Query constructQuery(List<Poi> currentPois, JSONObject poiBuilder) {
		List<String> subQueries = new LinkedList<String>();

		for (Poi p : currentPois) {
			subQueries.add(p.getPartialQuery(poiBuilder));
		}

		Query q;
		int s = subQueries.size();

		if (s == 1) {
			q = PersistenceService.getInstance().getEntityManager().createNativeQuery(subQueries.get(0) + " LIMIT 25");
		} else {
			StringBuilder sb = new StringBuilder(subQueries.size() * 256);

			sb.append("SELECT DISTINCT resource FROM ");
			for (String partialQuery : subQueries) {
				--s;
				sb.append('(');
				sb.append(partialQuery);
				sb.append(") s");
				sb.append(s);
				if (s > 0) {
					sb.append(" NATURAL JOIN ");
				}
			}
			sb.append(" LIMIT 25");
			q = PersistenceService.getInstance().getEntityManager().createNativeQuery(sb.toString());
		}
		return q;
	}

	/**
	 * Sets query parameters for the complete prepared statement.
	 *
	 * @param q complete prepared statement
	 * @param currentPois applicable {@code Poi}s.
	 * @param poiBuilder from request to the {@link qkai.controller.web.PoiController}
	 */
	private static void setQueryParameters(Query q, List<Poi> currentPois, JSONObject poiBuilder) {
		for (Poi p : currentPois) {
			p.setParameter(q, poiBuilder);
		}
	}

	/**
	 * Assembles a query which limits search space up to the common denominator imposed by all applicable {@code Poi}s.
	 *
	 * @param poiBuilder from request to the {@link qkai.controller.web.PoiController}
	 * @return prepared statement with parameters set
	 * @throws java.lang.IllegalStateException if no applicable {@code Poi} is available to construct a meaningful query
	 */
	public static Query queryOf(JSONObject poiBuilder) throws IllegalStateException {
		List<Poi> currentPois = getApplicablePois(poiBuilder);

		if (currentPois.isEmpty()) {
			throw new IllegalStateException();
		}

		Query q = constructQuery(currentPois, poiBuilder);

		setQueryParameters(q, currentPois, poiBuilder);
		return q;
	}

}
