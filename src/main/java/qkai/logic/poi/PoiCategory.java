package qkai.logic.poi;

import javax.persistence.Query;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONException;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * POI by category.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public class PoiCategory implements Poi {

	/** For limiting search space by category query. */
	private static final String Q_SEARCH = "SELECT DISTINCT cl.resource "
										   + "FROM relations cl JOIN categories_ft ccft ON (cl.target=ccft.resource) "
										   + "WHERE ccft.q=?cattext";
	private static final Logger log = LoggerFactory.getLogger(PoiCategory.class);

	@Override
	public boolean doesUnderstand(JSONObject poiBuilder) {
		return poiBuilder.has("category");
	}

	@Override
	public String getPartialQuery(JSONObject poiBuilder) {
		return Q_SEARCH;
	}

	@Override
	public void setParameter(Query poiLimitedQuery, JSONObject poiBuilder) {
		String term;

		try {
			term = poiBuilder.getString("category");
		} catch (JSONException e) {
			log.warn("Uncaught exception", e);
			return;
		}
		poiLimitedQuery.setParameter("cattext", term + ";sort=attr_desc:items;limit=3000");
	}
}
