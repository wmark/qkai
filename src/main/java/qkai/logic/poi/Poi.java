package qkai.logic.poi;

import javax.persistence.Query;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONObject;

/**
 * Marker for POI Aspects.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public interface Poi {

	/**
	 * Signalizes whether the current {@code Poi} can process the given POI aspect.
	 *
	 * @param poiBuilder with aspect's common name, such as "fulltext", "category" or "around", as key
	 * @return true, if this {@code Poi} can process the aspect named by the {@code poiKey}
	 */
	public boolean doesUnderstand(JSONObject poiBuilder);

	/**
	 * Gets the string representation of a query limiting search space.
	 *
	 * @warning The SQL query must not have any trailing semicolon and should alias its tables in a unique manner.
	 * @param poiBuilder builder determining details of the query
	 * @return SQL query as String, to be assembled to prepared statements by the caller
	 */
	public String getPartialQuery(JSONObject poiBuilder);

	/**
	 * Sets parameters for prepared statement, consisting of at least the query this {@code Poi} has provided.
	 *
	 * @param poiLimitedQuery prepared statement for which the parameters are expected to be set
	 * @param poiBuilder the {@code poiBuilder} provided at asking for the query
	 */
	public void setParameter(Query poiLimitedQuery, JSONObject poiBuilder);
}
