/**
 * Miscellaneous accompanying services are to be found here.
 *
 * {@stickyNote The most prominent is {@link qkai.logic.service.PersistenceService}, which is responsible for thread-safe handling
 * of persistence functions from within container that do not support the JPA Service Provider Interface.}
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
package qkai.logic.service;
