package qkai.logic.service;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

//~--- non-JDK imports --------------------------------------------------------

import org.eclipse.persistence.queries.DataModifyQuery;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.factories.SessionManager;

/**
 * Utility class for thread-safe dealing with persistence.
 *
 * <p>For thread-safety this encapsules Persistence (which happens to not being such in several implementations), and for
 * thread-isolation implements a thread-local Singleton for serving {@code EntityManager}s of a single context.</p>
 *
 * {@example "Sample Usage 1 - Queries only"
 * EntityManager em = PersistenceService.getInstance().getEntityManager();
 * try {
 *  Query q = em.createNamedQuery("Resource.findByResourceURI");
 *  q.setParameter("resourceURI", uri);
 *  q.setMaxResults(1);
 *  return (Resource) q.getSingleResult();
 * }}
 *
 * {@example "Sample Usage 2 - Transactions"
 * EntityManager em = PersistenceService.getInstance().getEntityManager();
 * try {
 *  EntityTransaction t = em.getTransaction();
 *  try {
 *      t.begin();
 *      Resource w = new Resource(uri, hint);
 *      em.persist(w);
 *      t.commit();
 *      return w;
 *  } finally {
 *      if (t.isActive()) {
 *          t.rollback();
 *      }
 *  }
 * } finally {
 *  em.close();
 * }}
 *
 * {@stickyInfo If you do not expect your query to return any data and if it needs an transaction,
 * you might consider utilizing {@link #runDatabaseActionInTransaction}.}
 *
 * {@stickyWarning It is imperative that you obey the coding standards shown above, or you will experience unpredictable behaviour,
 * especially in multi-threaded applications.}
 *
 * {@stickyWarning Again: This is responsible for thread-safe handling of persistence functions from within container that do not support
 * the JPA Service Provider Interface. Circumventing it will do definitely break the entire application in Byzantine ways.}
 *
 * @see <a href="http://www.oracle.com/technology/products/ias/toplink/jpa/howto/create-modify-delete.html">How to Create, Modify and Delete an Entity</a>
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
public final class PersistenceService {

	private static String DEFAULT_PU = "qkaiPU";
	private static ThreadLocal<PersistenceService> instance = new ThreadLocal<PersistenceService>() {
		@Override
		protected PersistenceService initialValue() {
			return new PersistenceService();
		}
	};
	private static boolean isConnectedToMySql;
	private static EntityManagerFactory pmf;

	static {
		try {
			pmf = (EntityManagerFactory) new InitialContext().lookup("java:comp/env/persistence/" + DEFAULT_PU);
		} catch (NamingException ex) {
			pmf = Persistence.createEntityManagerFactory(DEFAULT_PU);
		}
		isConnectedToMySql = ((String) pmf.createEntityManager().getProperties().get("javax.persistence.jdbc.url")).contains("mysql");
	}

	private EntityManager em;
	private Session session;

	private PersistenceService() {
		this.em = pmf.createEntityManager();
	}

	/**
	 * Gets a thread-local instance of this {@code PersistenceService}.
	 *
	 * @info Fetching a {@code EntityManager} must only be done by an instance (for thread-safety)!
	 * @return an instance of PersistenceService
	 */
	public static PersistenceService getInstance() {
		return instance.get();
	}

	private static void removeInstance() {
		instance.remove();
	}

	/**
	 * Gets an instance of {@code EntityManager}, guaranteed to be open.
	 *
	 * @note You can call this only once in unit-tests.
	 * @warning Except it is being solely used for {@code SELECT} statements, it needs to be closed or unpredictable behaviour will occur.
	 * @return an instance of {@code EntityManager}
	 */
	public EntityManager getEntityManager() {
		if ((this.em == null) || !this.em.isOpen()) {
			this.em = pmf.createEntityManager();
		}
		return em;
	}

	/**
	 * Gets an {@code Session}.
	 *
	 * @info You will most probably need this for {@code Expression}s.
	 * @return an instance of {@code Session}
	 */
	public Session getSession() {
		if (this.session == null) {
			this.session = SessionManager.getManager().getSession(DEFAULT_PU);
		}
		return session;
	}

	/**
	 * Closes this instance.
	 *
	 * @info Call this if you only had a transaction for isolation and need not to end it.
	 */
	public void close() {
		if ((em != null) && em.isOpen()) {
			em.close();
		}
		removeInstance();
	}

	/**
	 * Check whether the backend is MySQL.
	 *
	 * @warning Assumes that the application is connected to only one DBMS.
	 * @return true if the DBMS is MySQL
	 */
	public static boolean isConnectedToMysql() {
		return isConnectedToMySql;
	}

	/**
	 * Runs an isolated native query.
	 *
	 * @info It is recommended to use a {@code StringBuilder} to assemble large queries.
	 * @warning Make sure you have validated the query's inputs or mission critical side-effects can occur.
	 * @param query native query which gets passed to the DBMS without further checks
	 */
	public static void runIsolatedNativeQuery(final String query) {
		final DataModifyQuery q = new DataModifyQuery();

		q.setSQLString(query);
		getInstance().getSession().executeQuery(q);
	}

	/**
	 * Runs a {@code DatabaseAction} from within a transaction context.
	 *
	 * @param action action to be run
	 * @throws IllegalArgumentException if action is null
	 */
	public static <T> T runDatabaseActionInTransaction(final DatabaseAction<T> action) {
		if (action == null) {
			throw new IllegalArgumentException("Argument action cannot be null.");
		}

		final EntityManager myem = PersistenceService.getInstance().getEntityManager();

		try {
			final EntityTransaction t = myem.getTransaction();

			try {
				t.begin();
				action.em = myem;

				final T ret = action.run();

				t.commit();
				return ret;
			} finally {
				if (t.isActive()) {
					t.rollback();
				}
			}
		} finally {
			myem.close();
		}
	}

	/**
	 * Action to be run by {@code PersistenceService} in a transaction context.
	 *
	 * {@stickyInfo This is to aid programming with queries need to be run from within a transaction,
	 * no matter whether the underlying DBMS demands them. For example, {@link javax.persistence.Query#executeUpdate}
	 * insists on such context on MySQL, even with {@literal MyISAM} storage engine which is not
	 * transactional at all.}
	 *
	 * {@example "Sample Usage 1 - simple persistence action"
	 * final SearchToken token = ...;
	 *
	 * PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {
	 *  {@literal @Override}
	 *  public Boolean run() {
	 *      em.merge(token);
	 *      return true;
	 *  }
	 * });
	 * }
	 *
	 * {@example "Sample Usage 2 - a single query at closes scope"
	 * final Resource r = ...; // got by static void f(final Resource r);
	 *
	 * PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {
	 *  {@literal @Override}
	 *  public Boolean run() {
	 *      Query q = em.createNamedQuery("Resource.setFullyLoaded");
	 *
	 *      q.setParameter("fullyLoaded", true);
	 *      q.setParameter("resource", r.getID());
	 *      q.executeUpdate();
	 *      return true;
	 *  }
	 * });
	 * }
	 *
	 * {@example "Sample Usage 3 - multiple native queries"
	 * final String qStatement =
	 *  "INSERT IGNORE INTO findings (tid,resource) SELECT ?1 tid, sub.resource "
	 *  + "FROM (SELECT r.resource FROM resources r WHERE r.resourceURI = ?2 LIMIT 1) sub";
	 * EntityManager em = PersistenceService.getInstance().getEntityManager();
	 * final Query q = em.createNativeQuery(qStatement);
	 *
	 * q.setParameter("1", token.getID().toString());
	 * PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {
	 *
	 *  {@literal @Override}
	 *  public Boolean run() {
	 *      for (URI u : resourceURIs) {
	 *      q.setParameter("2", u.toString());
	 *      q.executeUpdate();
	 *      }
	 *      return true;
	 *  }
	 * });
	 * }
	 *
	 * @info See {@link PersistenceService#runDatabaseActionInTransaction} for a receiver.
	 * @author    W-Mark Kubacki
	 * @version   $Revision$
	 */
	public static abstract class DatabaseAction<T> {

		/**
		 * Prior invocation of {@code run} an usable {@code EntityManager} will be assigned to this field.
		 */
		protected EntityManager em;

		/**
		 * Runs the actual command. Typically this is some queries.
		 */
		public abstract T run();
	}

}
