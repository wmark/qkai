package qkai.logic.fetch;

import javax.persistence.EntityManager;
import javax.persistence.Query;

//~--- non-JDK imports --------------------------------------------------------

import qkai.logic.service.PersistenceService;

import qkai.model.db.SearchToken;

/**
 * Sieves to already known search tokens.
 *
 * To improve performance and avoid race conditions in {@link RelatedResourcesProducer} which may lead to persistence warnings.
 *
 * <p>Implements exactly-once behaviour for searching a KB for tokens.</p>
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
final class FoundTokenSieve implements Runnable {

	private final FetchingMediator fm;
	private final SearchToken input;

	/**
	 * Constructs a new {@code FoundTokenSieve} without side-effects.
	 *
	 * @param fm {@code FetchingMediator} the results will be handled over to, if there are any
	 * @param input {@code SearchToken} to be examined
	 */
	public FoundTokenSieve(final FetchingMediator fm, final SearchToken input) {
		assert fm != null;
		assert input != null;
		this.fm = fm;
		this.input = input;
	}

	/**
	 * Does the actual sieveing.
	 *
	 * @param input Search token to be found in resources.
	 * @return Array of resources as URIs containing the given token.
	 */
	@Override
	public void run() {
		if (tokenIsUnknown(input) && memorizeSearchToken(input)) {
			fm.fetchRelatedResources(input);
		}
	}

	/**
	 * @param token        to be looked up.
	 * @return boolean     true if the token has not been stored in DB yet
	 */
	private static boolean tokenIsUnknown(final SearchToken token) {
		final EntityManager em = PersistenceService.getInstance().getEntityManager();
		final Query q = em.createNamedQuery("SearchToken.countByToken");

		q.setParameter("kb_name", token.getKb());
		q.setParameter("token", token.getToken());
		return 0 == (Long) q.getSingleResult();
	}

	/**
	 * In order to avoid redundant searches search token have to be stored.
	 * As a handy side-effect this will block further processings if the database is down
	 * by supressing creation of new ones.
	 *
	 * @param token        to be stored in DB.
	 * @return boolean     true if the token has been stored successfully, false if it was already found
	 */
	private static boolean memorizeSearchToken(final SearchToken token) {
		return PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {
			@Override
			public Boolean run() {
				em.persist(token);
				return true;
			}
		});
	}

}
