package qkai.logic.fetch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.model.db.Resource;
import qkai.model.kb.ResponseInCommonFormat;

/**
 * Getter for a {@link Resource}'s related properties and relations.
 *
 * <p>Implements {@link Comparable} to support signaling of fetching priorities.</p>
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
final class AdjacentResourceFetcher implements Runnable, Comparable<AdjacentResourceFetcher> {

	private static final Logger log = LoggerFactory.getLogger(AdjacentResourceFetcher.class);
	private final FetchingMediator fm;
	private final Resource input;
	private final Integer priority;

	/**
	 * Constructs an {@code AdjacentResourceFetcher} for a {@code Resource}, meant to be run at a given priority.
	 *
	 * <p>The lower the priority the more likely (thus earlier) this Runnable is run.</p>
	 *
	 * @param fm {@code FetchingMediator} the results will be handled over to, if there are any
	 * @param input {@code Resource} adjacencies will be fetched for
	 * @param priority the lower the earlier this task gets run
	 */
	public AdjacentResourceFetcher(final FetchingMediator fm, final Resource input, final int priority) {
		assert fm != null;
		assert input != null;
		this.fm = fm;
		this.input = input;
		this.priority = priority;
	}

	/**
	 * Fetches {@link qkai.model.db.Property}es relating to the given {@code Resource}.
	 */
	@Override
	public void run() {
		try {
			final ResponseInCommonFormat f = fm.kb.getResourcesAdjacencies(input);

			fm.persistAdjacencies(input, f);
		} catch (Exception e) {
			log.debug("Uncaught exception", e);
		}
	}

	@Override
	public int compareTo(final AdjacentResourceFetcher o) {
		return priority.compareTo(o.priority);
	}

}
