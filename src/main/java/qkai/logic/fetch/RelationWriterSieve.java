package qkai.logic.fetch;

import java.net.URI;

//~--- non-JDK imports --------------------------------------------------------

import qkai.logic.service.PersistenceService;

import qkai.model.db.Resource;
import qkai.model.db.SearchToken;

/**
 * Sieve to already known and fully loaded {@link Resource}s, lets all other pass for them being loaded.
 *
 * <p>This version is for plain JPA.</p>
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
final class RelationWriterSieve implements Runnable {

	private final FetchingMediator fm;
	private final SearchToken input;

	/**
	 * Constructs an instance of this task without side-effects.
	 *
	 * @param fm {@code FetchingMediator} the results will be handled over to, if there are any
	 * @param input {@code SearchToken} which holds in {@link SearchToken#resourcesToBeConnectedWith}
	 * {@code URI}s to {@code Resource}s of interest here
	 *
	 */
	public RelationWriterSieve(final FetchingMediator fm, final SearchToken input) {
		assert fm != null;
		assert input != null;
		this.fm = fm;
		this.input = input;
	}

	/**
	 * Does the actual sieveing and passing-through.
	 *
	 * <p>Additionally, first {@code Resource}s get a lower priority so that they are processed earlier.
	 * This is meant for the case when several of this tasks run concurrently, to prevent a one large search result block
	 * the fetching of another.</p>
	 */
	@Override
	public void run() {
		Integer priority = 1;

		for (URI u : input.resourcesToBeConnectedWith) {
			final Resource r = linkWithResourceByURI(input, u);

			if (!r.isFullyLoaded()) {
				fm.fetchAdjacentResources(r, priority++);
			}
		}
		input.resourcesToBeConnectedWith = null;
		persistSearchToken(input);
	}

	/**
	 * Establishes a link (known as {@code finding}s) between a {@code SearchToken} and a {@code Resource}.
	 *
	 * <p>This is for marking a {@code Resource} containing that given {@code SearchTolken}.</p>
	 *
	 * @param input {@code SearchTolken} having been found in a {@code Resource}
	 * @param u URI to a {@code Resource}
	 * @return {@code Resource} generated from the 'URIs contents'
	 */
	private static Resource linkWithResourceByURI(final SearchToken input, final URI u) {
		final Resource resource = Resource.valueOf(u.toString(), Resource.ContentHint.NODE);

		input.addResource(resource);
		return resource;
	}

	/**
	 * Fills the {@code findings} table before the corresponding {@code Resource}s get fetched.
	 *
	 * @param token Will be written to database immediately.
	 */
	private static void persistSearchToken(final SearchToken token) {
		PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {
			@Override
			public Boolean run() {
				em.merge(token);
				return Boolean.TRUE;
			}
		});
	}

}
