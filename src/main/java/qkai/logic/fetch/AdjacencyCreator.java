package qkai.logic.fetch;

import javax.persistence.Query;

//~--- non-JDK imports --------------------------------------------------------

import qkai.logic.service.PersistenceService;

import qkai.model.db.EntityBuilder;
import qkai.model.db.Property;
import qkai.model.db.Resource;
import qkai.model.kb.ResponseInCommonFormat;

/**
 * Task for persisting a {@code Resource}'s adjacencies.
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
final class AdjacencyCreator implements Runnable {

	private final Resource input;
	private final ResponseInCommonFormat response;

	/**
	 * Constructs an {@code AdjacencyCreator} for a {@code Resource}.
	 *
	 * @param input {@code Resource} which is intended to be fully loaded
	 * @param response response of KB containing adjacency builder(s)
	 */
	public AdjacencyCreator(final Resource input, final ResponseInCommonFormat response) {
		assert input != null;
		assert response != null;
		this.input = input;
		this.response = response;
	}

	/**
	 * Stores {@link qkai.model.db.Property}es relating to the given {@code Resource},
	 * eventually along with {@link qkai.model.db.Relation}s more {@code Resource}s are created.
	 */
	@Override
	public void run() {
		for (String resourceURI : response.getDescribedResources()) {
			final Resource r = Resource.valueOf(resourceURI, Resource.ContentHint.NODE);

			for (EntityBuilder e : response.getBuilderFor(resourceURI)) {
				if (e instanceof Property.PropertyBuilder) {
					final Property.PropertyBuilder pe = (Property.PropertyBuilder) e;

					pe.resource(r).build();
				} else {
					e.build();
				}
			}
		}
		if (response.getDescribedResources().size() > 0) {
			setFullyLoadedToTrue(input);
		}
	}

	/**
	 * Does persist a {@code Resource} as fully (thus completely) loaded.
	 *
	 * @param r {@code Resource} which new state is to be persisted
	 */
	private static void setFullyLoadedToTrue(final Resource r) {
		PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction() {

			@Override
			public Boolean run() {
				final Query q = em.createNamedQuery("Resource.setFullyLoaded");

				q.setParameter("fullyLoaded", true);
				q.setParameter("resource", r.getID());
				q.executeUpdate();
				return true;
			}

		});
	}

}
