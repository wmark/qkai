package qkai.logic.fetch;

import java.net.URI;

import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.Query;

//~--- non-JDK imports --------------------------------------------------------

import org.eclipse.persistence.expressions.Expression;
import org.eclipse.persistence.expressions.ExpressionBuilder;
import org.eclipse.persistence.sessions.Session;

import qkai.logic.service.PersistenceService;

import qkai.model.db.Resource;
import qkai.model.db.SearchToken;

/**
 * Sieve to already known and fully loaded {@link Resource}s, lets all other pass for them being loaded.
 *
 * <p>This version has optimizations for MySQL.</p>
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
final class RelationWriterSieveMysql implements Runnable {

	private final FetchingMediator fm;
	private final SearchToken input;

	/**
	 * Constructs an instance of this task without side-effects.
	 *
	 * @param fm {@code FetchingMediator} the results will be handled over to, if there are any
	 * @param input {@code SearchToken} which holds in {@link SearchToken#resourcesToBeConnectedWith}
	 * {@code URI}s to {@code Resource}s of interest here
	 */
	public RelationWriterSieveMysql(final FetchingMediator fm, final SearchToken input) {
		assert fm != null;
		assert input != null;
		this.fm = fm;
		this.input = input;
	}

	/**
	 * Does the actual sieveing and passing-through.
	 *
	 * <p>Additionally, first {@code Resource}s get a lower priority so that they are processed earlier.
	 * This is meant for the case when several of this tasks run concurrently, to prevent a one large search result block
	 * the fetching of another.</p>
	 */
	@Override
	public void run() {
		int priority = 1;

		mysqlCreateAllResources(input.resourcesToBeConnectedWith, Resource.ContentHint.NODE);
		mysqlLinkWithResourceByURI(input, input.resourcesToBeConnectedWith);
		if (input.resourcesToBeConnectedWith == null) {
			return;
		}
		for (Resource r : mysqlGetUnfetchedResourcesOf(input.resourcesToBeConnectedWith)) {
			fm.fetchAdjacentResources(r, priority++);
		}
		input.resourcesToBeConnectedWith = null;
	}

	/**
	 * Creates all resources to be associated by the given URIs by a MySQL bulk {@code INSERT IGNORE}.
	 *
	 * <p>This does not alter existing resources.</p>
	 *
	 * @param resourceURIs URIs of the {@code Resource}s to be created should they not exist
	 * @param hint RDF extrinsic hint about this {@code Resource}'s content
	 * @throws IllegalArgumentException if resourceURIs or hint is null
	 */
	static void mysqlCreateAllResources(final Collection<?> resourceURIs, final Resource.ContentHint hint) {
		if ((resourceURIs == null) || (hint == null)) {
			throw new IllegalArgumentException("Neither resourceURIs nor hint can be null.");
		}

		final String qStatement = "INSERT IGNORE INTO resources (resourceURI,hint) VALUES (?1,?2)";
		final Query q = PersistenceService.getInstance().getEntityManager().createNativeQuery(qStatement);

		q.setParameter("2", hint.toString());
		PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {
			@Override
			public Boolean run() {
				for (Object o : resourceURIs) {
					q.setParameter("1", o.toString());
					q.executeUpdate();
				}
				return true;
			}
		});
	}

	/**
	 * Links the given {@code SearchToken} with {@code Resource}s by a MySQL bulk query.
	 *
	 * @param token {@code SearchTolken} having been found in the {@code Resource}s
	 * @param resourceURIs URI to the {@code Resource}s
	 */
	private static void mysqlLinkWithResourceByURI(final SearchToken token, final Collection<URI> resourceURIs) {
		final String qStatement = "INSERT IGNORE INTO findings (tid,resource) "
								  + "SELECT ?1 tid, a.resource FROM resources a WHERE a.resourceURI=?2 LIMIT 1";
		final Query q = PersistenceService.getInstance().getEntityManager().createNativeQuery(qStatement);

		q.setParameter("1", String.valueOf(token.getID()));
		PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {
			@Override
			public Boolean run() {
				for (URI u : resourceURIs) {
					q.setParameter("2", u.toString());
					q.executeUpdate();
				}
				return true;
			}
		});
	}

	/**
	 * Gets {@code Resource}s which have not been fully loaded by a bulk read.
	 *
	 * @param resourceURIs URIs of the {@code Resource}s to be fetched if they are not marked being fully loaded
	 * @return Collection of {@code Resource}s
	 */
	private static Collection<Resource> mysqlGetUnfetchedResourcesOf(final Collection<URI> resourceURIs) {
		if (resourceURIs.size() < 1) {
			return new LinkedList<Resource>();
		}

		final ExpressionBuilder emp = new ExpressionBuilder();
		final Expression fullyLoaded = emp.get("fullyLoaded").equal(false);
		final Expression matchesURI = emp.get("resourceURI").in(resourceURIs);
		final Session session = PersistenceService.getInstance().getSession();

		return session.readAllObjects(Resource.class, fullyLoaded.and(matchesURI));
	}

}
