package qkai.logic.fetch;

import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//~--- non-JDK imports --------------------------------------------------------

import qkai.logic.service.PersistenceService;

import qkai.model.db.Resource;
import qkai.model.db.SearchToken;
import qkai.model.kb.ResponseInCommonFormat;
import qkai.model.kb.SearchableKB;

/**
 * Mediator to the various steps of data fetching and processing.
 *
 * @impl The number of Threads in the pools is subject to change, currently result of fine-tuning by W-Mark Kubacki for his
 * quadcore workstation.
 * @impl The number of Threads must not be as high as to appear as DOS attack to the accessed KB, but be as high
 * as to sufficiently minimize idle times.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
final class FetchingMediator {

	private final ThreadPoolExecutor executorForRWS = new ThreadPoolExecutor(1, 1, 10, TimeUnit.SECONDS, new LinkedBlockingQueue());
	private final ThreadPoolExecutor executorForRRP = new ThreadPoolExecutor(2, 2, 10, TimeUnit.SECONDS, new LinkedBlockingQueue());
	private final ThreadPoolExecutor executorForFTS = new ThreadPoolExecutor(2, 2, 10, TimeUnit.SECONDS, new LinkedBlockingQueue());
	private final ThreadPoolExecutor executorForAdj = new ThreadPoolExecutor(6, 6, 10, TimeUnit.SECONDS, new PriorityBlockingQueue());
	private final ThreadPoolExecutor executorForAdC = new ThreadPoolExecutor(2, 2, 10, TimeUnit.SECONDS, new LinkedBlockingQueue());
	final SearchableKB kb;

	/**
	 * Constructs a {@code FetchingMediator} ready to work with.
	 *
	 * <p>Creation is costly, as a minimum of {@code Thread}s need to be created, maintained and perhaps started.</p>
	 *
	 * @param haystack KB this mediator will run fetching processes for
	 */
	public FetchingMediator(final SearchableKB haystack) {
		assert haystack != null;
		this.kb = haystack;
	}

	/**
	 * Processes search tokens by injecting them into the first {@code Executor}.
	 *
	 * @warning The token must be filtered and sanitized before getting to this method.
	 * @param needles Set of {code SearchToken} expected to yield {@link qkai.model.db.Resource}s containing them
	 */
	public void processTokens(final Set<SearchToken> needles) {
		assert needles != null;
		for (SearchToken t : needles) {
			executorForFTS.execute(new FoundTokenSieve(this, t));
		}
	}

	/**
	 * Puts {@link AdjacentResourceFetcher}'s result to further processing.
	 *
	 * @param t SearchToken for which related resources shall be fetched.
	 */
	void fetchRelatedResources(final SearchToken t) {
		assert t != null;
		executorForRRP.execute(new RelatedResourcesProducer(this, t));
	}

	/**
	 * Puts {@link RelatedResourcesProducer}'s result to further processing.
	 *
	 * @param t SerachToken which related resources shall be persisted
	 */
	void persistRelatedResources(final SearchToken t) {
		assert t != null;
		if (PersistenceService.isConnectedToMysql()) {
			executorForRWS.execute(new RelationWriterSieveMysql(this, t));
		} else {
			executorForRWS.execute(new RelationWriterSieve(this, t));
		}
	}

	/**
	 * Puts {@link RelationWriterSieve}'s result to further processing.
	 *
	 * @param r Resource for which properties and Relations should be fetched
	 * @param priority int which leads to the order Resources shall be processed (lower is higher)
	 */
	void fetchAdjacentResources(final Resource r, final int priority) {
		assert r != null;
		executorForAdj.execute(new AdjacentResourceFetcher(this, r, priority));
	}

	/**
	 * Puts {@link AdjacentResourceFetcher}'s results to further processing.
	 *
	 * @param r Resource for which the adjacencies are to be created
	 * @param response response of KB containing adjacency builder(s)
	 */
	void persistAdjacencies(final Resource r, final ResponseInCommonFormat response) {
		assert r != null;
		assert response != null;
		if (PersistenceService.isConnectedToMysql()) {
			executorForAdC.execute(new AdjacencyCreatorMysql(r, response));
		} else {
			executorForAdC.execute(new AdjacencyCreator(r, response));
		}
	}

}
