package qkai.logic.fetch;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import javax.persistence.Query;

//~--- non-JDK imports --------------------------------------------------------

import qkai.logic.service.PersistenceService;

import qkai.model.db.EntityBuilder;
import qkai.model.db.Property;
import qkai.model.db.Relation;
import qkai.model.db.Resource;
import qkai.model.kb.ResponseInCommonFormat;

/**
 * Task for persisting a {@code Resource}'s adjacencies.
 *
 * <p>This version has optimizations for MySQL.</p>
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
final class AdjacencyCreatorMysql implements Runnable {

	private final Resource input;
	private final ResponseInCommonFormat response;

	/**
	 * Constructs an {@code AdjacencyCreator} for a {@code Resource}.
	 *
	 * @param input {@code Resource} which is intended to be fully loaded
	 * @param response response of KB containing adjacency builder(s)
	 */
	public AdjacencyCreatorMysql(final Resource input, final ResponseInCommonFormat response) {
		assert input != null;
		assert response != null;
		this.input = input;
		this.response = response;
	}

	/**
	 * Stores {@link qkai.model.db.Property}es relating to the given {@code Resource},
	 * eventually along with {@link qkai.model.db.Relation}s more {@code Resource}s are created.
	 */
	@Override
	public void run() {
		final Set<String> descRes = response.getDescribedResources();
		final Set<String> resNODE = new HashSet<String>(descRes.size());
		final Set<String> resPROPERTY = new HashSet<String>(descRes.size() / 2 + 1);
		final Collection<Property.PropertyBuilder> colPROP = new LinkedList<Property.PropertyBuilder>();
		final Collection<Relation.RelationBuilder> colREL = new LinkedList<Relation.RelationBuilder>();

		for (String resourceURI : response.getDescribedResources()) {
			resNODE.add(resourceURI);
			for (EntityBuilder e : response.getBuilderFor(resourceURI)) {
				if (e instanceof Property.PropertyBuilder) {
					final Property.PropertyBuilder t = (Property.PropertyBuilder) e;

					resPROPERTY.add(t.predicateURI);
					colPROP.add(t);
				} else if (e instanceof Relation.RelationBuilder) {
					final Relation.RelationBuilder t = (Relation.RelationBuilder) e;

					resPROPERTY.add(t.predicateURI);
					resNODE.add(t.targetURI);
					colREL.add(t);
				}
			}
		}
		RelationWriterSieveMysql.mysqlCreateAllResources(resPROPERTY, Resource.ContentHint.PROPERTY);
		RelationWriterSieveMysql.mysqlCreateAllResources(resNODE, Resource.ContentHint.NODE);
		mysqlCreateProperty(input, colPROP);
		mysqlCreateRelation(colREL);
		setConditionallyFullyLoadedToTrue(input);
	}

	/**
	 * Does persist a {@code Resource} as fully (thus completely) loaded, if it has {@code Property}s;
	 *
	 * @param r {@code Resource} which new state is to be persisted
	 */
	private static void setConditionallyFullyLoadedToTrue(final Resource r) {
		final String query = "UPDATE resources r SET r.fully_loaded=1 WHERE r.resource={0} AND r.fully_loaded=0 AND "
							 + "(EXISTS (SELECT * FROM properties a WHERE a.resource={0} LIMIT 1) "
							 + "OR EXISTS (SELECT * FROM relations b WHERE b.resource={0} LIMIT 1))";

		PersistenceService.runIsolatedNativeQuery(query.replace("{0}", String.valueOf(r.getID())));
	}

	static void mysqlCreateProperty(final Resource propertyOwner, final Collection<Property.PropertyBuilder> properties) {
		final String qStatement = "INSERT IGNORE INTO properties (resource,predicate,ptype,pvalue,lang) "
								  + "SELECT ?1 resource, b.resource predicate, ?2 ptype, ?3 pvalue, ?4 lang "
								  + "FROM resources b WHERE b.resourceURI=?5";
		final Query q = PersistenceService.getInstance().getEntityManager().createNativeQuery(qStatement);

		q.setParameter("1", propertyOwner.getID());
		PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {

			@Override
			public Boolean run() {
				for (Property.PropertyBuilder b : properties) {
					q.setParameter("2", b.ptype);
					q.setParameter("3", b.pvalue);
					q.setParameter("4", b.lang);
					q.setParameter("5", b.predicateURI);
					q.executeUpdate();
				}
				return Boolean.TRUE;
			}

		});
	}

	static void mysqlCreateRelation(final Collection<Relation.RelationBuilder> relations) {
		final String qStatement = "INSERT IGNORE INTO relations (resource,predicate,target) "
								  + "SELECT a.resource resource, b.resource predicate, c.resource target "
								  + "FROM resources a, resources b, resources c "
								  + "WHERE a.resourceURI=?1 AND b.resourceURI=?2 AND c.resourceURI=?3";
		final Query q = PersistenceService.getInstance().getEntityManager().createNativeQuery(qStatement);

		PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {

			@Override
			public Boolean run() {
				for (Relation.RelationBuilder rel : relations) {
					q.setParameter("1", rel.resourceURI);
					q.setParameter("2", rel.predicateURI);
					q.setParameter("3", rel.targetURI);
					q.executeUpdate();
				}
				return true;
			}

		});
	}

}
