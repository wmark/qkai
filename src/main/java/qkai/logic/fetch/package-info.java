/**
 * Fetching services reside in this package.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
package qkai.logic.fetch;
