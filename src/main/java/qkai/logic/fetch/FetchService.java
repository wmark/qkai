package qkai.logic.fetch;

import java.util.Hashtable;

//~--- non-JDK imports --------------------------------------------------------

import qkai.model.kb.KBFactory;
import qkai.model.kb.SearchableKB;
import qkai.model.kb.Tokenizer;
import qkai.model.kb.TokenizerResult;

/**
 * Facade service for fetching data from knowledge bases to local database for caching and later processing.
 *
 * {@example "Sample Usage"
 * String term = "Leibniz Hanover \"University of Hanover\"";
 * TokenizerResult wordsPassed = FetchService.initiateFetchingFor("DBPedia", term);
 * if (wordsPassed.getAcceptedToken().size() > 0) {
 *  // report success
 * } else {
 *  // tell the user to provide something else for fetching
 * }
 * // display which words have been discarded
 * }
 *
 * @info It is safe to invoke this repeatedly: This implements exactly-once pattern of distributed systems.
 * @impl This is run prior any search operation, as the search space might not exist and thus needs to be fetched.
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public final class FetchService {

	private static final Hashtable<String, FetchingMediator> instances = new Hashtable<String, FetchingMediator>(2);

	private FetchService() {}

	/**
	 * Gets a FetchingMediator associated with given knowledge base name, if the latter exists.
	 *
	 * @param kbName name of the knowledge base the FetchingMediator is bound to
	 * @return FetchingMediator ready to be used
	 * @throws ClassNotFoundException if no knowledge base has been found by that kbName
	 */
	private static FetchingMediator getInstanceFor(final String kbName) throws ClassNotFoundException {
		if (!instances.containsKey(kbName)) {
			final SearchableKB haystack = KBFactory.getByName(kbName);
			final FetchingMediator f = new FetchingMediator(haystack);

			instances.put(kbName, f);
		}
		return instances.get(kbName);
	}

	/**
	 * Gets always a FetchingMediator associated with given knowledge base name.
	 *
	 * <p>This is like {@link #getInstanceFor} but with the additional guarantee which is needed by runnables already
	 * run by an {@code FetchingMediator} which hence must exists a priori.</p>
	 *
	 * @param kbName name of the knowledge base the FetchingMediator is bound to
	 * @return FetchingMediator in circulation
	 */
	static FetchingMediator getGuaranteedInstanceFor(final String kbName) {
		assert kbName != null;
		return instances.get(kbName);
	}

	/**
	 * Initates fetching of data at given knowledge base for given strings.
	 *
	 * @impl The {@code kbName} is expected to be in short format, e.g. {@code DBPedia} and not {@code qkai.model.kb.DBPedia}.
	 *
	 * @param kbName name of the knowledge base data has to be fetched from
	 * @param term this term will be validated and processed into token to be searched for
	 * @return result of search term processing
	 * @throws ClassNotFoundException if no knowledge base has been found by that kbName
	 * @throws IllegalArgumentException if either kbName or term is null
	 */
	public static TokenizerResult initiateFetchingFor(final String kbName, final String term) throws ClassNotFoundException {
		if ((kbName == null) || (term == null)) {
			throw new IllegalArgumentException("Neither kbName nor term cannot be null.");
		}

		final FetchingMediator f = FetchService.getInstanceFor(kbName);
		final TokenizerResult result = Tokenizer.parseText(f.kb.getName(), f.kb.getNoiseWordChecker(), term);

		f.processTokens(result.getAcceptedSearchToken());
		return result;
	}

}
