package qkai.logic.fetch;

import qkai.logic.service.PersistenceService;

import qkai.model.db.SearchToken;

/**
 * Producer of {@link qkai.model.db.Resource}s containing a given {@link SearchToken}.
 *
 * {@stickyInfo Searching for a token is costly on the kb.
 * Therefore the number of running {@code RelatedResourcesProducer}s is to be kept low.}
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
final class RelatedResourcesProducer implements Runnable {

	private final FetchingMediator fm;
	private final SearchToken input;

	/**
	 * Constructs an instance of this task without side-effects.
	 *
	 * @param fm {@code FetchingMediator} the results will be handled over to, if there are any
	 * @param input {@code SearchToken} for which {@link qkai.model.db.Resource}s shall be fetched
	 */
	public RelatedResourcesProducer(final FetchingMediator fm, final SearchToken input) {
		assert fm != null;
		assert input != null;
		this.fm = fm;
		this.input = input;
	}

	/**
	 * Does the actual searching for {@code Resource}s.
	 *
	 * <p>On errors, will unmemorize (i.e., delete) the {@code SearchToken} for following search requests to be able to try again.</p>
	 *
	 * @param input Search token to be found in resources.
	 * @return Array of resources as URIs containing the given token.
	 */
	@Override
	public void run() {
		try {
			input.resourcesToBeConnectedWith = fm.kb.resourcesContainingToken(input);
			fm.persistRelatedResources(input);
		} catch (java.io.IOException e) {
			unmemorizeSearchToken(input);
		}
	}

	/**
	 * Deletes the given token, so it may be searched for it again.
	 *
	 * <p>This is called whenever the search yields errors.</p>
	 *
	 * @param token {@code SearchToken} to be 'forgotten' ever been searched for
	 */
	private static void unmemorizeSearchToken(final SearchToken token) {
		PersistenceService.runDatabaseActionInTransaction(new PersistenceService.DatabaseAction<Boolean>() {
			@Override
			public Boolean run() {
				em.remove(em.find(SearchToken.class, token.getID()));
				return true;
			}
		});
	}

}
