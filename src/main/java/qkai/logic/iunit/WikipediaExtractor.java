package qkai.logic.iunit;

import java.io.IOException;

import java.util.HashMap;
import java.util.Map;

//~--- non-JDK imports --------------------------------------------------------

import opennlp.tools.sentdetect.SentenceDetectorME;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.model.kb.mediawiki.MediawikiExtractor;

import qkai.util.ConfigObj;

/**
 * Adapter of {@link MediawikiExtractor} to extract information units from Wikipedia.
 *
 * @author W-Mark Kubacki
 * @version     $Revision$
 */
public final class WikipediaExtractor {

	private static final Logger log = LoggerFactory.getLogger(WikipediaExtractor.class);
	private static final Map<String, SentenceDetectorME> sentenceDetectors = new HashMap<String, SentenceDetectorME>(4);

	static {
		final String mp = System.getProperty("os.name").toLowerCase().contains("win")
						  ? ConfigObj.getProperties().getProperty("qkai.opennlp.files_path.win", "C:/ProgramData/opennlp/models/")
						  : ConfigObj.getProperties().getProperty("qkai.opennlp.files_path", "/usr/share/opennlp/models/");

		try {
			sentenceDetectors.put("en", new opennlp.tools.lang.english.SentenceDetector(mp + "english/sentdetect/EnglishSD.bin.gz"));
			sentenceDetectors.put("de", new opennlp.tools.lang.german.SentenceDetector(mp + "german/sentdetect/sentenceModel.bin.gz"));
			sentenceDetectors.put("es", new opennlp.tools.lang.spanish.SentenceDetector(mp + "spanish/sentdetect/SpanishSent.bin.gz"));
			sentenceDetectors.put("th", new opennlp.tools.lang.thai.SentenceDetector(mp + "thai/sentdetect/thai.sent.bin.gz"));
		} catch (IOException e) {
			log.error("Please check if you have set qkai.opennlp.files_path to the right directory, "
					  + "whether it has a trailing slash and contains model files in subdirectories, such as "
					  + "\"english/sentdetect/EnglishSD.bin.gz\".");
			throw(RuntimeException) new RuntimeException().initCause(e);
		}
	}

	/** Constructor hidden for Singleton. */
	private WikipediaExtractor() {
		// intentionally left blank
	}

	/**
	 * Gets the information units as XML.
	 *
	 * @param pageTitle Title of the Wikipedia page to be extracted
	 * @param language Language of the Wikipedia subdomain as abbreviation according to ISO 639-2
	 * @return XML representation as String meant to be served to clients
	 * @throws java.io.IOException if the Wikipedia page cannot be read due to network failure or refusal
	 */
	public static String extract(final String pageTitle, final String language) throws IOException {
		return extractIUnits(assemblyExtPage(pageTitle, language), language);
	}

	/**
	 * Assemblies the Wikipedia's page URL to be extracted.
	 *
	 * @param pageTitle used to build that URL
	 * @param language used inside subdomain to build that URL
	 * @return URL as String
	 */
	private static String assemblyExtPage(final String pageTitle, final String language) {
		return "http://" + language + ".wikipedia.org/w/index.php?title=" + pageTitle + "&printable=yes";
	}

	/**
	 * Gets the information units as XML tree.
	 *
	 * @param pageURL URL of page to be extracted
	 * @param language Language of the Wikipedia subdomain as abbreviation according to ISO 639-2
	 * @return Information units as externalized XML document
	 * @throws java.io.IOException
	 */
	public static String extractIUnits(final String pageURL, final String language) throws IOException {
		String lang = language;

		if (!sentenceDetectors.containsKey(language)) {
			log.warn("No SentenceDetector found for langauge \"{}\". Using \"en\" instead for detection.", language);
			lang = "en";
		}

		MediawikiExtractor creator = new MediawikiExtractor(sentenceDetectors.get(lang));

		return creator.createString(pageURL);
	}

}
