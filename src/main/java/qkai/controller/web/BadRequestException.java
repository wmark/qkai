package qkai.controller.web;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

//~--- non-JDK imports --------------------------------------------------------

import qkai.util.rs.MediaType;

/**
 * For maintaining a consistent JSON error response API.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public class BadRequestException extends WebApplicationException {

	private static int BAD_REQUEST = 400;

	/**
	 * Create a HTTP 400 (Bad Request) exception.
	 */
	public BadRequestException() {
		super(Response.status(BAD_REQUEST).build());
	}

	/**
	 * Create a HTTP 400 (Bad Request) exception.
	 *
	 * @param   message the String that is the entity of the response.
	 */
	public BadRequestException(final String message) {
		super(Response.status(BAD_REQUEST).entity("{\"error\": \"" + message + "\"}").type(MediaType.APPLICATION_JSON_TYPE).build());
	}

}
