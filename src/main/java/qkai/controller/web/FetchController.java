package qkai.controller.web;

import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.*;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.logic.fetch.FetchService;

import qkai.model.kb.KBFactory;

import qkai.util.rs.MediaType;

/**
 * FrontController to the actual fetching processes.
 *
 * @see         <a href="http://java.sun.com/blueprints/patterns/FrontController.html">Front Controller blueprint</a>
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
@Singleton
@Path("/fetch")
public final class FetchController {

	private static final Logger log = LoggerFactory.getLogger(FetchController.class);

	/**
	 * Initiates server-side threaded fetching through all KBs.
	 *
	 * {@example "Example Output"
	 * {
	 * "DBPedia": {
	 *  "rejectedToken":[],
	 *  "noiseWords":[],
	 *  "acceptedToken":["Hanover","Leibniz","University of Hanover"]
	 * }
	 * }}
	 *
	 * @param term Term to be searched for.
	 * @return JSON dictionary, as shown above
	 */
	@GET
	@Path("{term}")
	@Produces(MediaType.APPLICATION_JSON)
	public String initiateFetching(@PathParam("term") final String term) throws NotFoundException {
		final JSONObject ret = new JSONObject();

		for (String kb : KBFactory.getKBsNames()) {
			try {
				ret.put(kb, FetchService.initiateFetchingFor(kb, term).toJSON());
			} catch (java.lang.ClassNotFoundException e) {
				log.debug("Uncaught exception, fetching ignored for KB: " + kb, e);
			} catch (org.json.JSONException e) {
				log.debug("Uncaught exception, fetcher result gets discarded from KB: " + kb, e);
			}
		}
		return ret.toString();
	}

}
