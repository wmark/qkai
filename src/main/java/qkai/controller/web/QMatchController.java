package qkai.controller.web;

import com.sun.jersey.spi.resource.Singleton;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

//~--- non-JDK imports --------------------------------------------------------

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.model.User;
import qkai.model.db.Relation;
import qkai.model.db.Resource;

import qkai.util.rs.MediaType;

/**
 * Controller of qMatch game and its actions.
 *
 * @author      W-Mark Kubacki
 * @author      Andelko Jovancevic
 * @version     $Revision$
 */
@Singleton
@Path("/game/qmatch")
public class QMatchController {

	private static final String hasPlayed = "http://qkai.org/game/qmatch/actions#hasPlayed";
	private static final String pathPlayed = "/game/qmatch/actions/hasPlayed";
	private static final Logger log = LoggerFactory.getLogger(QMatchController.class);
	private static final Resource hasPlyd = Resource.valueOf(hasPlayed, Resource.ContentHint.PROPERTY);

	/**
	 * Creates the {@link Relation} between a {@link User} and a picture be played.
	 *
	 * Extrinsinc {@code N} is set to whether the {@code User} has played the picture 'correctly'.
	 *
	 * @param request automatically set by JAX-WS, used to get the current request's user
	 * @param picURL URL of the picture having been played
	 * @param value the extrinsinc, 1 if correctly recognized, otherwise 0
	 * @return HTTP CREATED response with the location the current played state of that picture can be get from
	 * @throws java.net.URISyntaxException if the GET location cannot be constructed; save to ignore
	 */
	@PUT
	@Path("/actions/hasPlayed")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response putPlayedAssoc(@Context final HttpServletRequest request, @FormParam("picture") String picURL,
								   @QueryParam("correct") int value)
			throws java.net.URISyntaxException {
		Resource currentUser = User.getCurrent(request).getUserAsResource();
		Resource picture = Resource.valueOf(picURL, Resource.ContentHint.NODE);

		Relation.valueOf(currentUser, hasPlyd, picture).setN(value);
		log.debug("All actions concerning Relation have been done.");

		URI u = new URI("http", null, "qkai.org", -1, pathPlayed, "picture=" + picURL, null);

		return Response.created(u).build();
	}

	/**
	 * Gets the state of a {@link User} to picture {@link Relation}.
	 *
	 * 1 if correctly recognized by the user, otherwise 0
	 *
	 * @param request automatically set by JAX-WS, used to get the current request's user
	 * @param picURL URL of the picture having been played
	 * @return 1 or 0 as string, in plain text
	 */
	@GET
	@Path("/actions/hasPlayed")
	@Produces(MediaType.TEXT_PLAIN)
	public String getPlayedAssoc(@Context final HttpServletRequest request, @QueryParam("picture") String picURL) {
		Resource currentUser = User.getCurrent(request).getUserAsResource();
		Resource picture = Resource.valueOfIfExists(picURL);

		return String.valueOf(Relation.valueOf(currentUser, hasPlyd, picture).getN());
	}

}
