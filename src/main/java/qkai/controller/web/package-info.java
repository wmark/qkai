/**
 * Contains controller for RESTful web services as well as mapper to transform exceptions into error pages.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
package qkai.controller.web;
