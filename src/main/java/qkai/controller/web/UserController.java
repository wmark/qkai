package qkai.controller.web;

import com.sun.jersey.api.view.Viewable;
import com.sun.jersey.spi.resource.Singleton;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

//~--- non-JDK imports --------------------------------------------------------

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.model.User;

import qkai.util.rs.MediaType;

/**
 * Wrapper to {@link qkai.model.User}'s methods.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
@Singleton
@Path("/user")
public final class UserController {

	private static final Logger log = LoggerFactory.getLogger(UserController.class);
	private static final CacheControl dontCache;
	private static final Viewable loginPage;
	private static final URI uriLoginPage;
	private static final URI uriSettingsPage;

	static {
		try {
			loginPage = new Viewable("/account.jsp", null);
			uriLoginPage = new URI("/user/login");
			uriSettingsPage = new URI("/user/settings");
			dontCache = new CacheControl();
			dontCache.setNoCache(true);
		} catch (Exception e) {
			throw(RuntimeException) new RuntimeException().initCause(e);
		}
	}

	/**
	 * Logs the accessing user out.
	 *
	 * @param request automatically set by JAX-WS
	 * @param response will be modified to log out the user
	 * @return Response redirect to the login page
	 */
	@GET
	@Path("logout")
	@Produces({MediaType.TEXT_HTML, MediaType.APPLICATION_JSON})
	public Response logoutUser(@Context final HttpServletRequest request, @Context final HttpServletResponse response) {
		User currentUser = User.getCurrent(request);

		log.debug("User {} is about to log out.", currentUser.getIdentifier());
		currentUser.logout(request, response);
		return Response.seeOther(uriLoginPage).cacheControl(dontCache).build();
	}

	/**
	 * Serves the login and account management page.
	 *
	 * @return {@code /account.jsp}
	 */
	@GET
	@Path("login")
	public Viewable displayLoginPage() {
		log.debug("Hit login");
		return loginPage;
	}

	/**
	 * Serves the login and account management page.
	 *
	 * @impl Currently a dummy copy of login page
	 * @return {@code /account.jsp}
	 */
	@GET
	@Path("settings")
	public Viewable displaySettingsPage() {
		log.debug("Hit settings");
		return loginPage;
	}

	/**
	 * Redirects the suer after his login at OpenID's side.
	 *
	 * @param request automatically set by JAX-WS
	 * @return redirect response
	 */
	@GET
	@Path("login/openid")
	public Response openidReplyRedirector(@Context final HttpServletRequest request) {
		User currentUser = User.getCurrent(request);

		log.debug("User {} has logged in.", currentUser.getIdentifier());
		return Response.seeOther(uriSettingsPage).cacheControl(dontCache).build();
	}

}
