package qkai.controller.web;

import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

//~--- non-JDK imports --------------------------------------------------------

import qkai.model.db.Resource;

import qkai.util.rs.MediaType;

/**
 * Delegator and resolver of named nodes to common {@link Resource}s.
 *
 * <p>E.g., resolves {@literal http://qkai.org/rest/predicate/useraction/click} to {@code Resource} number x.,
 * which in return can be obtained by the controller serving all {@code Resource}s to the public.</p>
 *
 * <p>Such resolving is needed as at creation time of a {@code Resource} its ID is unknown.
 * If it would, qKAI could agree upon that figure 5 is for clicks and use it.
 * But it is not, to preserve readability introduced by names and to avoid ID conflicts with existing data.</p>
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
@Singleton
public final class QkaiOntologyDelegator {

	private static final String RESOURCE_PREFIX = "http://qkai.org/rest/predicate/";

	/**
	 * Resolves an qKAI ontology element as URI into a {@code Resource} ID.
	 *
	 * That ID can later be used to redirect the requestor to a place with more about that {@code Resource}.
	 *
	 * @todo Redirect eventually by "See Other" to controller serving {@code Resource}s.
	 *
	 * @param scope scope of the action, such as "useraction", "database"...
	 * @param action action or state property, such as in "useraction/click", "useraction/vote", "database/statistics"
	 * @return ID of the {@code Resource} identified by request path
	 * @throws qkai.controller.web.NotFoundException if no {@code Resource} exists or has not been created yet
	 */
	@GET
	@Path("/predicate/${scope}/${action}")
	@Produces(MediaType.TEXT_PLAIN)
	public long resolveToID(@PathParam("scope") final String scope, @PathParam("action") final String action) throws NotFoundException {
		try {
			final Resource r = Resource.valueOfIfExists(RESOURCE_PREFIX + scope + "/" + action);

			return r.getID();
		} catch (Exception e) {
			throw(NotFoundException) new NotFoundException().initCause(e);
		}
	}

}
