package qkai.controller.web;

import com.sun.jersey.spi.resource.Singleton;

import java.net.URI;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.logic.service.PersistenceService;

import qkai.model.RdfExternalizer;
import qkai.model.User;
import qkai.model.db.PredicateAgent;
import qkai.model.db.Resource;

import qkai.util.rs.MediaType;

/**
 * Serves {@code Resource}s to the public.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
@Singleton
@Path("/resource")
public class ResourceController {

	private static final String CLICK_PREDICATE = "http://qkai.org/rest/predicate/user/click";
	private static final String COMM_PREDICATE = "http://www.w3.org/2000/01/rdf-schema#comment";
	private static final String DESC_PREDICATE = "http://purl.org/dc/elements/1.1/description";
	private static final String LINK_PICTURE = "http://xmlns.com/foaf/0.1/img";
	private static final String LINK_THUMBNAIL = "http://xmlns.com/foaf/0.1/depiction";
//  private static final String LINK_HOMEPAGE = "http://xmlns.com/foaf/0.1/homepage";
//  private static final String LINK_WIKIPEDIA_DE = "http://dbpedia.org/property/wikipage-de";
//  private static final String LINK_WIKIPEDIA_EN = "http://xmlns.com/foaf/0.1/page";
	private static final String NAME_PREDICATE = "http://www.w3.org/2000/01/rdf-schema#label";
	private static final Logger log = LoggerFactory.getLogger(ResourceController.class);
	private static final Map<String, String> formatLocations = new HashMap<String, String>(6);
	private static final String Q_DESCRIPTIVE;
	private static final String Q_IMGLOCATION;

	static {
		String q =
			"SELECT r.resource, r.resourceURI, r.last_update," + "COALESCE(name_x.pvalue, name_r.pvalue, name.pvalue) 'caption',"
			+ "COALESCE(desc_x.pvalue, desc_r.pvalue, descr.pvalue) 'description',"
			+ "COALESCE(comm_x.pvalue, comm_r.pvalue, comm.pvalue) 'comment' " + "FROM resources r "
			+ "LEFT OUTER JOIN properties name_x ON (r.resource=name_x.resource AND name_x.predicate=@NAME_PREDICATE AND name_x.lang='{1}') "
			+ "LEFT OUTER JOIN properties name_r ON (r.resource=name_r.resource AND name_r.predicate=@NAME_PREDICATE AND name_r.lang='{2}') "
			+ "LEFT OUTER JOIN properties name ON (r.resource=name.resource AND name.predicate=@NAME_PREDICATE AND name.lang IS NULL) "
			+ "LEFT OUTER JOIN properties desc_x ON (r.resource=desc_x.resource AND desc_x.predicate=@DESC_PREDICATE AND desc_x.lang='{1}') "
			+ "LEFT OUTER JOIN properties desc_r ON (r.resource=desc_r.resource AND desc_r.predicate=@DESC_PREDICATE AND desc_r.lang='{2}') "
			+ "LEFT OUTER JOIN properties descr ON (r.resource=descr.resource AND descr.predicate=@DESC_PREDICATE AND descr.lang IS NULL) "
			+ "LEFT OUTER JOIN properties comm_x ON (r.resource=comm_x.resource AND comm_x.predicate=@COMM_PREDICATE AND comm_x.lang='{1}') "
			+ "LEFT OUTER JOIN properties comm_r ON (r.resource=comm_r.resource AND comm_r.predicate=@COMM_PREDICATE AND comm_r.lang='{2}') "
			+ "LEFT OUTER JOIN properties comm ON (r.resource=comm.resource AND comm.predicate=@COMM_PREDICATE AND comm.lang IS NULL) "
			+ "WHERE r.resource IN ({0})";

		Q_DESCRIPTIVE = q.replace("@NAME_PREDICATE",
								  String.valueOf(PredicateAgent.getPredicateValueOf(NAME_PREDICATE))).replace("@DESC_PREDICATE",
									  String.valueOf(PredicateAgent.getPredicateValueOf(DESC_PREDICATE))).replace("@COMM_PREDICATE",
										  String.valueOf(PredicateAgent.getPredicateValueOf(COMM_PREDICATE)));
		q = "SELECT pt.resource, CAST(pt.latitude AS CHAR), CAST(pt.longitude AS CHAR), rel.predicate, img.resourceURI FROM properties_pt pt"
			+ " LEFT JOIN relations rel USING (resource) JOIN resources img ON (rel.target=img.resource)"
			+ " WHERE pt.resource IN ({0}) AND rel.predicate IN (@IMG1,@IMG2)";
		Q_IMGLOCATION = q.replace("@IMG1", String.valueOf(PredicateAgent.getPredicateValueOf(LINK_THUMBNAIL))).replace("@IMG2",
								  String.valueOf(PredicateAgent.getPredicateValueOf(LINK_PICTURE)));
		formatLocations.put(MediaType.TEXT_RDF_N3, "/resource/{id}-v1.n3");
		formatLocations.put(MediaType.APPLICATION_RDF_N3, "/resource/{id}-v1.n3");
		formatLocations.put(MediaType.TEXT_NTRIPLES, "/resource/{id}-v1.nt");
		formatLocations.put(MediaType.APPLICATION_NTRIPLES, "/resource/{id}-v1.nt");
		formatLocations.put(MediaType.APPLICATION_RDF_XML, "/resource/{id}-v1.xml");
		formatLocations.put(MediaType.APPLICATION_XTURTLE, "/resource/{id}-v1.ttl");
	}

	/**
	 * Gets {@code Resource}s' descriptive texts by an {@code Array} of their IDs.
	 *
	 * @param resourceIds IDs of the {@code Resource}s information to obtain for
	 * @return JSONObject with the requested informations, if exist
	 */
	@GET
	@Path("text/{ids: (?:\\d+,?)+}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_JAVASCRIPT})
	public String getByArray(@PathParam("ids") String resourceIds) {
		log.debug("Got request for IDs {}", resourceIds);

		long time1, elapsed;

		time1 = System.nanoTime();

		String tmpq = Q_DESCRIPTIVE.replace("{0}", resourceIds).replace("{1}", "DE").replace("{2}", "EN");
		Query q = PersistenceService.getInstance().getEntityManager().createNativeQuery(tmpq);
		List<Object[]> results = q.getResultList();
		JSONObject ret = new JSONObject();
		JSONArray arr = new JSONArray();

		for (Object[] row : results) {
			try {
				JSONObject entry = new JSONObject();

				entry.put("id", (Long) row[0]);
				entry.put("URI", (String) row[1]);
				entry.put("lastUpdate", (java.sql.Timestamp) row[2]);
				entry.put("caption", (String) row[3]);
				entry.put("description", (String) row[4]);
				entry.put("comment", (String) row[5]);
				arr.put(entry);
			} catch (JSONException e) {
				log.debug("JSONException during parsing of query's results", e);
			}
		}
		elapsed = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time1);
		if (elapsed >= 500) {
			log.warn("Serving resources took {}ms", elapsed);
		} else {
			log.trace("Serving resources took {}ms", elapsed);
		}
		try {
			ret.put("items", arr);
			ret.put("elapsed", elapsed);
		} catch (JSONException e) {
			log.warn("Uncaught exception", e);
		}
		return ret.toString();
	}

	/**
	 * Gets a single {@code Resource} by its unique ID.
	 *
	 * Use this to obtain {@code Resource}s in controllers.
	 *
	 * @param id ID of the {@code Resource} to be obtained
	 * @return the {@code Resource}
	 * @throws NoResultException if the provided ID doesn't belong to a {@code Resource};
	 *                           this exception is mapped to a HTTP 404 error page if not catched
	 */
	private static Resource getResourceById(Long id) throws NoResultException {
		EntityManager em = PersistenceService.getInstance().getEntityManager();
		Query q = em.createNamedQuery("Resource.findByResource");

		q.setParameter("resource", id);
		q.setMaxResults(1);
		return (Resource) q.getSingleResult();
	}

	/**
	 * Wrapper to {@link RdfExternalizer} which throws a {@link NotFoundException} on invalid mime types.
	 *
	 * @param resourceId ID of the {@code Resource} to be externalized
	 * @param mimeType an member of {@link MediaType} is expected; see {@code RdfExternalizer} which types are supported
	 * @return the externalized {@code Resource} ready to be served
	 */
	private static String externalizeResource(final Long resourceId, final String mimeType) {
		try {
			return RdfExternalizer.externalize(getResourceById(resourceId), mimeType);
		} catch (ClassNotFoundException e) {
			log.debug("RdfExternalizer did not find writer for MediaType \"{}\".", mimeType);
			throw new NotFoundException();
		}
	}

	/**
	 * Register a user's click on a {@code Resource}
	 *
	 * @param request
	 * @param response
	 * @param resourceID ID of the {@code Resource} to register a click for
	 * @return empty string - PUT is expected to have empty response body
	 */
	@PUT
	@Path("click/{id: \\d+}")
	public String registerClick(@Context final HttpServletRequest request, @Context final HttpServletResponse response,
								@PathParam("id") Long resourceID) {
		log.warn("User clicked on resource {}", resourceID);

		final User u = User.getCurrent(request);
		Resource r;

		try {
			r = getResourceById(resourceID);
			if (r.getHint() != Resource.ContentHint.NODE) {
				// TODO: error illegal state
				log.debug("User '{}' clicked on resource {} which is not a node.", u.getIdentifier(), resourceID);
			}
		} catch (javax.persistence.NoResultException e) {
			log.debug("Resource with ID {} has not been found.", resourceID);
		}

		long p = PredicateAgent.getPredicateValueOf(CLICK_PREDICATE);

		// already an click registered?
		// ... increase value
		// else register first click
		return "";
	}

	/**
	 * Gets {@code Resource}s' geolocation and images by an {@code Array} of their IDs.
	 *
	 * @param resourceIds IDs of the {@code Resource}s information to obtain for
	 * @return JSONObject with the requested informations, if exist
	 */
	@GET
	@Path("image,geo/{ids: (?:\\d+,?)+}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_JAVASCRIPT})
	public String getImgLocation(@PathParam("ids") String resourceIds) {
		log.debug("Got request for IDs {}", resourceIds);

		long time1, elapsed;

		time1 = System.nanoTime();

		String tmpq = Q_IMGLOCATION.replace("{0}", resourceIds);
		Query q = PersistenceService.getInstance().getEntityManager().createNativeQuery(tmpq);
		List<Object[]> results = q.getResultList();
		JSONObject ret = new JSONObject();
		JSONArray arr = new JSONArray();

		for (Object[] row : results) {
			try {
				JSONObject entry = new JSONObject();

				entry.put("id", row[0]);
				entry.put("latitude", row[1]);
				entry.put("longitude", row[2]);
				entry.put("predicate", row[3]);
				entry.put("imageURI", row[4]);
				arr.put(entry);
			} catch (JSONException e) {
				log.warn("JSONException during parsing of query's results", e);
			}
		}
		elapsed = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time1);
		if (elapsed >= 500) {
			log.warn("Serving image and coordinates took {}ms", elapsed);
		} else {
			log.trace("Serving image and coordinates took {}ms", elapsed);
		}
		try {
			ret.put("items", arr);
			ret.put("elapsed", elapsed);
		} catch (JSONException e) {
			log.warn("Uncaught exception", e);
		}
		return ret.toString();
	}

	/**
	 * Convenience wrapper to {@link Response}'s static constructor.
	 *
	 * @param resourceId will be used to build the URI's path
	 * @param mimeType used to lookup path and file extension in {@link #formatLocations}
	 * @return HTTP {@code see other} response redirecting to the {@code Resource} URI
	 * @throws java.net.URISyntaxException impossible, ignore
	 */
	private static Response redirectToResourceByMimeType(final Long resourceId, final String mimeType) throws java.net.URISyntaxException {
		URI u = new URI("http", "qkai.org", formatLocations.get(mimeType).replace("{id}", String.valueOf(resourceId)), null);

		return Response.seeOther(u).build();
	}

	/**
	 * Redirects to the {@code Resource}'s URI which serves the mime type.
	 *
	 * @param resourceId ID of the {@code Resource} the URI will be for; whether it exists is not checked here
	 * @return redirect response
	 * @throws java.net.URISyntaxException impossible, ignore
	 */
	@GET
	@Path("{id: \\d+}")
	@Produces({MediaType.TEXT_RDF_N3, MediaType.APPLICATION_RDF_N3})
	public Response redirectToResourceN3(@PathParam("id") Long resourceId) throws java.net.URISyntaxException {
		return redirectToResourceByMimeType(resourceId, MediaType.TEXT_NTRIPLES);
	}

	/**
	 * Redirects to the {@code Resource}'s URI which serves the mime type.
	 *
	 * @param resourceId ID of the {@code Resource} the URI will be for; whether it exists is not checked here
	 * @return redirect response
	 * @throws java.net.URISyntaxException impossible, ignore
	 */
	@GET
	@Path("{id: \\d+}")
	@Produces({MediaType.TEXT_NTRIPLES, MediaType.APPLICATION_NTRIPLES})
	public Response redirectToResourceNT(@PathParam("id") Long resourceId) throws java.net.URISyntaxException {
		return redirectToResourceByMimeType(resourceId, MediaType.TEXT_RDF_N3);
	}

	/**
	 * Redirects to the {@code Resource}'s URI which serves the mime type.
	 *
	 * @param resourceId ID of the {@code Resource} the URI will be for; whether it exists is not checked here
	 * @return redirect response
	 * @throws java.net.URISyntaxException impossible, ignore
	 */
	@GET
	@Path("{id: \\d+}")
	@Produces(MediaType.APPLICATION_XTURTLE)
	public Response redirectToResourceTurtle(@PathParam("id") Long resourceId) throws java.net.URISyntaxException {
		return redirectToResourceByMimeType(resourceId, MediaType.APPLICATION_XTURTLE);
	}

	/**
	 * Redirects to the {@code Resource}'s URI which serves the mime type.
	 *
	 * @param resourceId ID of the {@code Resource} the URI will be for; whether it exists is not checked here
	 * @return redirect response
	 * @throws java.net.URISyntaxException impossible, ignore
	 */
	@GET
	@Path("{id: \\d+}")
	@Produces(MediaType.APPLICATION_RDF_XML)
	public Response redirectToResourceXML(@PathParam("id") Long resourceId) throws java.net.URISyntaxException {
		return redirectToResourceByMimeType(resourceId, MediaType.APPLICATION_RDF_XML);
	}

	/**
	 * Gets the {@code Resource} as N3 document.
	 *
	 * File extension is used for static caching in front of qKAI.
	 * The {@literal -vX} suffix is reserved for future generation numbering:
	 * Should the content be changed redirects will occur to a new, higher version thus omitting a possibly old file in cache.
	 *
	 * @note output can be parsed e.g. by {@link org.openrdf.rio.Parser}
	 * @info yields a HTTP 404 if the {@code Resource} does not exist
	 * @param resourceId ID of the {@code Resource} the document will be written for
	 * @return externalized {@code Resource} in a specific RDF file format
	 */
	@GET
	@Path("{id: \\d+}-v1.n3")
	@Produces(MediaType.TEXT_RDF_N3)
	public String getResourceDumpN3(@PathParam("id") Long resourceId) {
		return externalizeResource(resourceId, MediaType.TEXT_RDF_N3);
	}

	/**
	 * Gets the {@code Resource} as N3 document.
	 *
	 * File extension is used for static caching in front of qKAI.
	 * The {@literal -vX} suffix is reserved for future generation numbering:
	 * Should the content be changed redirects will occur to a new, higher version thus omitting a possibly old file in cache.
	 *
	 * @note output can be parsed e.g. by {@link org.openrdf.rio.Parser}
	 * @info yields a HTTP 404 if the {@code Resource} does not exist
	 * @param resourceId ID of the {@code Resource} the document will be written for
	 * @return externalized {@code Resource} in a specific RDF file format
	 */
	@GET
	@Path("{id: \\d+}-v1.nt")
	@Produces(MediaType.TEXT_NTRIPLES)
	public String getResourceDumpNT(@PathParam("id") Long resourceId) {
		return externalizeResource(resourceId, MediaType.TEXT_NTRIPLES);
	}

	/**
	 * Gets the {@code Resource} as N3 document.
	 *
	 * File extension is used for static caching in front of qKAI.
	 * The {@literal -vX} suffix is reserved for future generation numbering:
	 * Should the content be changed redirects will occur to a new, higher version thus omitting a possibly old file in cache.
	 *
	 * @note output can be parsed e.g. by {@link org.openrdf.rio.Parser}
	 * @info yields a HTTP 404 if the {@code Resource} does not exist
	 * @param resourceId ID of the {@code Resource} the document will be written for
	 * @return externalized {@code Resource} in a specific RDF file format
	 */
	@GET
	@Path("{id: \\d+}-v1.ttl")
	@Produces(MediaType.APPLICATION_XTURTLE)
	public String getResourceDumpTurtle(@PathParam("id") Long resourceId) {
		return externalizeResource(resourceId, MediaType.APPLICATION_XTURTLE);
	}

	/**
	 * Gets the {@code Resource} as N3 document.
	 *
	 * File extension is used for static caching in front of qKAI.
	 * The {@literal -vX} suffix is reserved for future generation numbering:
	 * Should the content be changed redirects will occur to a new, higher version thus omitting a possibly old file in cache.
	 *
	 * @note output can be parsed e.g. by {@link org.openrdf.rio.Parser}
	 * @info yields a HTTP 404 if the {@code Resource} does not exist
	 * @param resourceId ID of the {@code Resource} the document will be written for
	 * @return externalized {@code Resource} in a specific RDF file format
	 */
	@GET
	@Path("{id: \\d+}-v1.xml")
	@Produces(MediaType.APPLICATION_RDF_XML)
	public String getResourceDumpXML(@PathParam("id") Long resourceId) {
		return externalizeResource(resourceId, MediaType.APPLICATION_RDF_XML);
	}

}
