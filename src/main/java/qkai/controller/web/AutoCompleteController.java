package qkai.controller.web;

import com.sun.jersey.spi.resource.Singleton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.persistence.Query;

import javax.ws.rs.*;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.commons.lang.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.logic.service.PersistenceService;

import qkai.model.db.PredicateAgent;
import qkai.model.kb.Tokenizer;
import qkai.model.kb.TokenizerResult;

import qkai.util.ConfigObj;
import qkai.util.rs.MediaType;

/**
 * Server of suggestions in {@code AutoComplete} fields.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
@Singleton
@Path("/autocomplete")
public final class AutoCompleteController {

	private static final String EMPTY_RESULT = "<ul></ul>";
	private static final String[] NAME_PREDICATE = {"http://xmlns.com/foaf/0.1/name", "http://www.w3.org/2000/01/rdf-schema#label"};

	/** Query for category names. */
	private static final String Q_CNAMES = "SELECT DISTINCT p.pvalue FROM categories_ft c JOIN properties p USING (resource) "
										   + "WHERE c.q=?b AND p.predicate IN ?name_pred LIMIT 25";

	/** Query to obtain existing {@code SearchToken} beginning by a string and having at least one loaded {@code Resource}. */
	private static final String Q_FETCH = "SELECT t.token FROM search_token t NATURAL JOIN findings f NATURAL JOIN resources r "
										  + "WHERE t.token LIKE ?c AND r.fully_loaded=1 GROUP BY t.token "
										  + "ORDER BY COUNT(0) DESC LIMIT 25";

	/** Query which solely uses present data, not any user input. */
	private static final String Q_PDATA = "SELECT DISTINCT p.pvalue FROM properties_fulltext ft JOIN properties p USING (property) "
										  + "WHERE ft.q=?a AND ft.predicate IN ?name_pred LIMIT 25";
	private static final Logger log = LoggerFactory.getLogger(AutoCompleteController.class);

	/**
	 * Assemblies unordered lists for output to {@code AutoComplete}.
	 *
	 * @param tokens tokens to be enlisted
	 * @return string representation of the UL
	 */
	static String assemblyUL(Collection<String> tokens) {
		if (tokens.isEmpty()) {
			return EMPTY_RESULT;
		} else {
			return "<ul><li>" + StringUtils.join(tokens, "</li><li>") + "</li></ul>";
		}
	}

	/**
	 * Gets the last token of a term.
	 *
	 * @info Used for queries fetching candidates for term completion.
	 * @param term term from which the token shall be extracted
	 * @return last term, might be an empty string
	 */
	static String getLastToken(final String term) {
		final int t = term.lastIndexOf(' ');

		return term.substring(t + 1);
	}

	/**
	 * Tells whether term seems to be an complete complex search query.
	 *
	 * <p>If so, it would be eligible skipping auto complete turn.</p>
	 *
	 * @param term term the user has written
	 * @return true, if auto completion should be skipped for this term as it is not complete
	 */
	static boolean isCompleteComplex(String term) {
		char letter = '"';
		int count = 0;

		for (int pos = -1; (pos = term.indexOf(letter, pos + 1)) != -1; count++) {
			// intentionally left blank
		}
		return (count % 2) == 0;
	}

	/**
	 * Constructs the Query for fulltext {@code AutoComplete}.
	 *
	 * @return {@code NativeQuery} without bound parameters as String
	 */
	static String constructACQuery() {
		final String afc = ConfigObj.getProperties().getProperty("qkai.autocomplete.triggers_fetching", "false");
		final String src = ConfigObj.getProperties().getProperty("qkai.autocomplete.data_from", "database");
		String finalQuery;
		String queryPresentData = Q_PDATA.replace("?name_pred",
									  "("
									  + StringUtils.join(PredicateAgent.getPredicateValueOf((List<String>) Arrays.asList(NAME_PREDICATE)),
										  ',') + ")");

		if ("false".equals(afc) || "database".equals(src)) {
			finalQuery = queryPresentData;
		} else if ("fetching".equals(src)) {
			finalQuery = Q_FETCH;
		} else {
			String subQuery1, subQuery2;

			if (src.startsWith("database")) {
				subQuery1 = queryPresentData;
				subQuery2 = Q_FETCH;
			} else {
				subQuery1 = Q_FETCH;
				subQuery2 = queryPresentData;
			}
			finalQuery = "(" + subQuery1 + ") UNION ALL (" + subQuery2 + ") LIMIT 25";
		}
		log.debug("Final query for fulltext AutoComplete: {}", finalQuery);
		return finalQuery;
	}

	/**
	 * Constructs the Query for fulltext {@code AutoComplete}.
	 *
	 * @return {@code NativeQuery} without bound parameters as String
	 */
	static String constructCatQuery() {
		String finalQuery = Q_CNAMES.replace("?name_pred",
								"("
								+ StringUtils.join(PredicateAgent.getPredicateValueOf((List<String>) Arrays.asList(NAME_PREDICATE)), ',')
								+ ")");

		log.debug("Final query for category AutoComplete: {}", finalQuery);
		return finalQuery;
	}

	/**
	 * Finds terms to suggest by {@code AutoComplete}.
	 *
	 * @param term term the user has provided so far
	 * @param finalQuery unmodifiable final query as string
	 * @return string representation of the UL ready to be passed back
	 */
	static String autoCompleteFor(final String term, final String finalQuery) {
		final String lastToken = getLastToken(term);

		if (lastToken.length() <= 2) {
			return EMPTY_RESULT;
		} else {
			final Query q = PersistenceService.getInstance().getEntityManager().createNativeQuery(finalQuery);

			if (finalQuery.contains("?a")) {
				q.setParameter("a", "(?t) | (?t*);mode=extended2;limit=1000".replaceAll("\\?t", term));
			}
			if (finalQuery.contains("?b")) {
				q.setParameter("b", "(?t) | (?t*);mode=extended2;sort=attr_desc:items;limit=25".replaceAll("\\?t", term));
			}
			if (finalQuery.contains("?c")) {
				q.setParameter("c", term + "%");
			}

			List<String> res = new ArrayList<String>(25);

			try {
				for (String t : (List<String>) q.getResultList()) {
					if (t.contains(" ")) {
						res.add("\"" + t + "\"");
					} else {
						res.add(t);
					}
				}
				log.debug("AutoComplete search for >{}< yielded {} results.", lastToken, res.size());
			} catch (Exception e) {
				log.warn("Uncaught exception during query", e);
			}
			return assemblyUL(res);
		}
	}

	/**
	 * Gets suggestions for {@code AutoComplete} derived from the term the user has provided so far.
	 *
	 * @param input term the user has provided so far
	 * @return string representation of the UL ready to be passed back
	 */
	@POST
	@Path("searchtoken")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces({MediaType.TEXT_JAVASCRIPT, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
	public String getSuggestions(@FormParam("input") String input) {
		String term = input.trim();

		log.debug("'input' for AutoCompletion is >{}<", input);
		if ((term.length() > 2) && isCompleteComplex(term)) {
			final TokenizerResult r = Tokenizer.parseText(null, term);

			if (r.getAcceptedToken().size() > 0) {
				if (ConfigObj.getProperties().getProperty("qkai.autocomplete.triggers_fetching", "false").equals("true")) {
					new FetchController().initiateFetching(term);
				}
				return autoCompleteFor(term.replaceFirst("\\\"", ""), constructACQuery());
			}
		}
		return EMPTY_RESULT;
	}

	/**
	 * Gets suggestions for {@code AutoComplete} derived from the category term the user has provided so far.
	 *
	 * @param input term the user has provided so far
	 * @return string representation of the UL ready to be passed back
	 */
	@POST
	@Path("category")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces({MediaType.TEXT_JAVASCRIPT, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
	public String getCategories(@FormParam("input") String input) {
		String term = input.trim();

		log.debug("'input' for AutoCompletion is >{}<", input);
		if ((term.length() > 2) && isCompleteComplex(term)) {
			final TokenizerResult r = Tokenizer.parseText(null, term);

			if (r.getAcceptedToken().size() > 0) {
				return autoCompleteFor(term.replaceFirst("\\\"", ""), constructCatQuery());
			}
		}
		return EMPTY_RESULT;
	}

}
