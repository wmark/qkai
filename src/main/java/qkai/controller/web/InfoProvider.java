package qkai.controller.web;

import com.sun.jersey.spi.resource.Singleton;

import java.util.Calendar;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.commons.lang.time.DateFormatUtils;

import org.json.JSONException;
import org.json.JSONObject;

import qkai.util.rs.MediaType;

/**
 * Serves debugging and information purposes.
 *
 * <p>I.e., when Safari users complain receiving XML instead of HTML
 * to show to them that their browser sets wrong 'accept' headers.</p>
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
@Singleton
@Path("/info")
public final class InfoProvider {

	/**
	 * Echoes request headers send by the client, in JSON.
	 *
	 * {@example "Example Output (Firefox 3.0.8)"
	 * {
	 *  "connection": ["keep-alive"],
	 *  "host": ["localhost:8080"],
	 *  "accept-language": ["de,en;q=0.9,pl;q=0.7,cs;q=0.6,zh-cn;q=0.4,zh;q=0.3,la;q=0.1"],
	 *  "accept": ["text/html,application/xhtml+xml,application/xml;q=0.9"],
	 *  "keep-alive": ["300"],
	 *  "user-agent": ["Mozilla/5.0 (Windows; U; Windows NT 6.0; de; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8 (.NET CLR 3.5.30729)"],
	 *  "accept-encoding": ["gzip,deflate"],
	 *  "accept-charset": ["ISO-8859-1,utf-8;q=0.7,*;q=0.7"]
	 * }}
	 *
	 * @param hh automatically set by JAX-WS
	 * @return JSON dictionary, as shown above
	 * @throws org.json.JSONException if headers contain unparseable Strings
	 */
	@GET
	@Path("request")
	@Produces(MediaType.APPLICATION_JSON)
	public String getRequestHeaders(@Context final HttpHeaders hh) throws JSONException {
		final JSONObject jo = new JSONObject();
		final MultivaluedMap<String, String> requestHeaders = hh.getRequestHeaders();

		for (Map.Entry e : requestHeaders.entrySet()) {
			jo.put(e.getKey().toString(), e.getValue());
		}
		return jo.toString();
	}

	/**
	 * Gets the server's current date and time in the international ISO 8601 format.
	 *
	 * {@example "Example Output"
	 * 2009-04-21T18:02:22+02:00
	 * }
	 *
	 * @return ISO 8601 date and time as plaintext
	 */
	@GET
	@Path("datetime")
	@Produces(MediaType.TEXT_PLAIN)
	public String getDateTime() {
		return DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.format(Calendar.getInstance());
	}
}
