package qkai.controller.web;

import com.sun.jersey.api.view.Viewable;
import com.sun.jersey.spi.resource.Singleton;

import java.io.IOException;

import java.util.concurrent.TimeUnit;

import javax.ws.rs.*;

//~--- non-JDK imports --------------------------------------------------------

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.logic.iunit.WikipediaExtractor;

import qkai.util.rs.MediaType;

/**
 * Front controller to {@link WikipediaExtractorController}.
 *
 * @todo yield an 404 error page if corresponding wiki page does not exist
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
@Singleton
@Path("/WikipediaExtractor")
public class WikipediaExtractorController {

	private static final Logger log = LoggerFactory.getLogger(WikipediaExtractorController.class);
	private static final Viewable index_page;

	static {
		try {
			index_page = new Viewable("/jsp/form.jsp", null);
		} catch (Exception e) {
			throw(RuntimeException) new RuntimeException().initCause(e);
		}
	}

	/**
	 * Serves the front page of the Wikipedia information unit extractor.
	 *
	 * @return {@code /jsp/form.jsp}
	 */
	@GET
	public Viewable index() {
		return index_page;
	}

	/**
	 * Serves the extracted information units as XML.
	 *
	 * @impl wrapper to {@link qkai.logic.iunit.WikipediaExtractor#extract}
	 * @param pageTitle title of the Wikipedia page to be extracted
	 * @param language language of the Wikipedia subdomain as abbreviation according to ISO 639-2
	 * @return XML representation as String meant to be served to clients
	 * @throws java.io.IOException if the Wikipedia page cannot be read due to network failure or refusal
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
	public String extractInformationUnits(@FormParam("pageTitle") String pageTitle, @DefaultValue("en")
	@FormParam("language") String language) throws IOException {
		long elapsed = System.nanoTime();
		final String response = WikipediaExtractor.extract(pageTitle, language);

		elapsed = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - elapsed);
		if (elapsed >= 500) {
			log.warn("Extracting information units took {}ms", elapsed);
		}
		return response;
	}

	/**
	 * Convenience wrapper to {@link #extractInformationUnits(java.lang.String, java.lang.String)}.
	 *
	 * @impl wrapper to {@link qkai.logic.iunit.WikipediaExtractor#extract} with {@literal "en"} as assumed language
	 * @param pageTitle title of the Wikipedia page to be extracted
	 * @return XML representation as String meant to be served to clients
	 * @throws java.io.IOException if the Wikipedia page cannot be read due to network failure or refusal
	 */
	@GET
	@Path("/{pageTitle: [a-zA-Z0-9_\\-\\(\\)\\:\\ ]+}")
	@Produces({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
	public String extractInformationUnits(@PathParam("pageTitle") String pageTitle) throws IOException {
		return this.extractInformationUnits(pageTitle, "en");
	}

}
