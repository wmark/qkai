package qkai.controller.web;

import com.sun.jersey.api.Responses;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

//~--- non-JDK imports --------------------------------------------------------

import qkai.util.rs.MediaType;

/**
 * For maintaining a consistent JSON error response API.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public class NotFoundException extends WebApplicationException {

	/**
	 * Create a HTTP 404 (Not Found) exception.
	 */
	public NotFoundException() {
		super(Responses.notFound().build());
	}

	/**
	 * Create a HTTP 404 (Not Found) exception.
	 *
	 * @param   message the String that is the entity of the 404 response.
	 */
	public NotFoundException(final String message) {
		super(Response.status(Responses.NOT_FOUND).entity("{\"error\": \"" + message
							  + "\"}").type(MediaType.APPLICATION_JSON_TYPE).build());
	}
}
