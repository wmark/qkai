package qkai.controller.web;

import com.sun.jersey.spi.resource.Singleton;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.persistence.Query;

import javax.ws.rs.*;

//~--- non-JDK imports --------------------------------------------------------

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import qkai.logic.poi.PoiMediator;

import qkai.util.rs.MediaType;

/**
 * Responsible for search space limitations.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
@Singleton
@Path("/poi")
public class PoiController {

	private static final Logger log = LoggerFactory.getLogger(PoiController.class);

	/**
	 * Gets member {@code Resource}s of search space limited by POI.
	 *
	 * @param poiBuilder JSON representation of a PoiBuilder.
	 * @return unmodifiable list of primary IDs of Resources matching given POI
	 */
	List<Long> getResourceIdsOfPoi(JSONObject poiBuilder) {
		try {
			Query q = PoiMediator.queryOf(poiBuilder);

			return q.getResultList();
		} catch (IllegalStateException e) {
			log.info("Un-POI-able request detected: {}", poiBuilder.toString());
			return new LinkedList<Long>();
		}
	}

	/**
	 * Selects {@code Resource}s matching a POI.
	 *
	 * @param poi_id request identifier, used by the client to detect out-of-order responses
	 * @param raw_poi JSON representation of the POI Builder
	 * @return JSON String, with members 'PoiId' and 'items'
	 */
	@POST
	@Path("resources")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces({MediaType.TEXT_JAVASCRIPT, MediaType.APPLICATION_JSON})
	public String getResources(@FormParam("PoiId") int poi_id, @FormParam("poi") String raw_poi) {
		if (raw_poi == null) {
			throw new BadRequestException();
		}
		log.debug("Got request for POI with ID={} and POI={}", poi_id, raw_poi);
		try {
			JSONObject poi = new JSONObject(raw_poi);
			JSONObject response = new JSONObject();
			long time1, elapsed;

			time1 = System.nanoTime();

			List<Long> resourceIDs = getResourceIdsOfPoi(poi);

			elapsed = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time1);
			response.put("PoiId", poi_id);
			response.put("items", new JSONArray(resourceIDs));
			response.put("elapsed", elapsed);
			if (elapsed >= 500) {
				log.warn("Setting POI took {}ms", elapsed);
			} else {
				log.trace("Setting POI took {}ms", elapsed);
			}
			return response.toString();
		} catch (JSONException e) {
			log.info("JSON input seems malformed.", e);
			throw(BadRequestException) new BadRequestException().initCause(e);
		}
	}

}
