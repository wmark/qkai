package qkai.controller.web;

import java.net.HttpURLConnection;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

//~--- non-JDK imports --------------------------------------------------------

import qkai.util.rs.MediaType;

/**
 * Translator of (intentionally) uncaught regular exceptions to JSON errors.
 *
 * @impl For every {@code Exception} an {@link  ExceptionMapper} has to be implemented,
 * which yields in another {@link #toResponse} method.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
@Provider
public final class ExceptionMapperForJSON implements ExceptionMapper<java.io.IOException> {

	private String embedErrorInJSON(final String message) {
		return "{\"error\": \"" + message + "\"}";
	}

	/**
	 * Translates exception to regular JSON output with appropriate HTTP response code.
	 *
	 * @param ex the Exception is set by JAX-WS automatically
	 * @return Response, which carries an HTTP status as response code and a HTTP body, with the JSON represantation of that exception
	 */
	@Override
	public Response toResponse(final java.io.IOException ex) {
		return Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR).entity(
			embedErrorInJSON("a ressource needed to process your request couldn't be accessed")).type(
			MediaType.APPLICATION_JSON_TYPE).build();
	}

}
