package qkai.controller.web;

import com.sun.jersey.api.view.Viewable;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

//~--- non-JDK imports --------------------------------------------------------

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Server to the SE front page.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
@Singleton
@Path("/")
public class SEController {

	private static final Logger log = LoggerFactory.getLogger(SEController.class);
	private static final Viewable index_page;

	static {
		try {
			index_page = new Viewable("/index.jsp", null);
		} catch (Exception e) {
			throw(RuntimeException) new RuntimeException().initCause(e);
		}
	}

	/**
	 * Serves the front page.
	 *
	 * @return {@code /jsp/form.jsp}
	 */
	@GET
	public Viewable index() {
		return index_page;
	}

	/**
	 * Serves the front page.
	 *
	 * @return {@code /jsp/form.jsp}
	 */
	@GET
	@Path("se")
	public Viewable sePage() {
		log.debug("Hit on alternate index page.");
		return index_page;
	}

}
