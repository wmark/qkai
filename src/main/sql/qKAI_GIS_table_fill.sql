--
-- Does fill the auxiliary table properties_pt.
-- Ellipsoid no23 is WGS84.
--
-- @author W-Mark Kubacki; wmark@hurrikane.de
--

TRUNCATE TABLE properties_pt;

SELECT @pred_lat:=resource FROM resources WHERE resourceURI="http://www.w3.org/2003/01/geo/wgs84_pos#lat";
SELECT @pred_lon:=resource FROM resources WHERE resourceURI="http://www.w3.org/2003/01/geo/wgs84_pos#long";

ALTER TABLE properties_pt DISABLE KEYS;
LOCK TABLES resources AS rlat READ, resources AS rlon READ, properties AS lat READ, properties AS lon READ, properties_pt WRITE;
INSERT IGNORE INTO properties_pt
	SELECT lat.resource, POINTFROMTEXT(LatLong2UTM(23, lat.pvalue, lon.pvalue)), lat.pvalue, lon.pvalue
	FROM properties lat JOIN properties lon 
                          ON (lat.predicate=@pred_lat AND lon.predicate=@pred_lon AND lat.resource=lon.resource)
	WHERE POINTFROMTEXT(LatLong2UTM(23, lat.pvalue, lon.pvalue)) IS NOT NULL;
UNLOCK TABLES;
ALTER TABLE properties_pt ENABLE KEYS;
