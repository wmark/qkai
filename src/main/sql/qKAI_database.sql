-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.34-qkai


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Temporary table structure for view `object_count`
--
DROP TABLE IF EXISTS `object_count`;
DROP VIEW IF EXISTS `object_count`;
CREATE TABLE `object_count` (
  `SearchToken` bigint(21),
  `findings` bigint(21),
  `Resource` bigint(21),
  `Property` bigint(21),
  `Relation` bigint(21)
);

--
-- Definition of table `categories_ft`
--

DROP TABLE IF EXISTS `categories_ft`;
CREATE TABLE  `categories_ft` (
  `resource` int(10) unsigned NOT NULL,
  `weight` int(11) NOT NULL,
  `q` varchar(3072) NOT NULL,
  `items` int(10) unsigned NOT NULL,
  KEY `q` (`q`(1024))
) ENGINE=SPHINX DEFAULT CHARSET=utf8 CONNECTION='sphinx://127.0.0.1:3312/idx_categories';

--
-- Dumping data for table `properties_fulltext`
--

/*!40000 ALTER TABLE `categories_ft` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories_ft` ENABLE KEYS */;


--
-- Definition of table `findings`
--

DROP TABLE IF EXISTS `findings`;
CREATE TABLE `findings` (
  `tid` int(10) unsigned NOT NULL,
  `resource` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tid`,`resource`) USING BTREE,
  KEY `f_result` (`resource`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

--
-- Dumping data for table `findings`
--

/*!40000 ALTER TABLE `findings` DISABLE KEYS */;
/*!40000 ALTER TABLE `findings` ENABLE KEYS */;


--
-- Definition of table `properties`
--

DROP TABLE IF EXISTS `properties`;
CREATE TABLE  `properties` (
  `property` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resource` int(10) unsigned NOT NULL,
  `predicate` int(10) unsigned NOT NULL,
  `ptype` enum('literal','typed-literal') NOT NULL DEFAULT 'literal',
  `pvalue` text NOT NULL,
  `lang` char(2) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`property`,`predicate`),
  KEY `f_resource` (`resource`),
  KEY `f_predicate` (`predicate`),
  KEY `idx_ptype` (`ptype`),
  KEY `pi_value` (`pvalue`(4))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC
/*!50100 PARTITION BY HASH (predicate)
PARTITIONS 64 */;

--
-- Dumping data for table `properties`
--

/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;


--
-- Definition of table `properties_fulltext`
--

DROP TABLE IF EXISTS `properties_fulltext`;
CREATE TABLE  `properties_fulltext` (
  `property` int(10) unsigned NOT NULL,
  `weight` int(11) NOT NULL,
  `q` varchar(3072) NOT NULL,
  `resource` int(10) unsigned NOT NULL,
  `predicate` int(10) unsigned NOT NULL,
  KEY `q` (`q`(1024))
) ENGINE=SPHINX DEFAULT CHARSET=utf8 CONNECTION='sphinx://127.0.0.1:3312/idx_properties,idx_delta_properties';

--
-- Dumping data for table `properties_fulltext`
--

/*!40000 ALTER TABLE `properties_fulltext` DISABLE KEYS */;
/*!40000 ALTER TABLE `properties_fulltext` ENABLE KEYS */;


--
-- Definition of table `properties_pt`
--

DROP TABLE IF EXISTS `properties_pt`;
CREATE TABLE  `properties_pt` (
  `resource` bigint(20) unsigned NOT NULL,
  `location` point NOT NULL,
  `latitude` FLOAT NOT NULL,
  `longitude` FLOAT NOT NULL,
  PRIMARY KEY (`resource`),
  SPATIAL KEY `location` (`location`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

--
-- Dumping data for table `properties_tp`
--

/*!40000 ALTER TABLE `properties_pt` DISABLE KEYS */;
/*!40000 ALTER TABLE `properties_pt` ENABLE KEYS */;


--
-- Definition of table `relations`
--

DROP TABLE IF EXISTS `relations`;
CREATE TABLE `relations` (
  `relation` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resource` int(10) unsigned NOT NULL,
  `predicate` int(10) unsigned NOT NULL,
  `target` int(10) unsigned NOT NULL,
  `n` int(11) DEFAULT NULL,
  PRIMARY KEY (`relation`,`predicate`),
  KEY `u_relation` (`resource`,`predicate`,`target`),
  KEY `f_to` (`target`),
  KEY `f_predicate2` (`predicate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED
/*!50100 PARTITION BY HASH (predicate)
PARTITIONS 64 */;

--
-- Dumping data for table `relations`
--

/*!40000 ALTER TABLE `relations` DISABLE KEYS */;
/*!40000 ALTER TABLE `relations` ENABLE KEYS */;


--
-- Definition of table `resources`
--

DROP TABLE IF EXISTS `resources`;
CREATE TABLE `resources` (
  `resource` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resourceURI` varchar(255) NOT NULL,
  `fully_loaded` tinyint(1) NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hint` enum('NODE','PROPERTY','USER') NOT NULL DEFAULT 'NODE',
  PRIMARY KEY (`resource`),
  UNIQUE KEY `idx_uri` (`resourceURI`),
  KEY `idx_hint` (`hint`),
  KEY `idx_loaded` (`fully_loaded`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `resources`
--

/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;


--
-- Definition of table `search_token`
--

DROP TABLE IF EXISTS `search_token`;
CREATE TABLE `search_token` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(64) NOT NULL,
  `kb` varchar(16) NOT NULL,
  PRIMARY KEY (`tid`),
  UNIQUE KEY `i_token` (`token`,`kb`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `search_token`
--

/*!40000 ALTER TABLE `search_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_token` ENABLE KEYS */;

--
-- Definition of table `sph_counter`
--

DROP TABLE IF EXISTS `sph_counter`;
CREATE TABLE  `sph_counter` (
  `counter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `min_p_id` int(10) unsigned NOT NULL,
  `max_p_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`counter_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

--
-- Dumping data for table `sph_counter`
--

/*!40000 ALTER TABLE `sph_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `sph_counter` ENABLE KEYS */;



--
-- Definition of view `object_count`
--

DROP TABLE IF EXISTS `object_count`;
DROP VIEW IF EXISTS `object_count`;
CREATE VIEW `object_count` AS select (select count(0) AS `COUNT(*)` from `search_token`) AS `SearchToken`,(select count(0) AS `COUNT(*)` from `findings`) AS `findings`,(select count(0) AS `COUNT(*)` from `resources`) AS `Resource`,(select count(0) AS `COUNT(*)` from `properties`) AS `Property`,(select count(0) AS `COUNT(*)` from `relations`) AS `Relation`;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;