--------------------------------------------------------------------------------
-- Clears all tables; handy for testing purposes.
DROP PROCEDURE IF EXISTS `clear_tables`;

DELIMITER $$

CREATE PROCEDURE `clear_tables`()
BEGIN
TRUNCATE TABLE search_token;
TRUNCATE TABLE findings;
TRUNCATE TABLE properties;
TRUNCATE TABLE relations;
TRUNCATE TABLE resources;
TRUNCATE TABLE resources_pt;
TRUNCATE TABLE sph_counter;
END $$

DELIMITER ;

--------------------------------------------------------------------------------
-- Resources around a known resource.
DELIMITER $$

DROP PROCEDURE IF EXISTS `RES_AROUND_RES` $$
CREATE PROCEDURE `RES_AROUND_RES` (other_resource BIGINT(20), radius FLOAT UNSIGNED)
	DETERMINISTIC READS SQL DATA
BEGIN
	DECLARE x0 FLOAT;
	DECLARE y0 FLOAT;
	DECLARE lat FLOAT;
	DECLARE lon FLOAT;
	DECLARE bbox POLYGON;

	SELECT X(c.location), Y(c.location), c.latitude, c.longitude INTO x0, y0, lat, lon
	FROM properties_pt c WHERE c.resource=other_resource;

	SET bbox = ENVELOPE(GEOMFROMTEXT(CONCAT('MULTIPOINT(', x0-(radius*1000), ' ', y0-(radius*1000), ', ', x0+(radius*1000), ' ', y0+(radius*1000), ')')));

	SELECT pt.resource, simple_distance(pt.latitude, pt.longitude, lat, lon) km, pt.latitude, pt.longitude
	FROM properties_pt pt
	WHERE MBRContains(bbox, pt.location) AND simple_distance(pt.latitude, pt.longitude, lat, lon) <= radius
	ORDER BY km ASC;
END $$

DELIMITER ;

--------------------------------------------------------------------------------
-- Resource around given latitude and longitude (in WGS-84).
-- This procedure is exact and does not rely on bounding box alone.
DELIMITER $$

DROP PROCEDURE IF EXISTS `RES_AROUND_POINT` $$
CREATE PROCEDURE `RES_AROUND_POINT` (lat FLOAT, lon FLOAT, radius FLOAT UNSIGNED)
	DETERMINISTIC READS SQL DATA
BEGIN
	DECLARE x0 FLOAT;
	DECLARE y0 FLOAT;
	DECLARE bbox POLYGON;

	SET x0 = X(POINTFROMTEXT(LatLong2UTM(23, lat, lon)));
	SET y0 = Y(POINTFROMTEXT(LatLong2UTM(23, lat, lon)));
	SET bbox = ENVELOPE(GEOMFROMTEXT(CONCAT('MULTIPOINT(', x0-(radius*1000), ' ', y0-(radius*1000), ', ', x0+(radius*1000), ' ', y0+(radius*1000), ')')));

	SELECT pt.resource, simple_distance(pt.latitude, pt.longitude, lat, lon) km, pt.latitude, pt.longitude
	FROM properties_pt pt
	WHERE MBRContains(bbox, pt.location) AND simple_distance(pt.latitude, pt.longitude, lat, lon) <= radius
	ORDER BY km ASC;
END $$

DELIMITER ;
