package integration;

import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;

import com.meterware.httpunit.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.junit.*;

/**
 * Test class for {@link qkai.controller.web.InfoProvider} for checking server's behaviour.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public class BusinessTest {

	private static final String acCategory = "http://localhost:8012/autocomplete/category";
	private static final String acSearchToken = "http://localhost:8012/autocomplete/searchtoken";
	private static final String homeUrl = "http://localhost:8012/";
	private static final String poiUrl = "http://localhost:8012/poi/resources";
	private static final String textUrl = "http://localhost:8012/resource/251518,430772/text";
	private static final String imggeoUrl = "http://localhost:8012/resource/410,510/image,geo";
	private static final String ERR_MSG = "Unexpected exception in test. Is a webserver running at " + homeUrl + " ? ->";
	private WebConversation wc;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() {
		wc = new WebConversation();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {}

	/**
	 * Test whether main page contains necessary elements.
	 */
	@Ignore("httpunit has JS errors")
	@Test
	public void testMainPage() {
		try {
			WebResponse res = wc.getResponse(homeUrl);

			assertThat(res.getText(), containsString("src=\"js/qkai_se.js\""));
			assertThat(res.getText(), containsString("qkai.doSearch()"));
			assertThat(res.getText(), containsString("id=\"search_form_token\""));
			assertThat(res.getText(), containsString("id=\"cat_form_token\""));
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}

	/**
	 * Test whether autocompletion of categories works with input "Univ".
	 */
	@Test
	public void testCategoryAC1() {
		try {
			PostMethodWebRequest req = new PostMethodWebRequest(acCategory);

			req.setParameter("input", "Univer");

			WebResponse res = wc.getResponse(req);

			assertThat(res.getText(), containsString("<ul><li>"));
			assertThat(res.getText(), containsString("</li></ul>"));
			assertThat(res.getText(), containsString("\"University of"));
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}

	/**
	 * Test whether autocompletion of categories works with input "University Han".
	 */
	@Test
	public void testCategoryAC2() {
		try {
			PostMethodWebRequest req = new PostMethodWebRequest(acCategory);

			req.setParameter("input", "University Han");

			WebResponse res = wc.getResponse(req);

			assertThat(res.getText(), containsString("\"University of Hanover\""));
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}

	/**
	 * Test whether autocompletion of search text works.
	 */
	@Test
	public void testSearchtextACpositive() {
		try {
			PostMethodWebRequest req = new PostMethodWebRequest(acSearchToken);

			req.setParameter("input", "Leibn");

			WebResponse res = wc.getResponse(req);

			assertThat(res.getText(), containsString("<ul><li>"));
			assertThat(res.getText(), containsString("</li></ul>"));
			assertThat(res.getText(), containsString("\"Gottfried Wilhelm Leibniz\""));
			assertThat(res.getText(), containsString("<li>Leibnitz</li>"));
			assertThat(res.getText(), containsString("\"Gottfried Wilhelm Leibniz Universität Hannover\""));
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}

	/**
	 * Test whether autocompletion of search text works if text cannot be resolved.
	 */
	@Test
	public void testSearchtextACnegative() {
		try {
			PostMethodWebRequest req = new PostMethodWebRequest(acSearchToken);

			req.setParameter("input", "dgdfgdfg");

			WebResponse res = wc.getResponse(req);

			assertThat(res.getText(), containsString("<ul></ul>"));
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}

	/**
	 * Tests whether {@code POI}S answer is correct.
	 */
	@Test
	public void testPoiAnswer() {
		try {
			PostMethodWebRequest req = new PostMethodWebRequest(poiUrl);

			req.setParameter("PoiId", "4");
			req.setParameter("poi", "{\"fulltext\": \"Leibniz Hannover\", \"category\": \"University\"}");

			WebResponse res = wc.getResponse(req);
			JSONObject jo = new JSONObject(res.getText());

			assertTrue(jo.has("elapsed"));
			assertTrue(jo.has("items"));
			assertTrue(jo.has("PoiId"));
			assertEquals(4, jo.getInt("PoiId"));
		} catch (JSONException e) {
			fail(e.getMessage());
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}

	/**
	 * Tests searching by text.
	 */
	@Test
	public void testSearchtextPoi() {
		try {
			PostMethodWebRequest req = new PostMethodWebRequest(poiUrl);

			req.setParameter("PoiId", "1");
			req.setParameter("poi", "{\"fulltext\": \"Leibniz Hannover\"}");

			WebResponse res = wc.getResponse(req);
			JSONObject jo = new JSONObject(res.getText());

			assertTrue(jo.has("items"));
			assertThat(jo.getJSONArray("items").length(), greaterThanOrEqualTo(1));
		} catch (JSONException e) {
			fail(e.getMessage());
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}

	/**
	 * Tests setting category POI.
	 */
	@Test
	public void testCategoryPoi() {
		try {
			PostMethodWebRequest req = new PostMethodWebRequest(poiUrl);

			req.setParameter("PoiId", "2");
			req.setParameter("poi", "{\"category\": \"University\"}");

			WebResponse res = wc.getResponse(req);
			JSONObject jo = new JSONObject(res.getText());

			assertTrue(jo.has("items"));
			assertThat(jo.getJSONArray("items").length(), greaterThanOrEqualTo(1));
		} catch (JSONException e) {
			fail(e.getMessage());
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}

	/**
	 * Tests whether a combination of text and category yields expected results
	 */
	@Test
	public void testTextCategoryPoi() {
		try {
			PostMethodWebRequest req = new PostMethodWebRequest(poiUrl);

			req.setParameter("PoiId", "5");
			req.setParameter("poi", "{\"fulltext\": \"Leibniz Hannover\", \"category\": \"University\"}");

			WebResponse res = wc.getResponse(req);
			JSONObject jo = new JSONObject(res.getText());

			assertTrue(jo.has("items"));
			assertThat(jo.getJSONArray("items").length(), greaterThanOrEqualTo(1));
		} catch (JSONException e) {
			fail(e.getMessage());
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}

	/**
	 * Tests whether for a given set of Resources correct text is returned.
	 */
	@Test
	public void testTextReturn() {
		try {
			WebResponse res = wc.getResponse(textUrl);

			assertThat(res.getText(), containsString("elapsed"));
			assertThat(res.getText(), containsString("items"));

			JSONObject jo = new JSONObject(res.getText());

			assertTrue(jo.has("elapsed"));
			assertTrue(jo.has("items"));

			JSONArray items = jo.getJSONArray("items");

			assertEquals(2, items.length());
			for (int i = 0; i < 2; ) {
				JSONObject e = items.getJSONObject(i++);

				assertTrue(e.has("URI"));
				assertTrue(e.has("caption"));
				assertTrue(e.has("comment"));
				// assertTrue(e.has("description"));
				assertTrue(e.has("id"));
				assertTrue(e.has("lastUpdate"));
			}
		} catch (JSONException e) {
			fail(e.getMessage());
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}

	/**
	 * Tests whether for a given set of Resources correct image and geolocation is returned.
	 */
	@Test
	public void testImgGeoReturn() {
		try {
			WebResponse res = wc.getResponse(imggeoUrl);

			assertThat(res.getText(), containsString("elapsed"));
			assertThat(res.getText(), containsString("items"));

			JSONObject jo = new JSONObject(res.getText());

			assertTrue(jo.has("elapsed"));
			assertTrue(jo.has("items"));

			JSONArray items = jo.getJSONArray("items");

			assertEquals(2, items.length());
			for (int i = 0; i < 2; ) {
				JSONObject e = items.getJSONObject(i++);

				assertTrue(e.has("id"));
				assertTrue(e.has("predicate"));
				assertTrue(e.has("latitude"));
				assertTrue(e.has("longitude"));
				assertTrue(e.has("imageURI"));
			}
		} catch (JSONException e) {
			fail(e.getMessage());
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}
}
