package integration;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.matchers.JUnitMatchers.containsString;

import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebResponse;

import org.json.JSONException;
import org.json.JSONObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for {@link qkai.controller.web.InfoProvider} for checking server's behaviour.
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public class InfoProviderIntegrationTest {

	private static final String URL = "http://localhost:8012/info/request";
	private static final String ERR_MSG = "Unexpected exception in test. Is a webserver running at " + URL + " ? ->";
	private WebConversation wc;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() {
		wc = new WebConversation();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {}

	/**
	 * Test method for {@link qkai.controller.web.InfoProvider#getRequestHeaders}.
	 */
	@Test
	public void getRequestHeaders() {
		try {
			WebResponse res = wc.getResponse(URL);

			assertThat(res.getText(), containsString("\"Host\":"));

			JSONObject jo = new JSONObject(res.getText());

			assertTrue(jo.has("Host"));
		} catch (JSONException e) {
			fail("JSON string seems malformed. -> " + e);
		} catch (Exception e) {
			fail(ERR_MSG + e);
		}
	}

}
