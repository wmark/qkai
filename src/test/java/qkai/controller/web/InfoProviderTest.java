package qkai.controller.web;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//~--- non-JDK imports --------------------------------------------------------

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test class for {@link InfoProvider}
 *
 * @author      W-Mark Kubacki
 * @version     $Revision$
 */
public class InfoProviderTest {

	/**
	 * Test method for {@link InfoProvider#getDateTime} for checking raw output.
	 * 
	 * @throws java.lang.Exception
	 */
	@Test
	public void testGetTimeDate() {
			InfoProvider ip = new InfoProvider();
			String dt = ip.getDateTime();

			Pattern p = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}");
			Matcher m = p.matcher(dt);

			assertTrue(m.find());
	}
}
