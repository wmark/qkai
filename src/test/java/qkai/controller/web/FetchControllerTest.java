package qkai.controller.web;

import static org.junit.Assert.*;

import org.junit.*;

/**
 * Test class for {@link FetchController}
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
public class FetchControllerTest {

	/**
	 * Test method for {@link FetchController#initiateFetching} to check whether facade finds KBs and proxies returns.
	 * @throws java.io.IOException
	 */
	@Ignore("testrun is expensive") @Test
	public void initiateFetching() throws java.io.IOException {
		String kb = new String("DBPedia");
		String term = new String("Hanover Leibniz \"University of Hanover\"");
		FetchController controller = new FetchController();
		String result = controller.initiateFetching(term);

		assertFalse(result.isEmpty());
	}
}
