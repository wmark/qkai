package qkai.model.db;

import javax.persistence.EntityManager;

//~--- non-JDK imports --------------------------------------------------------

import static org.junit.Assert.*;

import org.junit.*;

import qkai.logic.service.PersistenceService;

/**
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
public class PropertyTest {

	private EntityManager em;

	@Before
	public void doSetup() {
		em = PersistenceService.getInstance().getEntityManager();
	}

	@After
	public void destroy() {
		// em.close();
	}

	@Test
	public void testGetProperty() {
		long prop = 2022L;
		Property p = em.find(Property.class, prop);

		assertTrue(p.getID() == prop);
	}

	@Test
	public void testGetPtype() {
		Property p = em.find(Property.class, 2022L);

		assertEquals("literal", p.getPtype());
	}

	@Test
	public void testGetPvalue() {
		Property p = em.find(Property.class, 2022L);

		assertEquals("Stephan Weil", p.getPvalue());
	}

	@Test
	public void testGetLang() {
		Property p = em.find(Property.class, 2022L);
		Property d = em.find(Property.class, 1975L);

		assertEquals("en", p.getLang());
		assertNull(d.getLang());
	}

	@Test
	public void testGetResource() {
		Property p = em.find(Property.class, 2022L);

		assertNotNull(p.getResource());
		assertTrue(p.getResource().getID() == 865L);
	}

	/**
	 * Does equals work as expected?
	 */
	@Test
	public void equalsOfProperty() {
		Property m = new Property(em.find(Resource.class, 865L), em.find(Resource.class, 1819L), "literal", "Stephan Weil", "en");
		Property a = em.find(Property.class, 2022L);
		Property a2 = em.find(Property.class, 2022L);
		Property b = em.find(Property.class, 1975L);

		// reflexive
		assertTrue("a not reflexive", a.equals(a));
		assertTrue("a not reflexive on a2", a.equals(a2));
		assertTrue("b not reflexive", b.equals(b));
		// symmetric
		assertFalse("a should not equal b", a.equals(b));
		assertFalse("b should not equal a", b.equals(a));
		assertTrue("a must equal m", a.equals(m));
		assertTrue("m must equal a", m.equals(a));
		// consistency
		assertFalse("b should not equal a", b.equals(a));
		assertFalse("b should not equal a", b.equals(a));
		// null
		assertFalse("a.equals(null) must be false", a.equals(null));
		assertFalse("b.equals(null) must be false", b.equals(null));
	}

	@Test
	public void testHashCode() {
		Property m = new Property(em.find(Resource.class, 865L), em.find(Resource.class, 1819L), "literal", "Stephan Weil", "en");
		Property a = em.find(Property.class, 2022L);
		Property b = em.find(Property.class, 1975L);

		assertFalse(m.hashCode() == 0);
		assertFalse(a.hashCode() == 0);
		assertFalse(b.hashCode() == 0);
		if (m.equals(a)) {
			assertEquals(m.hashCode(), a.hashCode());
		}
		if (!a.equals(b)) {
			assertTrue(a.hashCode() != b.hashCode());
		}
	}

	@Test
	public void testToString() {
		Property a = em.find(Property.class, 2022L);

		assertEquals(
			"qkai.model.db.Property({\"predicate\":1819,\"resource\":865,\"pvalue\":\"Stephan Weil\",\"lang\":\"EN\",\"ptype\":\"literal\"})",
			a.toString());
	}

}
