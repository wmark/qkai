package qkai.model.db;

import javax.persistence.EntityManager;

//~--- non-JDK imports --------------------------------------------------------

import static org.junit.Assert.*;

import org.junit.*;

import qkai.logic.service.PersistenceService;

/**
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
public class ResourceTest {

	private EntityManager em;

	@Before
	public void doSetup() {
		em = PersistenceService.getInstance().getEntityManager();
	}

	@After
	public void destroy() {
		// em.close();
	}

	@Ignore("JPA does harm tests if EntityManager is closed once") @Test
	public void testValueOf() {
		String uri = "http://dbpedia.org/resource/Hanover";
		Resource a = Resource.valueOf(uri, Resource.ContentHint.NODE);
		Resource b = Resource.valueOf(uri, Resource.ContentHint.NODE);

		assertNotNull(a.getID());
		assertTrue(a == b);
	}

	@Test
	public void testGetPropertyCollection() {
		Resource a = em.find(Resource.class, 865L);

		assertTrue(a.getPropertyCollection().size() > 0);
		assertTrue(a.getPropertyCollection().contains(em.find(Property.class, 2022L)));
	}

	@Test
	public void testGetResource() {
		Resource a = em.find(Resource.class, 1L);

		assertTrue(a.getID() == 1L);
	}

	@Test
	public void testGetResourceURI() {
		String uri = "http://dbpedia.org/resource/Hanover";
		Resource a = Resource.valueOf(uri, Resource.ContentHint.NODE);

		assertEquals(uri, a.getResourceURI());
	}

	@Test
	public void testGetFullyLoaded() {
		Resource a = em.find(Resource.class, 1L);
		Resource b = em.find(Resource.class, 1274L);

		assertTrue(a.isFullyLoaded());
		assertFalse(b.isFullyLoaded());
	}

	@Test
	public void testSetFullyLoaded() {
		Resource b = em.find(Resource.class, 1274L);

		if (!b.isFullyLoaded()) {
			b.setFullyLoaded(true);
			assertTrue(b.isFullyLoaded());
			b.setFullyLoaded(false);
		}
	}

	/**
	 * Does equals work as expected?
	 */
	@Test
	public void equalsOfResource() {
		Resource a = em.find(Resource.class, 4L);
		Resource a2 = em.find(Resource.class, 4L);
		Resource b = em.find(Resource.class, 17L);

		// reflexive
		assertTrue("a not reflexive", a.equals(a));
		assertTrue("a not reflexive on a2", a.equals(a2));
		assertTrue("b not reflexive", b.equals(b));
		// symmetric
		assertFalse("a should not equal b", a.equals(b));
		assertFalse("b should not equal a", b.equals(a));
		// consistency
		assertFalse("b should not equal a", b.equals(a));
		assertFalse("b should not equal a", b.equals(a));
		// null
		assertFalse("a.equals(null) must be false", a.equals(null));
		assertFalse("b.equals(null) must be false", b.equals(null));
	}

	@Test
	public void testHashCode() {
		Resource a = em.find(Resource.class, 1L);
		Resource b = em.find(Resource.class, 865L);

		assertFalse(a.hashCode() == 0);
		assertFalse(b.hashCode() == 0);
		if (!a.equals(b)) {
			assertTrue(a.hashCode() != b.hashCode());
		}
	}

	@Test
	public void testToString() {
		Resource a = em.find(Resource.class, 865L);

		assertEquals(
			"qkai.model.db.Resource({\"last_update\":\"Mon Apr 20 19:00:05 CEST 2009\",\"resourceURI\":\"http://dbpedia.org/resource/Hanover\",\"resource\":865,\"hint\":\"NODE\",\"fully_loaded\":true})",
			a.toString());
	}

}
