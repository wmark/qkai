package qkai.model.db;

import javax.persistence.EntityManager;

//~--- non-JDK imports --------------------------------------------------------

import static org.junit.Assert.*;

import org.junit.*;

import qkai.logic.service.PersistenceService;

/**
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
public class SearchTokenTest {

	private EntityManager em;

	@Before
	public void doSetup() {
		em = PersistenceService.getInstance().getEntityManager();
	}

	@After
	public void destroy() {
		// em.close();
	}

	@Test
	public void testGetTid() {
		SearchToken a = em.find(SearchToken.class, 1);

		assertTrue(a.getID() == 1);
	}

	@Test
	public void testGetToken() {
		SearchToken a = em.find(SearchToken.class, 2);

		assertTrue(a.getToken().equals("Hanover"));
	}

	@Test
	public void testGetKb() {
		SearchToken a = em.find(SearchToken.class, 1);

		assertTrue(a.getKb().equals("DBPedia"));
	}

	/**
	 * Does equals work as expected?
	 */
	@Test
	public void equalsOfSearchToken() {
		SearchToken m = new SearchToken("DBPedia", "Hanover");
		SearchToken a = em.find(SearchToken.class, 2);
		SearchToken a2 = em.find(SearchToken.class, 2);
		SearchToken b = em.find(SearchToken.class, 1);

		// reflexive
		assertTrue("a not reflexive", a.equals(a));
		assertTrue("a not reflexive on a2", a.equals(a2));
		assertTrue("b not reflexive", b.equals(b));
		// symmetric
		assertFalse("a should not equal b", a.equals(b));
		assertFalse("b should not equal a", b.equals(a));
		assertTrue("a must equal m", a.equals(m));
		assertTrue("m must equal a", m.equals(a));
		// consistency
		assertFalse("b should not equal a", b.equals(a));
		assertFalse("b should not equal a", b.equals(a));
		// null
		assertFalse("a.equals(null) must be false", a.equals(null));
		assertFalse("b.equals(null) must be false", b.equals(null));
	}

	@Test
	public void testHashCode() {
		SearchToken m = new SearchToken("DBPedia", "Hanover");
		SearchToken a = em.find(SearchToken.class, 2);
		SearchToken b = em.find(SearchToken.class, 1);

		assertFalse(m.hashCode() == 0);
		assertFalse(a.hashCode() == 0);
		assertFalse(b.hashCode() == 0);
		if (m.equals(a)) {
			assertEquals(m.hashCode(), a.hashCode());
		}
		if (!a.equals(b)) {
			assertTrue(a.hashCode() != b.hashCode());
		}
	}

	@Test
	public void testToString() {
		SearchToken m = new SearchToken("DBPedia", "Hanover");
		SearchToken a = em.find(SearchToken.class, 2);

		assertEquals("qkai.model.db.SearchToken({\"token\":\"Hanover\",\"kb\":\"DBPedia\",\"tid\":null})", m.toString());
		assertEquals("qkai.model.db.SearchToken({\"token\":\"Hanover\",\"kb\":\"DBPedia\",\"tid\":2})", a.toString());
	}

}
