package qkai.model.db;

import javax.persistence.EntityManager;

//~--- non-JDK imports --------------------------------------------------------

import static org.junit.Assert.*;

import org.junit.*;

import qkai.logic.service.PersistenceService;

/**
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
public class RelationTest {

	private EntityManager em;

	@Before
	public void doSetup() {
		em = PersistenceService.getInstance().getEntityManager();
	}

	@After
	public void destroy() {
		// em.close();
	}

	@Test
	public void testGetRelation() {
		long id = 281L;
		Relation a = em.find(Relation.class, id);

		assertTrue(a.getID() == id);
	}

	@Test
	public void testGetPredicate() {
		Relation a = em.find(Relation.class, 756L);

		assertEquals("http://dbpedia.org/property/disambiguates", a.getPredicate().getResourceURI());
	}

	@Test
	public void testGetTarget() {
		Relation a = em.find(Relation.class, 787L);

		assertNotNull(a.getTarget());
		assertEquals(872L, a.getTarget().getID());
	}

	@Test
	public void testGetResource() {
		Relation a = em.find(Relation.class, 787L);

		assertNotNull(a.getResource());
		assertEquals(865L, a.getResource().getID());
	}

	/**
	 * Does equals work as expected?
	 */
	@Test
	public void equalsOfRelation() {
		Relation m = new Relation(em.find(Resource.class, 865L), em.find(Resource.class, 281L), em.find(Resource.class, 872L));
		Relation a = em.find(Relation.class, 787L);
		Relation a2 = em.find(Relation.class, 787L);
		Relation b = em.find(Relation.class, 225L);

		// reflexive
		assertTrue("a not reflexive", a.equals(a));
		assertTrue("a not reflexive on a2", a.equals(a2));
		assertTrue("b not reflexive", b.equals(b));
		// symmetric
		assertFalse("a should not equal b", a.equals(b));
		assertFalse("b should not equal a", b.equals(a));
		assertTrue("a must equal m", a.equals(m));
		assertTrue("m must equal a", m.equals(a));
		// consistency
		assertFalse("b should not equal a", b.equals(a));
		assertFalse("b should not equal a", b.equals(a));
		// null
		assertFalse("a.equals(null) must be false", a.equals(null));
		assertFalse("b.equals(null) must be false", b.equals(null));
	}

	@Test
	public void testHashCode() {
		Relation m = new Relation(em.find(Resource.class, 865L), em.find(Resource.class, 281L), em.find(Resource.class, 872L));
		Relation a = em.find(Relation.class, 787L);
		Relation b = em.find(Relation.class, 225L);

		assertFalse(m.hashCode() == 0);
		assertFalse(a.hashCode() == 0);
		assertFalse(b.hashCode() == 0);
		if (m.equals(a)) {
			assertEquals(m.hashCode(), a.hashCode());
		}
		if (!a.equals(b)) {
			assertTrue(a.hashCode() != b.hashCode());
		}
	}

	@Test
	public void testToString() {
		Relation a = em.find(Relation.class, 756L);

		assertEquals(
			"qkai.model.db.Relation({\"predicate\":281,\"n\":null,\"target\":865,\"resource\":865,\"relation\":756})",
			a.toString());
	}

}
