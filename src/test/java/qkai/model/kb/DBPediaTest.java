package qkai.model.kb;

import static org.junit.Assert.*;

import org.junit.*;

import qkai.model.db.Resource;
import qkai.model.db.SearchToken;

/**
 * Test class for KB {@link DBPedia}
 *
 * @author              W-Mark Kubacki
 * @version             $Revision$
 */
public class DBPediaTest {

	/**
	 * Test method for {@link DBPedia#resourcesContainingToken} for checking raw output.
	 * @throws java.lang.JSONException
	 */
	@Ignore("testrun is expensive") @Test
	public void resourcesContainingToken() {
		try {
			DBPedia t = new DBPedia();

			assertTrue(t.resourcesContainingToken(new SearchToken(t.getName(), "Hanover")).size() > 0);
			assertTrue(t.resourcesContainingToken(new SearchToken(t.getName(), "UodAkdfCjwDgdf")).size() == 0);
		} catch (java.io.IOException e) {
			fail("IOException. -> " + e);
		}
	}

	/**
	 * Test method for {@link DBPedia#getResourcesAdjacencies} for checking raw output.
	 * @throws java.lang.JSONException
	 */
	@Ignore("testrun is expensive") @Test
	public void getResourcesAdjacencies() {
		try {
			DBPedia t = new DBPedia();
			String uri1 = "http://dbpedia.org/resource/Hanover";
			String uri2 = "http://does-not-exists.nono";

			assertTrue(t.getResourcesAdjacencies(Resource.valueOf(uri1, Resource.ContentHint.NODE)).getDescribedResources().size() > 0);
			assertTrue(t.getResourcesAdjacencies(Resource.valueOf(uri2, Resource.ContentHint.NODE)).getDescribedResources().size() == 0);
		} catch (java.io.IOException e) {
			fail("IOException. -> " + e);
		}
	}

	@Test
	public void getName() {
		DBPedia t = new DBPedia();

		assertEquals("DBPedia", t.getName());
	}
}
