API
~~~~~

      [Note:] This is a work-in-progress.

* Methods
~~~~~~~~~~~

** Debug and Info Provider
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------+---------------------------------------------------------------+
| /info/request               | Echoes request headers send by the client.
*-----------------------------+---------------------------------------------------------------+
| /info/datetime              | Gets the server's current date and time in ISO 8601 format.
*-----------------------------+---------------------------------------------------------------+

*** <<</info/request>>>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------:---------------------------------------------------------------+
 Path                         | /info/request
*-----------------------------:---------------------------------------------------------------+
 Methods                      | GET
*-----------------------------:---------------------------------------------------------------+
 Consumes                     | none
*-----------------------------:---------------------------------------------------------------+
 Produces                     | "application/json"
*-----------------------------:---------------------------------------------------------------+
 Purpose                      | debugging
*-----------------------------:---------------------------------------------------------------+
 Description                  | Echoes request headers send by the client.
*-----------------------------:---------------------------------------------------------------+

**** usage
~~~~~~~~~~~~

  The request is:

---
GET http://qkai.org/info/request
---

  The response is (body reformatted for readability):

---
Content-Type: application/json

{
  "connection": ["keep-alive"],
  "host": ["localhost:8080"],
  "accept-language": ["de,en;q=0.9,pl;q=0.7,cs;q=0.6,zh-cn;q=0.4,zh;q=0.3,la;q=0.1"],
  "accept": ["text/html,application/xhtml+xml,application/xml;q=0.9"],
  "keep-alive": ["300"],
  "user-agent": ["Mozilla/5.0 (Windows; U; Windows NT 6.0; de; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8 (.NET CLR 3.5.30729)"],
  "accept-encoding": ["gzip,deflate"],
  "accept-charset": ["ISO-8859-1,utf-8;q=0.7,*;q=0.7"]
}
---

*** <<</info/datetime>>>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------:---------------------------------------------------------------+
 Path                         | /info/datetime
*-----------------------------:---------------------------------------------------------------+
 Methods                      | GET
*-----------------------------:---------------------------------------------------------------+
 Consumes                     | none
*-----------------------------:---------------------------------------------------------------+
 Produces                     | "text/plain"
*-----------------------------:---------------------------------------------------------------+
 Purpose                      | debugging, synchronization
*-----------------------------:---------------------------------------------------------------+
 Description                  | Gets the server's current date and time in ISO 8601 format.
*-----------------------------:---------------------------------------------------------------+

**** output format
~~~~~~~~~~~~~~~~~~~~

  String with the date and time in the given format.

**** usage
~~~~~~~~~~~~

  The request is:

---
GET http://qkai.org/info/datetime
---

  The response is:

---
Content-Type: text/plain; charset=utf-8

2009-06-29T11:52:33+02:00
---

** Resource Controller
~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------+---------------------------------------------------------------+
| /resource/text/\{ids\}      | Gets resources' descriptive texts.
*-----------------------------+---------------------------------------------------------------+
| /resource/text/\{ids\}      | Gets images and geolocation for resources.
*-----------------------------+---------------------------------------------------------------+
| /resource/click/\{id\}      | Registers a user's click on a resource.
*-----------------------------+---------------------------------------------------------------+

*** <<</resource/text/\{ids\}>>>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------:---------------------------------------------------------------+
 Path                         | /resource/text/\{ids\}
*-----------------------------:---------------------------------------------------------------+
 Methods                      | GET
*-----------------------------:---------------------------------------------------------------+
 Consumes                     | none
*-----------------------------:---------------------------------------------------------------+
 Produces                     | "application/json", "text/javascript"
*-----------------------------:---------------------------------------------------------------+
 Purpose                      | search listing
*-----------------------------:---------------------------------------------------------------+
 Description                  | Gets resources' descriptive texts.
*-----------------------------:---------------------------------------------------------------+
 Side-Effects                 | Measures the time retrieving textual data from database.
                              | Evaluates the request's "<<<Accept-Language>>>" header.
*-----------------------------:---------------------------------------------------------------+

  As the order of the provided IDs is not preserved, you must re-order (re-rank) the outputs.

**** parameters
~~~~~~~~~~~~~~~~~

  [ids] can be one or many IDs, representing the resources the texts are fetched for. \
        If for a resource does not exists by a given ID, that ID is ignored and the remaining processed. \
        IDs are of type <<<Long>>>. They are delimited by commas, without any whitespace.

**** usage
~~~~~~~~~~~~

  The request is:

---
GET http://qkai.org/resource/text/180371,430770
---

  The response is:

---
Content-Type: application/json

{
	"elapsed":8,
	"items": [
		{
			"id":180371,
			"lastUpdate":"2009-05-05 00:56:42.0",
			"caption":"Butterkeks",
			"comment":"Butterkekse sind Kekse aus Weizenmehl [...]",
			"URI":"http://dbpedia.org/resource/Leibniz-Keks"
		},
		{
			"id":430770,
			"description":"deutscher Philosoph und [...]",
			"lastUpdate":"2009-05-05 01:02:28.0",
			"caption":"Gottfried Wilhelm Leibniz",
			"comment":"Gottfried Wilhelm Leibniz [...] Doktor des weltlichen und des Kirchenrechts.",
			"URI":"http://dbpedia.org/resource/Gottfried_Leibniz"
		}
	]
}
---

  "<<elapsed>>" is the time it took to retrieve the textual representation from database, in milliseconds. \
  Fields such as "<<caption>>", "<<description>>" or "<<comment>>" are only set if available, else left out.

  The texts' language will depend on the request's "<<Accept-Language>>", with the preferred language
  being served, if available, and the fallback language (<<<en>>>) being used instead.

*** <<</resource/image,geo/\{ids\}>>>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------:---------------------------------------------------------------+
 Path                         | /resource/image,geo/\{ids\}
*-----------------------------:---------------------------------------------------------------+
 Methods                      | GET
*-----------------------------:---------------------------------------------------------------+
 Consumes                     | none
*-----------------------------:---------------------------------------------------------------+
 Produces                     | "application/json", "text/javascript"
*-----------------------------:---------------------------------------------------------------+
 Purpose                      | display in maps
*-----------------------------:---------------------------------------------------------------+
 Description                  | Gets images and geolocation for resources.
*-----------------------------:---------------------------------------------------------------+
 Side-Effects                 | Measures the time retrieving data from database.
*-----------------------------:---------------------------------------------------------------+

  As the order of the provided IDs is not preserved, you must re-order (re-rank) the outputs.

**** parameters
~~~~~~~~~~~~~~~~~

  [ids] can be one or many IDs, representing the resources the images and geolocation are fetched for. \
        If for a resource does not exists by a given ID, that ID is ignored and the remaining processed. \
        IDs are of type <<<Long>>>. They are delimited by commas, without any whitespace.

**** usage
~~~~~~~~~~~~

  The request is:

---
GET http://qkai.org/resource/image,geo/410,510
---

  The response is:

---
Content-Type: application/json

{
	"elapsed":7,
	"items": [
		{
			"id":410,
			"predicate":5111030,
			"longitude":"101.616",
			"latitude":"3.14815",
			"imageURI":"http://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/1_Utama.jpg/200px-1_Utama.jpg"
		},
		{
			"id":410,
			"predicate":5111034,
			"longitude":"101.616",
			"latitude":"3.14815",
			"imageURI":"http://upload.wikimedia.org/wikipedia/commons/3/3c/1_Utama.jpg"
		}
	]
}
---

  Both image and geolocation are returned or none att all for a resource. \
  In order to determine the meaning of the predicate you will have to issue another webservice request.

*** <<</resource/click/\{id\}>>>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------:---------------------------------------------------------------+
 Path                         | /resource/click/\{id\}
*-----------------------------:---------------------------------------------------------------+
 Methods                      | PUT
*-----------------------------:---------------------------------------------------------------+
 Consumes                     | none
*-----------------------------:---------------------------------------------------------------+
 Produces                     | none
*-----------------------------:---------------------------------------------------------------+
 Purpose                      | search, for ranking
*-----------------------------:---------------------------------------------------------------+
 Description                  | Registers a user's click on a resource.
*-----------------------------:---------------------------------------------------------------+
 Side-Effects                 | Attributes the click to the current user (or Anonymous)
*-----------------------------:---------------------------------------------------------------+

**** parameters
~~~~~~~~~~~~~~~~~

  [id] ID of the resource to register a click for

  The current user is determined by the cookie data.

**** usage
~~~~~~~~~~~~

  The request is:

---
PUT http://qkai.org/resource/click/430770
Cookie: JSESSIONID=DC8CAB427F8FA4310C4FF8AB3D0C69C8
---

  There is no response body. On errors yields an appriopriate HTTP status code.

** AutoComplete Controller
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------+---------------------------------------------------------------+
| /autocomplete/searchtoken   | Gets suggestions for search token.
*-----------------------------+---------------------------------------------------------------+
| /autocomplete/category      | Gets suggestions for categories.
*-----------------------------+---------------------------------------------------------------+

  These methods' outputs are crafted for {{{http://script.aculo.us/docs/Autocompletion.html}Prototype's AutoComplete}} widget.

*** <<</autocomplete/searchtoken>>> and <<</autocomplete/category>>>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------:---------------------------------------------------------------+
 Path                         | /autocomplete/searchtoken
                              | /autocomplete/category
*-----------------------------:---------------------------------------------------------------+
 Methods                      | POST
*-----------------------------:---------------------------------------------------------------+
 Consumes                     | "application/x-www-form-urlencoded"
*-----------------------------:---------------------------------------------------------------+
 Produces                     | "text/javascript", "text/plain", "text/html"
*-----------------------------:---------------------------------------------------------------+
 Purpose                      | search input
*-----------------------------:---------------------------------------------------------------+
 Description                  | Gets suggestions for search fields.
*-----------------------------:---------------------------------------------------------------+

**** parameters
~~~~~~~~~~~~~~~~~

  [input] A fraction of the user's input as text. \
          Will be used to find similar text as suggestion or completion of the term. \
          Input shorter than three (excl.) characters will be ignored.

**** usage
~~~~~~~~~~~~

  The request is:

---
POST http://qkai.org/autocomplete/searchtoken
Accept: text/html

input: Leibni
---

  The response is:

---
Content-Type: text/html;charset=utf-8

<ul>
	<li>"Gottfried Wilhelm Leibniz"</li>
	<li>"Gottfried Wilhelm Leibniz Universität Hannover"</li>
	[...]
	<li>Leibniz-Keks</li>
</ul>
---

** POI Controller
~~~~~~~~~~~~~~~~~~~
*-----------------------------+---------------------------------------------------------------+
| /poi/resources              | Selects resources matching a POI.
*-----------------------------+---------------------------------------------------------------+

  [Note:] The POI controller's behaviour depends on the activated POI setter extensions.

*** /poi/resources
~~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------:---------------------------------------------------------------+
 Path                         | /poi/resources
*-----------------------------:---------------------------------------------------------------+
 Methods                      | POST
*-----------------------------:---------------------------------------------------------------+
 Consumes                     | "application/x-www-form-urlencoded"
*-----------------------------:---------------------------------------------------------------+
 Produces                     | "text/javascript", "application/json"
*-----------------------------:---------------------------------------------------------------+
 Purpose                      | search, universal
*-----------------------------:---------------------------------------------------------------+
 Description                  | Selects resources matching a POI.
                              | Returns them as (weighted, but not ranked) list of IDs.
*-----------------------------:---------------------------------------------------------------+

**** parameters
~~~~~~~~~~~~~~~~~

  [PoiId] Request identifier, used by the client to detect out-of-order responses. \
          Setting POIs might take long on slow machines. In order to avoid an recent request
          be overwritten with a slower one being received after the recent, you must
          set unique identifiers (e.g., integers increased per request) for book-keeping.

  [poi] Nested parameters for the POI setter extensions. The format is JSON. \
        The POI setter are identified by unique names, such as "geolocation", "fulltext" or "category".

**** usage
~~~~~~~~~~~~

  The request is:

---
POST http://qkai.org/poi/resources
Accept: application/json
Content-Type: application/x-www-form-urlencoded

PoiId: 4
poi: {"fulltext": "Leibniz", "category": "Philosopher", "geolocation": "25km around Hanover, Germany"}
---

  The response is:

---
Content-Type: application/json

{
	"elapsed": 63,
	"items": [
		478665,2754622,2780805,721220,4292936,
		279830,1532902,1890125,521749,3073560,
		4212423,47418,217573,407979,2255624,
		430770,471622,510869,533146,554186,
		554394,555805,626986,834853,79182
	],
	"PoiId": 4
}
---

** Wikipedia Extractor Controller
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------+---------------------------------------------------------------+
| /WikipediaExtractor         | Extracts information units from Wikipedia pages.
*-----------------------------+---------------------------------------------------------------+

*** /WikipediaExtractor
~~~~~~~~~~~~~~~~~~~~~~~~~
*-----------------------------:---------------------------------------------------------------+
 Path                         | GET: /WikipediaExtractor/{pageTitle}
                              | POST: /WikipediaExtractor
*-----------------------------:---------------------------------------------------------------+
 Methods                      | GET, POST
*-----------------------------:---------------------------------------------------------------+
 Consumes                     | "application/x-www-form-urlencoded" if POST
*-----------------------------:---------------------------------------------------------------+
 Produces                     | "application/xml", "text/xml"
*-----------------------------:---------------------------------------------------------------+
 Purpose                      | legacy, experimental
*-----------------------------:---------------------------------------------------------------+
 Description                  | Extracts information units from Wikipedia pages.
                              | In general, these are sentences from the latter's printable version
*-----------------------------:---------------------------------------------------------------+

**** parameters
~~~~~~~~~~~~~~~~~

  [pageTitle] Title of the Wikipedia's page to be extracted. As stated in its URL.

  [language] Only if POST, else "en" is assumed. Determines from which Wikipedia's subdomain \
             and thus language the units get extracted. If not appropriate NLP sentence detector \
             has been configured english sentencing is used, but the informations will still \
             be in that given language.

**** usage
~~~~~~~~~~~~

  In case of GET:

---
GET http://qkai.org/WikipediaExtractor/Hanover
Accept: application/xml
---

  The response is (shortened for brevity):

---
Content-Type: application/xml

<article>
    <sect1 id="info1">
        <title id="info2">Hanover</title>
        [...]
        <para id="info78">
            <phrase id="info79">
                With a population of 522,944 (1 February 2007) the city is a major center of northern Germany,
                known for hosting annual commercial expositions such as the Hanover Fair and the CeBIT.
            </phrase>
        </para>
    </sect1>
</article>
---
