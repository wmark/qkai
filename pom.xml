<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>qkai</groupId>
  <artifactId>qkai</artifactId>
  <inceptionYear>2008</inceptionYear>
  <packaging>war</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>qKai Webapp</name>
  <description>qualifying Knowledge Acquisition and Inquiry</description>
  <url>http://qkai.org</url>

  <organization>
    <name>University of Hanover, Department of Computer Science, Institute of Systems Engineering</name>
    <url>http://www.sra.uni-hannover.de/</url>
  </organization>

  <scm>
    <developerConnection>scm:svn:https://username:password@svn.qkai.org/svn/projects/qKai/</developerConnection>
  </scm>

  <issueManagement>
    <system>Flyspray</system>
    <url>http://extern.sra.uni-hannover.de/sra-bts/</url>
  </issueManagement>

  <ciManagement>
    <system>Hudson</system>
    <url>http://ci.qkai.org/</url>
  </ciManagement>

  <distributionManagement>
    <site>
      <id>qkai-docs</id>
      <url>scp://docs.qkai.org/srv/www/docs/html</url>
    </site>
    <repository>
      <id>nexus-releases</id>
      <name>Internal Releases</name>
      <url>http://nexus.ossdl.de/content/repositories/releases</url>
    </repository>
    <snapshotRepository>
      <id>nexus-snapshots</id>
      <name>Internal Snapshots</name>
      <url>http://nexus.ossdl.de/content/repositories/snapshots</url>
    </snapshotRepository>
  </distributionManagement>

  <developers>
    <developer>
      <id>steinberg</id>
      <name>Monika Steinberg</name>
      <email>steinberg@sra.uni-hannover.de</email>
      <url>http://blog.qkai.org/</url>
      <roles>
        <role>Founder</role>
        <role>Architect</role>
      </roles>
      <timezone>+2</timezone>
    </developer>
    <developer>
      <id>kubacki</id>
      <name>W-Mark Kubacki</name>
      <email>wmark@hurrikane.de</email>
      <url>http://mark.ossdl.de/</url>
      <roles>
        <role>Search</role>
        <role>Release Engineer</role>
        <role>Repository Maintainer</role>
      </roles>
      <timezone>+2</timezone>
    </developer>
    <developer>
      <id>hein</id>
      <name>Jan Hein</name>
      <email>janp.hein@web.de</email>
      <roles>
        <role>Information Mining</role>
      </roles>
      <timezone>+2</timezone>
    </developer>
  </developers>

  <prerequisites>
    <maven>2.0.10</maven>
  </prerequisites>

  <repositories>
    <repository>
      <id>central</id>
      <url>http://nexus.qkai.org/content/groups/public/</url>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
    <repository>
      <id>snapshots</id>
      <url>http://nexus.qkai.org/content/groups/public-snapshots/</url>
      <releases>
        <enabled>false</enabled>
      </releases>
    </repository>
  </repositories>
  <pluginRepositories>
    <pluginRepository>
      <id>central</id>
      <url>http://nexus.qkai.org/content/groups/public/</url>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </pluginRepository>
    <pluginRepository>
      <id>snapshots</id>
      <url>http://nexus.qkai.org/content/groups/public-snapshots/</url>
      <releases>
        <enabled>false</enabled>
      </releases>
    </pluginRepository>
  </pluginRepositories>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <jersey-version>1.1.1-ea</jersey-version>
    <slf4j-version>1.5.8</slf4j-version>
    <eclipselink-version>2.0.0.r3856</eclipselink-version><!-- 2.0.0-M2-->
    <mysql-connector-version>5.1.8</mysql-connector-version>
    <!-- defaults; 
         Overwrite them by your profile defined in ~/.m2/settings.xml .
         see http://maven.apache.org/guides/introduction/introduction-to-profiles.html
    -->
    <sql-skip-create-testdata>false</sql-skip-create-testdata>
    <sql-skip-create-tables>true</sql-skip-create-tables>
    <sql-root-user>root</sql-root-user>
    <sql-root-passwd>root</sql-root-passwd>
    <jetty-port>8012</jetty-port><!-- if not 8012, alter integration tests! -->
  </properties>

  <dependencies>
    <!-- TEST SCOPE DEPENDENCIES -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>[4.4,)</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>javanettasks</groupId>
      <artifactId>httpunit</artifactId>
      <version>[1.6.1,)</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <!-- this is needed by HTTP-UNIT and is missing from their pom.xml -->
      <groupId>rhino</groupId>
      <artifactId>js</artifactId>
      <version>1.7R1</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.hamcrest</groupId>
      <artifactId>hamcrest-all</artifactId>
      <version>[1.1,)</version>
	  <scope>test</scope>
    </dependency>
    <!-- COMMON -->
    <dependency>
      <groupId>org.jdom</groupId>
      <artifactId>jdom</artifactId>
      <version>1.1</version>
    </dependency>
    <dependency>
      <groupId>com.sun.jersey</groupId>
      <artifactId>jersey-server</artifactId>
       <version>${jersey-version}</version>
    </dependency>
    <dependency>
      <groupId>org.json</groupId>
      <artifactId>json</artifactId>
      <version>20090211</version>
    </dependency>
    <dependency>
      <groupId>net.sourceforge.opennlp</groupId>
      <artifactId>opennlp-tools</artifactId>
      <version>1.4.0</version>
    </dependency>
    <dependency>
      <groupId>net.sourceforge.opennlp</groupId>
      <artifactId>maxent</artifactId>
      <version>2.4.0</version>
    </dependency>
	<dependency>
	  <!-- needed by opennlp.maxent -->
	  <groupId>trove</groupId>
	  <artifactId>trove</artifactId>
	  <version>2.1.1</version>
	</dependency>
    <dependency>
      <groupId>org.openrdf</groupId>
      <artifactId>rio</artifactId>
      <version>1.0.10</version>
    </dependency>
    <dependency>
      <groupId>ant</groupId>
      <artifactId>ant-bzip2</artifactId>
      <version>1.7.1</version>
    </dependency>
    <dependency>
      <groupId>commons-lang</groupId>
      <artifactId>commons-lang</artifactId>
      <version>2.4</version>
    </dependency>
    <dependency>
      <groupId>apache-httpclient</groupId>
      <artifactId>commons-httpclient</artifactId>
      <version>3.1</version>
    </dependency>
    <dependency>
      <groupId>com.dyuproject</groupId>
      <artifactId>dyuproject-openid</artifactId>
      <version>1.1.3</version>
    </dependency>
    <dependency>
      <groupId>org.eclipse.persistence</groupId>
      <artifactId>eclipselink</artifactId>
      <version>${eclipselink-version}</version>
    </dependency>
    <dependency>
      <!-- needed by eclipselink -->
      <groupId>javax.persistence</groupId>
      <artifactId>persistence</artifactId>
      <version>[2.0_preview,)</version>
    </dependency>
    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>${mysql-connector-version}</version>
      <classifier>bin</classifier>
    </dependency>
    <!-- PROVIDED BY CONTAINER; HERE FOR COMPILE ONLY -->
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>servlet-api</artifactId>
      <version>[2.5,)</version>
      <scope>provided</scope>
    </dependency>
    <!-- NEEDED IN CONTAINER -->
    <dependency>
      <groupId>opensymphony</groupId>
      <artifactId>sitemesh</artifactId>
      <version>[2.4,)</version>
    </dependency>
    <!-- SLF4J troublemaker -->
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-simple</artifactId>
      <version>${slf4j-version}</version>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
      <version>${slf4j-version}</version>
      <scope>compile</scope>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>jcl104-over-slf4j</artifactId>
      <version>${slf4j-version}</version>
      <scope>compile</scope>
    </dependency>
  </dependencies>

  <build>
    <finalName>qkai</finalName>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-site-plugin</artifactId>
      </plugin>
      <!-- THIS IS FOR TOMCAT -->
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>tomcat-maven-plugin</artifactId>
        <configuration>
          <path>/</path>
        </configuration>
      </plugin>
      <!-- THIS FOR COMPILING -->
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <configuration>
            <source>1.6</source>
            <target>1.6</target>
            <fork>true</fork>
            <showDeprecation>true</showDeprecation>
            <showWarnings>true</showWarnings>
        </configuration>
      </plugin>
      <!-- FOR ECLIPSELINK CODE WEAVING -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-antrun-plugin</artifactId>
        <executions>
          <execution>
          <phase>compile</phase>
          <configuration>
            <tasks>
              <echo>Static weaving of JPA Entities...</echo>
              <java classname="org.eclipse.persistence.tools.weaving.jpa.StaticWeave" classpathref="maven.runtime.classpath" fork="true">
                <arg line="-loglevel WARNING -persistenceinfo src/main/webapp/WEB-INF/classes target/classes target/classes" />
              </java>
              <echo>You can safely ignore exceptions about 'HttpServlet' not being found.</echo>
            </tasks>
          </configuration>
          <goals>
            <goal>run</goal>
          </goals>
          </execution>
        </executions>
      </plugin>
      <!-- FOR TESTS -->
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>sql-maven-plugin</artifactId>
        <dependencies>
          <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>${mysql-connector-version}</version>
            <classifier>bin</classifier>
          </dependency>
        </dependencies>
        <configuration>
          <!--
            These credentials are not meant for qKAI in production mode but only for creating tables!
            You shall not give an exposed application too many rights.
          -->
          <driver>com.mysql.jdbc.Driver</driver>
          <username>${sql-root-user}</username>
          <password>${sql-root-passwd}</password>
        </configuration>
        <executions>
          <execution><!-- will create filled tables for testing -->
            <id>create-testdata</id>
            <phase>process-test-resources</phase>
            <goals>
              <goal>execute</goal>
            </goals>
            <configuration>
              <skip>${sql-skip-create-testdata}</skip>
              <url>jdbc:mysql://localhost:3306/qkai_test</url>
              <autocommit>true</autocommit>
              <srcFiles>
                <srcFile>src/test/sql/qKAI_database_testfill.sql</srcFile>
              </srcFiles>
            </configuration>
          </execution>
          <execution><!-- will create production tables after test -->
            <id>create-emtpy-tables</id>
            <phase>test</phase>
            <goals>
              <goal>execute</goal>
            </goals>
            <configuration>
              <skip>${sql-skip-create-tables}</skip><!-- SKIP is here -->
              <url>jdbc:mysql://localhost:3306/qkai</url>
              <autocommit>true</autocommit>
              <srcFiles>
                <srcFile>src/main/sql/qKAI_database.sql</srcFile>
              </srcFiles>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <excludes>
            <exclude>**/integration/*.java</exclude>
          </excludes>
        </configuration>
        <!-- integration tests with Jetty have their own testcases -->
          <executions>
            <execution>
              <id>integration-test</id>
              <goals>
                <goal>test</goal>
              </goals>
              <phase>integration-test</phase>
              <configuration>
                <excludes>
                  <exclude>none</exclude>
                </excludes>
                <includes>
                  <include>**/integration/*.java</include>
                </includes>
              </configuration>
            </execution>
          </executions>
      </plugin>
      <!-- FOR JETTY / SERVLET TESTING / INTEGRATION TESTS -->
      <plugin>
        <groupId>org.mortbay.jetty</groupId>
        <artifactId>maven-jetty-plugin</artifactId>
        <configuration>
          <scanIntervalSeconds>10</scanIntervalSeconds>
          <contextPath>/</contextPath>
          <stopKey>bar</stopKey>
          <stopPort>9999</stopPort>
          <connectors>
            <connector implementation="org.mortbay.jetty.nio.SelectChannelConnector">
              <port>${jetty-port}</port>
              <maxIdleTime>60000</maxIdleTime>
            </connector>
          </connectors>
          <webApp>${basedir}/target/jersey-demo</webApp>
          <requestLog implementation="org.mortbay.jetty.NCSARequestLog">
            <filename>target/yyyy_mm_dd.request.log</filename>
            <retainDays>90</retainDays>
            <append>true</append>
            <extended>true</extended>
            <logTimeZone>GMT</logTimeZone>
          </requestLog>
        </configuration>
        <executions>
           <!-- this is for running integration tests with JETTY -->
           <execution>
              <id>start-jetty</id>
              <phase>pre-integration-test</phase>
              <goals>
                 <goal>run</goal>
              </goals>
              <configuration>
                 <daemon>true</daemon>
              </configuration>
           </execution>
           <execution>
              <id>stop-jetty</id>
              <phase>post-integration-test</phase>
              <goals>
                 <goal>stop</goal>
              </goals>
           </execution>
        </executions>
      </plugin>
		<plugin>
			<groupId>net.sf.alchim</groupId>
			<artifactId>winstone-maven-plugin</artifactId>
			<version>1.2</version>
			<dependencies>
			  <dependency>
				<groupId>net.sourceforge.winstone</groupId>
				<artifactId>winstone</artifactId>
				<version>0.9.10</version>
			  </dependency>
				<dependency>
				  <groupId>tomcat</groupId>
				  <artifactId>jasper-compiler</artifactId>
				  <version>5.5.9</version>
				</dependency>
			</dependencies>
			<configuration>
				<cmdLineOptions>
					<property>
						<name>httpPort</name>
						<value>8080</value>
					</property>
					<property>
						<name>ajp13Port</name>
						<value>-1</value>
					</property>
					<property>
						<name>controlPort</name>
						<value>-1</value>
					</property>
					<property>
						<name>directoryListings</name>
						<value>false</value>
					</property>
					<property>
						<name>useInvoker</name>
						<value>false</value>
					</property>
					<property>
						<name>useJasper</name>
						<value>true</value>
					</property>
				</cmdLineOptions>
			</configuration>
			<executions>
				<execution>
					<goals>
						<goal>embed</goal>
					</goals>
					<phase>package</phase>
				</execution>
			</executions>
		</plugin>
    </plugins>
  </build>

  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-report-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jxr-plugin</artifactId>
        <version>2.1</version>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>jdepend-maven-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-pmd-plugin</artifactId>
        <configuration>
          <targetJdk>1.6</targetJdk>
          <rulesets>
            <ruleset>/rulesets/braces.xml</ruleset>
            <ruleset>/rulesets/coupling.xml</ruleset>
            <ruleset>/rulesets/clone.xml</ruleset>
            <ruleset>/rulesets/design.xml</ruleset>
            <ruleset>/rulesets/finalizers.xml</ruleset>
            <ruleset>/rulesets/imports.xml</ruleset>
            <ruleset>/rulesets/logging-java.xml</ruleset>
            <!-- ruleset>/rulesets/optimizations.xml</ruleset -->
            <!-- ruleset>/rulesets/strictexception.xml</ruleset -->
            <ruleset>/rulesets/strings.xml</ruleset>
            <ruleset>/rulesets/typeresolution.xml</ruleset>
            <ruleset>/rulesets/unusedcode.xml</ruleset>
          </rulesets>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <configuration>
          <taglet>net.sourceforge.taglets.Taglets</taglet>
          <tagletArtifact>
            <groupId>net.sourceforge.taglets</groupId>
            <artifactId>Taglets</artifactId>
            <version>2.0.3</version>
          </tagletArtifact>
          <quiet>true</quiet>
        </configuration>
      </plugin>
    </plugins>
  </reporting>

</project>
